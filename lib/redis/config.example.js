module.exports = {
    TEST: {
        HOST: '10.10.0.4',
        PORT: 6379,
        BULL_HOST: '10.10.0.4',
        BULL_PORT: 6379,
    },
    PRODUCTION: {
        HOST: '127.0.0.1',
        PORT: 6379,
        BULL_HOST: '127.0.0.1',
        BULL_PORT: 6379,
    }
}