const { promisify } = require('util')
const redis = require('redis');
const REDIS_CONFIG = require('./redis/config')
const { HOST: host, PORT: port } = REDIS_CONFIG[process.env.NODE_ENV === 'production' ? 'PRODUCTION' : 'TEST']
const redisClient = redis.createClient({
  host, port
});
const session = require('express-session');
const redisStore = require('connect-redis')(session);
const store = new redisStore({ host, port, client: redisClient, disabledTTL: true })

redisClient.on('error', (err) => {
  console.error('Redis error: ', err);
});
/* Functions */

const permissions = ['ADMIN', 'TRAINER', 'FITBOXER']
function checkVersion(req, currentVersion) {
  if (currentVersion) {
    const version = (req.request || req).headers.version
    if (version !== currentVersion) {
      throw new Error(`New version is available (${currentVersion}). Please refresh the page`)
    }
  }
}

function getCurrentRole(session) {
  if (!session || !session.isAuth) return null // throw new Error('Session is required')
  return permissions.find(el => el === session.currentRole)
}


async function setSession(req) {
  const header = req.headers.authorization
  if (!header) {
    // De la autenticación se encarga la capa Shield
    return null
  }
  
  const sessionId = header.replace('Bearer ', '')
  
  const getStore = promisify(store.get).bind(store)
  const session = await getStore(sessionId)
  if (session) {
    req.sessionID = sessionId
    req.session = { id: sessionId }
    for (const prop in session) {
      const value = session[prop]
      if (Array.isArray(value)) req.session[prop] = [...value]
      else if (typeof value === 'object') req.session[prop] = { ...value }
      else req.session[prop] = value
    }
    req.session.save = save
    req.session.destroy = destroy
  }
}

async function save(id, data) {
  const savePromise = promisify(store.set).bind(store)
  try {
    await savePromise(id, data)
  } catch (ex) {
    console.error(ex)
  }
}

async function destroy(id) {
  const del = promisify(store.destroy).bind(store)
  // console.log('destroy', id)
  try {
    await del(id)
  } catch (ex) {
    console.error(ex)
  }
}

module.exports.init = (app, currentVersion) => {
  app.use(async (req, res, next) => {
    await setSession(req, currentVersion).then(() => next()).catch((ex) => next(ex))
  })

  app.use(session({
    secret: 'swcLA700fZMbdsjIhGYh6PXuD5y55fgC',
    name: 'SessionHandle',
    resave: false,
    saveUninitialized: false,
    cookie: {
      secure: false,
      httpOnly: false,
      maxAge: 1000 * 60 * 60 * 24 * 365,
    }, // Note that the cookie-parser module is no longer needed
    store,
  }))

}

module.exports.store = store
module.exports.checkVersion = checkVersion
module.exports.getCurrentRole = getCurrentRole
