const crypto = require("crypto")
const axios = require('axios')
const mongodb =  require('../../datasources/mongodb/generated/prisma-client')
// Set your X-API-KEY with the API key from the Customer Area.

function md5(value) {
  return crypto.createHash('md5').update(value).digest("hex")
}

function sha256(value) {
  return crypto.createHash('sha256').update(value).digest('hex')
}

/**
 * 
 * @param {District} district 
 * @param {{cardTokens: Array<string>, email: string, shopperReference:string, defaultToken:string}} shopperInfo 
 * @param {returnUrl: string}
 * @returns 
 */
async function getPaymentMethods(district, shopperInfo, returnUrl ) {
  const cards = JSON.parse(shopperInfo.cardTokens || '[]')
  const response = {
    savedCards: cards,
    __config: {
      shopIDP: district.unitellerShopIDP,
      password: district.unitellerPassword,
      urlReturn: returnUrl,
      email: shopperInfo.email,
      customerIDP: shopperInfo.shopperReference,
      defaultToken: shopperInfo.defaultToken
    }
  }

  return response
}

/**
 * 
 * @param {string} token 
 * @param {Order} order 
 * @param {District} district 
 * @throws PaymentError
 * @returns 
 */
async function paymentWithToken(token, order, district) {
  try {
    if (!token) throw new Error('Token is required')
    const amount = (order.amount / 100).toString()
    const Shop_IDP = district.unitellerShopIDP //parametro de la cuenta Uniteller para tests
    const Order_IDP = order.id //numero de pedido
    const password = district.unitellerPassword

    const Signature = md5(md5(Shop_IDP) + '&' + md5(Order_IDP) + '&' + md5(amount) + '&' + md5(token) + '&' + md5(password)).toUpperCase()

    const receiptPosition = order.description // nombre el servicio/producto en el ticket
    const place = 'www.brooklynfitboxing.com' // lugar de pago, se pone la web
    const taxmode = 1 // tipo de imposición fiscal
    const vat = -1 // IVA
    const payattr = 1 // señal del pago
    const lineattr = 4 // objeto del pago

    const lines = [{
      name: receiptPosition,
      price: amount,
      qty: 1,
      sum: amount,
      vat,
      payattr,
      lineattr,
      taxmode
    }]

    const receipt = Buffer.from(JSON.stringify({
      'lines': lines,
      'params': {
        'place': place
      },
      'total': amount
    })).toString('base64')

    const ReceiptSignature = sha256(sha256(Shop_IDP) + '&' + sha256(Order_IDP) + '&' + sha256(amount) + '&' + sha256(receipt) + '&' + sha256(password)).toUpperCase()

    const postData = {
      "Shop_IDP": Shop_IDP,
      "Order_IDP": Order_IDP,
      "Subtotal_P": amount,
      "Parent_Order_IDP": token,
      "Signature": Signature,
      "Receipt": receipt,
      "ReceiptSignature": ReceiptSignature
    }

    const config = {
      method: 'POST',
      url: 'https://fpay.uniteller.ru/v1/recurrent',
      data: postData,
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
      transformRequest: [
        function (data) {
          const serializedData = []

          for (const k in data) {
            serializedData.push(`${k}=${encodeURIComponent(data[k])}`)
          }
          return serializedData.join('&')
        }
      ]
    }

    const { data } = await axios(config)

    const rows = data.split(/\r?\n/)
    let response
    if (rows[0].split(';')[1] === 'Response_Code' && rows[1].split(';')[1] === 'AS000') {
      // OK
      response = {
        status: 'authorized',
        id: Order_IDP
      }
    } else {
      // Error
      response = {
        status: 'failed',
        id: Order_IDP,
        refusalReason: rows[1].split(';')[1]
      }
    }
    return response
  } catch (ex) {
    // Actualizar order
    console.error(`uniteller - paymentWithToken`, ex)
    throw ex
  }
}
/**
 * 
 * @param {Order} order 
 * @param {{cardTokens: Array<string>, }} paymentData
 * @returns 
 */
async function setRecurringPayment(order, paymentData) {
  try {
    // const profile = await ctx.prisma.profile({v7_id: order.v7_clt})
    const card = paymentData.defaultToken || (JSON.parse(paymentData.cardTokens || '[{}]')[0].id || null)
    if (!card) {
      throw new Error('Unknown Credit Card for ' + order.id)
    }

    // Guardar el token usado en el objeto orden
    await mongodb.updateOrder({
      where: {
        id: order.id
      },
      data: {
        tokenReference: card.id
      }
    })

    const payment = await paymentWithToken(card, order)
    return payment
  } catch (ex) {
    // Actualizar order
    await mongodb.updateOrder({
      where: {
        id: order.id
      },
      data: {
        status: 'FAILED',
        error: `${ex}`
      }
    })
    throw ex
  }
}

async function disableToken(profile, id) {
  const cardTokens = JSON.parse(profile.cardTokens || '[]')
  const cardIndex = cardTokens.findIndex(el => el.id === id)
  if (cardIndex !== - 1) {
    cardTokens.splice(cardIndex, 1)
    // const fitboxer = new Fitboxer(ctx)
    // await fitboxer.findById(profile.v7_id)
    // await fitboxer.update({
    //   cardTokens: JSON.stringify(cardTokens)
    // })
    /*
    await ctx.prisma.updateProfile({
      where: {
        id: profile.id
      },
      data: {
        cardTokens: JSON.stringify(cardTokens)
      }
    })
    */
  }
}


module.exports = {
  getPaymentMethods,
  setRecurringPayment,
  paymentWithToken,
  disableToken,
}