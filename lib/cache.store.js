const { promisify } = require('util')

const redis = require('redis');
const REDIS_CONFIG = require('./redis/config')
const { HOST, PORT } = REDIS_CONFIG[process.env.NODE_ENV === 'production' ? 'PRODUCTION' : 'TEST']

const redisClient = redis.createClient({
  host: HOST,
  port: PORT
});

redisClient.on('error', (err) => {
  console.error('Redis Cache error: ', err);
});

const asyncClient = {}

asyncClient.get = promisify(redisClient.get).bind(redisClient)
asyncClient.set = promisify(redisClient.set).bind(redisClient)
asyncClient.hmset = promisify(redisClient.hmset).bind(redisClient)
asyncClient.setex = promisify(redisClient.setex).bind(redisClient)
asyncClient.del = promisify(redisClient.del).bind(redisClient)
asyncClient.keys = promisify(redisClient.keys).bind(redisClient)
asyncClient.ttl = promisify(redisClient.ttl).bind(redisClient)

async function get(key) {
  return asyncClient.get(key)
}

async function keys(pattern) {
  return asyncClient.keys(pattern)
}

async function expireTime(key) {
  return asyncClient.ttl(key)
}

async function getCacheExpiration(key) {
  return asyncClient.ttl(`fwg:$${key}`)
}
async function set(key, value, expire) {
  if (expire) {
    return asyncClient.setex(key, expire, typeof value === 'object' ? JSON.stringify(value) : value)
  } else {
    return asyncClient.set(key, typeof value === 'object' ? JSON.stringify(value) : value)
  }
}

async function del(keys) {
  return asyncClient.del(keys)
}

async function getCache(key) {
  return get(`fwg:$${key}`)
}
/**
 * 
 * @param {string} key 
 * @param {string} value 
 * @param {number} expire 
 * @returns 
 */
async function setCache(key, value, expire) {
  return set(`fwg:$${key}`, value, expire)
}

async function delCache(key) {
  if (Array.isArray(key)) {
    const newKeys = key.map(k => `fwg$${k}`)
    return del(newKeys)

  } else {
    return del(`fwg:$${key}`)
  }
}

module.exports = {
  get, set, del, keys, expireTime, getCacheExpiration,
  getCache, setCache, delCache
}


