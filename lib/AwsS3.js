const fs = require('fs')
const stream = require('stream')
const { promisify } = require('util')
const aws = require('aws-sdk') // ^2.2.41
const multer = require('multer') // "multer": "^1.1.0"
const multerS3 = require('multer-s3');

class AwsS3 {
  constructor(AWSConfig) {
    this.config = {
      bucketName: AWSConfig.BUCKET_NAME,
      dirName: '.', // root by default 
      region: AWSConfig.REGION,
      accessKeyId: AWSConfig.ACCESS_KEY_ID,
      secretAccessKey: AWSConfig.SECRET_ACCESS_KEY,
      s3Url: AWSConfig.S3_URL
    }
    aws.config.update(this.config);
    this.s3 = new aws.S3();
  }

  async getSignedUrl({ bucket, fileLocation, expires }) {
    const params = {
      'Bucket': bucket,
      'Key': fileLocation,
    };
    if(expires) {
      params.Expires = expires
    }
    promisify(this.s3.getSignedUrlPromise);
    const signedUrl = await this.s3.getSignedUrlPromise('getObject',params);
    return signedUrl
  }
  deleteFile({ bucketName, file }) {
    const params = {
      'Bucket': bucketName,
      'Key': file
    };
    this.s3.deleteObject(params).promise()
  }
  async copyFile({ sourceFile, sourceBucket, destinationBucket, destinationFile, deleteSource = false }) {
    const source = sourceBucket + '/' + sourceFile;
    const params = {
      Bucket: destinationBucket,
      CopySource: source,
      Key: destinationFile
    };
    const copyResult = await this.s3.copyObject(params).promise();
    const location = this.getUrlFromBucket(destinationFile);
    let result = { ...copyResult, location }
    if (deleteSource) {
      try {
        await this.deleteFile({ bucketName: sourceBucket, file: sourceFile });
        result = { ...copyResult, deletedSource: true, location }
      } catch (ex) {
        console.error(ex);
        throw ex;
      }
    }
    return result
  }
  getUrlFromBucket(fileName) {
    const { bucketName, region } = this.config;
    const regionString = region.includes('us-east-1') ? '' : ('-' + region)
    return `https://${bucketName}.s3${regionString}.amazonaws.com/${fileName}`;
  }

  uploadMiddleware() {
    return multer({
      storage: multerS3({
        s3: this.s3,
        acl: 'public-read',
        bucket: this.config.bucketName,
        key: function (req, file, cb) {
          cb(null, file.originalname);
        }
      }),
      limits: { fileSize: 1024 * 1024 * 1024 }
    });
  }

  /**
   * 
   * @param {String} filepath ruta al fichero. 
   * @returns String base64
   */
  getBinaryFromFile(filePath) {
    const file = fs.readFileSync(filePath);
    const bin = Buffer.from(file, 'binary');
    return bin;
  }

  /**
   * Subir ficheros a bucket en AWS
   * @param {bucketName, fileName, fileBuffer}  
   * @returns 
   */
  async uploadFile({ bucketName, key, fileBuffer }) {
    const params = {
      'Bucket': bucketName,
      'Key': key,
      'Body': fileBuffer,
      'ACL': 'public-read'
    };
    promisify(this.s3.putObject);
    const uploadResult = await this.s3.putObject(params);
    return uploadResult;
  }

  uploadStream({ Bucket, Key }) {
    const pass = new stream.PassThrough();
    return {
      writeStream: pass,
      promise: this.s3.upload({ Bucket, Key, Body: pass }).promise(),
    };
  }

}

module.exports = {
  init: (AWSConfig) => {
    return new AwsS3(AWSConfig);
  }
}