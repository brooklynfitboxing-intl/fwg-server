const { Client, Config, CheckoutAPI } = require('@adyen/api-library')
const AdyenConfig = require('./config')
const axios = require('axios')

const PUBLIC_BASE_URL = process.env.PUBLIC_BASE_URL || 'https://fitboxingworldgames.com'

const ADYEN_LOCALE = {
  'es': 'es-ES',
  'en': 'en-US',
  'it': 'it-IT',
  'pt': 'pt-PT'
}

function AdyenClient(club) {
  const environment = club.adyenEnv
  const clientKey = (AdyenConfig[environment] || AdyenConfig.live).clientKey

  const config = new Config();
  config.apiKey = (AdyenConfig[environment] || AdyenConfig.live).apiKey

  const client = new Client({
    config,
    httpClient: {
      async request(endpoint, json, config, isApiKeyRequired, requestOptions) {
        const response = await axios({
          method: 'POST',
          url: endpoint,
          data: JSON.parse(json),
          headers: {
            "X-API-Key": config.apiKey,
            "Content-type": "application/json"
          },
        });

        return response.data;
      }
    }
  })
  config.merchantAccount = club.adyenMerchantAccount

  if (environment === 'test') {
    client.setEnvironment("TEST")
  } else {
    client.setEnvironment("LIVE", AdyenConfig.live.liveEndpointUrlPrefix)
  }
  return { client, environment, clientKey }
}


function AdyenCheckout(club) {
  const { client, environment, clientKey } = AdyenClient(club)
  const checkout = new CheckoutAPI(client)
  return { checkout, environment, clientKey }
}

/**
 * 
 * @param {Object} club 
 * @param {string} shopperReference 
 * @param {number} amount 
 * @returns {Object}
 */
async function getPaymentMethods(club, shopperReference, currency, amount, locale) {

  const { checkout, environment, clientKey } = AdyenCheckout(club)

  const shopperLocale = ADYEN_LOCALE[locale || 'en']
  const paymentsResponse = await checkout.paymentMethods({
    merchantAccount: club.adyenMerchantAccount,
    countryCode: (club.country || 'es').toUpperCase(),
    shopperLocale,
    shopperReference,
    amount: { currency, value: parseFloat(amount) },
    channel: "Web"
  })

  paymentsResponse.__config = {
    clientKey,
    locale: shopperLocale,
    environment,
    billingAddressRequired: club.isBillingAddressRequired,
    defaultToken: null
  }
  return paymentsResponse
}

/**
 * 
 * @param {import('../../datasources/mongodb/generated/prisma-client').Order}  order
 * @param {object} paymentData paymentMethod, billingAddress, browserInfo 
 * @param {string} returnUrl 
 * @returns 
 */
async function setPayment(order, { currency, price, merchantAccount, paymentMethod, billingAddress, browserInfo, clientIp }, returnUrl, club) {
  const { checkout } = AdyenCheckout(club)
  try {
    const res = await checkout.payments({
      merchantAccount,
      paymentMethod,
      billingAddress, // Optional
      amount: { currency, value: price },
      reference: order.id,
      /* Store Token */
      storePaymentMethod: true,
      // shopperReference: order.shopperReference,
      recurringProcessingModel: 'CardOnFile',
      shopperInteraction: 'Ecommerce',
      /* Risk fields */
      shopperEmail: order.email,
      shopperName: `${order.firstName} ${order.lastName}`,
      shopperIP: clientIp,
      /* 3D Secure */
      browserInfo,
      additionalData: {
        allow3DS2: true
      },
      channel: 'Web',
      origin: PUBLIC_BASE_URL, //'https://fitboxingworldgames.com',
      returnUrl
    })

    return res

  } catch (ex) {
    if (ex.response) {
      console.error(`Error procesing order: ${order.id}`)
      console.error(ex.response.data)
    }
    throw ex
  }
}

/**
 * 
 * @param {object} district 
 * @param {ICheckout.DetailsRequest} paymentInfo 
 * @returns 
 */
async function setPaymentDetails(district, { details, paymentData }) {
  const { checkout } = AdyenCheckout(district)
  try {
    const res = await checkout.paymentsDetails({
      details,
      paymentData
    })
    return res
  } catch (ex) {
    if (ex.response) {
      console.error(`Error processing payment:`, ex.response.data)
    }
    throw ex
  }
}

/**
 * 
 * @param {import('../../../../brooklyn-server/mongodb/generated/prisma-client').District} district 
 */
async function refundPayment(district, { pspReference, amount, currency }) {
  const { checkout } = AdyenCheckout(district)
  const json = JSON.stringify({
    merchantAccount: checkout.client.config.merchantAccount,
    amount: {
      value: amount,
      currency
    }
  });
  const response = await checkout.client.httpClient.request(`/payments/${pspReference}/refunds`, json, checkout.client.config)
  return response
}

module.exports = {
  getPaymentMethods,
  setPayment,
  setPaymentDetails,
  refundPayment
}