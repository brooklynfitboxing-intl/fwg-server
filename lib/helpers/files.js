const { once } = require('events');
/**
 * 
 * @param {WritableStream} stream 
 * @param {Array<string>} data 
 */
async function writeToFile(stream, data) {
    if (!stream) {
        throw 'stream param is Not a valid WritableStream'
    }
    if (!stream.write(`${data.join(";")}\n`, "utf-8")) {
      await once(stream, "drain");
    }
  }
  module.exports = {
    writeToFile
  }