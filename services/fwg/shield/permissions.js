const { 
  shield,
  // and, 
  or, 
  // not, 
  allow 
} = require('graphql-shield')
const {
  isTrainer,
  isAdmin,
  isFitboxer,
  isReferee
  // isReferee
 } = require('../../../middleware/permissions')

const permissions = shield({
  Query: {
    // users: isAdmin,
    // // tournaments: allow
    // myTournaments: or(isReferee, isTrainer)

  },
  Mutation: {
    createUser: allow,
    createCode: isTrainer,
    createTeamOrder: isFitboxer,
    createPaymentOrder: isAdmin,
    updateTeam: or(isFitboxer, isAdmin) ,
    createTournament : isAdmin,
    updateTournament : isAdmin,
    publishTournament : isAdmin,
    deleteTournament : isAdmin,
    verifyUser: isAdmin,
    requestVerification: isAdmin,
    // logout: isAuthenticated
    setVotes:  isReferee,
    setScores: isAdmin

  },
}, {
  debug: true
})

module.exports = permissions