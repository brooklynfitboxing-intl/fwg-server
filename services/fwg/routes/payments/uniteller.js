const router = require('express').Router();
const crypto = require('crypto')
const District = require('../../../../../../brooklyn-server/abstractTypes/District');
const { prisma: mongodb } = require('../../../../datasources/mongodb/generated/prisma-client');
const Team = require('../../../../Entities/Team');
const Tournament = require('../../../../Entities/Tournament');

async function completeOrderHandler(req, res) {
  const { orderId } = req.params
  const { Signature, Status, CardNumber, Customer_IDP, Total } = req.body
  let order = await mongodb.order({ id: orderId })

  if (!order) { return res.status(404).json({ message: `Order ${orderId} not found.` }) }

  if (orderId && Signature && Status && CardNumber && Customer_IDP && Total) {
    const disctrictEntity = new District()
    const district = await disctrictEntity.findByAlias(order.clubMerchantAlias)
    const password = district.unitellerPassword

    const dataToEncode = orderId + Status + CardNumber + Customer_IDP + Total + password
    const calcSignature = crypto.createHash('md5').update(dataToEncode).digest("hex")

    if (calcSignature.toUpperCase() === Signature) {
      if (Status === 'authorized') {
        await mongodb.updateOrder({ where: { id: orderId }, data: { status: 'COMPLETED' } })
        const tournament = new Tournament()
        await tournament.findById(order.tournamentId)
        const team = await new Team().findByCode(order.teamCode)
        await tournament.addTeam(team.get())
      }
    }
  }
  res.send('OK')
}

async function redirectPayment(req, res) {
  res.send("OK :`) ")
}

router.get('/complete_order', redirectPayment);
router.post('/complete_order/:orderId', completeOrderHandler);
module.exports = router;
