const router = require('express').Router();
const { setPayment, setPaymentDetails } = require('../../../../lib/Adyen');
const District = require('../../../../../../brooklyn-server/abstractTypes/District');
const Tournament = require('../../../../Entities/Tournament');
const Order = require('../../../../Entities/Order');
const Team = require('../../../../Entities/Team');
/**
 * @type {import('@adyen/api-library').Stat}
 */
const ERROR_STATUS = ['Error', 'Cancelled', 'Refused']
/**
 * 
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 * @returns 
 */
async function initiatePayment(req, res) {
  const { orderId } = req.params
  const { STATE_DATA } = req.body
  if (!orderId || !STATE_DATA) {
    return res.status(400).json({ status: false, message: 'INVALID_REQUEST' })
  }
  const orderHandler = new Order()
  const tournamentEntity = new Tournament();

  let order = await orderHandler.findById(orderId)
  if (!order) {
    return res.status(404).json({ status: false, message: 'ORDER_NOT_FOUND' })
  }
  const confirmUrl = orderHandler.paymentCompleteOrderUrl();
  const returnUrl = await orderHandler.paymentReturnUrl();
  const teamHandler = new Team().set(await orderHandler.team())
  try {
    await tournamentEntity.findById(order.tournamentId)

    if (await tournamentEntity.hasTeam(teamHandler)){
      throw { message: 'Team already in tournmanet'}
    }
    if (tournamentEntity.get('status') === 'FULL'){
      throw { message: 'Tournament full'}
    }
   
    const club = await (new District().findByAlias(order.clubMerchantAlias))
    const paymentData = {
      price: Math.round(order.amount * 100),
      currency: order.currency,
      merchantAccount: club.adyenMerchantAccount,
      ...JSON.parse(STATE_DATA)
    }

    const paymentResponse = await setPayment(order, paymentData, confirmUrl, club)
    if (paymentResponse.resultCode === 'Refused') {

      order = await orderHandler.update({
        status: 'FAILED',
        reason: `${paymentResponse.refusalReason} - ${paymentResponse.refusalReasonCode}`,
        paymentReference: paymentResponse.pspReference || '',
      })

      return res.json({
        status: false,
        data: { paymentResponse, order, returnUrl }
      })

    } else if (paymentResponse.resultCode === 'IdentifyShopper') {
      const metadata = JSON.parse(order.metadata) || {}

      metadata.adyenResponse = {
        resultCode: paymentResponse.resultCode,
        details: paymentResponse.details,
        paymentData: paymentResponse.paymentData
      }

      order = await orderHandler.update({
        metadata: JSON.stringify(metadata),
        status: 'IN_PROGRESS',
      })

    } else if (paymentResponse.resultCode === 'Authorised') {
       order = await orderHandler.completeOrder('COMPLETED', {
        paymentReference: paymentResponse.pspReference || ''
      })
    }
    return res.json({
      status: true,
      data: { paymentResponse, order, returnUrl }
    })

  } catch (ex) {
    order = await orderHandler.update({ status: 'FAILED', reason: ex.responseBody || ex.message })

    return res.json({
      status: false,
      message: ex.toString(),
      data: {
        orderId,
        returnUrl,
        order,
      }
    })
  }
}

async function paymentAdditionalDetails(req, res) {
  const { orderId } = req.params
  const { DROPIN_DATA } = req.body

  const district = new District()
  const orderHandler = new Order()
  const tournamentHandler = new Tournament()
  let order = await orderHandler.findById(orderId);
  order = await orderHandler.get();
  const returnUrl = await orderHandler.paymentReturnUrl();
  const redirectFailedUrl = await orderHandler.redirectFailedUrl();
  if (!order) {
    res.json({ status: false, returnUrl: redirectFailedUrl})
    return
  }

  try {
    await tournamentHandler.findById(order.tournamentId);
    const club = await district.findByAlias(order.clubMerchantAlias)
    const stateData = JSON.parse(DROPIN_DATA)
    const paymentResponse = await setPaymentDetails(club, stateData)
    if (paymentResponse.resultCode === 'Authorised') {
      const result = await orderHandler.completeOrder('COMPLETED', {
        paymentReference: paymentResponse.pspReference
      })
      return res.json({
        status: true,
        data: { order: result, paymentResponse, returnUrl }
      })
    } else if (paymentResponse.resultCode === 'Refused') {

      order = await orderHandler.completeOrder('FAILED', {
        paymentReference: paymentResponse.pspReference,
        reason: paymentResponse.refusalReason
      })

      return res.json({
        status: false,
        data: { order, returnUrl }
      })
    }
    return res.json({
      status: true,
      data: { order, paymentResponse, returnUrl }
    })

  } catch (ex) {
    console.error("Error completing order", ex, ex.message || ex.responseBody || ex.response.data)

    order = await orderHandler.completeOrder('FAILED', {
      reason: ex.responseBody || ex.message || JSON.stringify(ex.response.data)
    })

    return res.json({
      status: false,
      data: {
        order,
        orderId,
        returnUrl
      }
    })
  }
}

async function completeOrderRequest(req, res) {
  const { orderId } = req.params
  const reqParams = req.body || req.query.params
  const orderHandler = new Order()
  const redirectFailedUrl = await orderHandler.redirectFailedUrl()

  try {
    await orderHandler.findById(orderId)
    let order = orderHandler.get();
    if (!order) {
      throw new Error('Order not found')
    }
    const returnUrl = await orderHandler.paymentReturnUrl()

    await completeOrder(order, reqParams)

    res.redirect(returnUrl)
  } catch (err) {
    res.redirect(redirectFailedUrl)
  }
}

/**
 * @param {string} orderId 
 * @param {any} additionalPaymentDetail 
 * @returns 
 */
async function completeOrder(order, additionalPaymentDetail) {

  const orderHandler = new Order().set(order);
  const tournament = new Tournament()
  const districtEntity = new District();

  await tournament.findById(order.tournamentId)
  const teamHandler = new Team().set(await orderHandler.team())
  const district = await districtEntity.findByAlias(order.clubMerchantAlias)
  const returnUrl = await orderHandler.paymentReturnUrl();
  try {
    console.log('Executing completeOrder on ', order.id)
    if (tournament.get('status') === 'FULL') {
      console.log(`Tournament:${order.tournamentId} FULL`)

      await orderHandler.completeOrder('FAILED', {
        reason: `Tournament full`
      })
    } else if (await tournamentEntity.hasTeam(teamHandler)){
      await orderHandler.completeOrder('FAILED', {
        reason: 'Team already in tournmanet'
      })
      
    } else {
      if (order.metadata) {

        const details = {}
        const metadata = JSON.parse(order.metadata)
        for (const param of metadata.adyenResponse.details) {
          details[param.key] = additionalPaymentDetail[param.key]
        }
        const paymentData = metadata.adyenResponse.paymentData
        const paymentResponse = await setPaymentDetails(district, { details, paymentData })

        if (paymentResponse.resultCode === 'Authorised') {

          await orderHandler.completeOrder('COMPLETED', {
            paymentReference: paymentResponse.pspReference || ''
          })

        } else if (ERROR_STATUS.includes(paymentResponse.resultCode)) {
          console.log('Order failed - Refused', order.id, paymentResponse.refusalReason)

          await orderHandler.completeOrder('FAILED', {
            reason: `${paymentResponse.refusalReason}-${paymentResponse.refusalReasonCode}`
          })
        } else {
          await orderHandler.update({
            status: "IN_PROGRESS"
          })
        }
      }
    }
  } catch (error) {
    // TODO REFUND SI SE COMPLETÓ Y HA FALLADO
    let errMessage = JSON.stringify(error.message)
    if (error.response && error.response.data) {
      errMessage = error.response.data.reason || JSON.stringify(error.response.data)
    }
    await orderHandler.completeOrder('FAILED', {
      reason: errMessage
    })
    console.error(`Error trying to complete order, check for refund :${order.id}`, errMessage);
    res.json({
      status: false,
      data: {
        returnUrl,
        order
      }
    });
  }

}

router.post('/initiate/:orderId', initiatePayment)
router.post('/details/:orderId', paymentAdditionalDetails)
router.get('/complete_order/:orderId', completeOrderRequest)
router.post('/complete_order/:orderId', completeOrderRequest)

module.exports = router