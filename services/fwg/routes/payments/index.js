const router = require('express').Router();
const { prisma: mongodb } = require('../../../../datasources/mongodb/generated/prisma-client')
const Tournament = require('../../../../Entities/Tournament')
const { Fitboxer } = require('../../../../Entities/User')
const District = require('../../../../../../brooklyn-server/abstractTypes/District')
const Adyen = require('../../../../lib/Adyen');

const { isAuth } = require('../../../../middleware/permissions');
const Order = require('../../../../Entities/Order');
const Team = require('../../../../Entities/Team');

/**
 * @param {String} teamId
 * @param {string} clubAlias
 */
async function isValidCountry(teamId, clubAlias) {
  const teamHandler = new Team()
  const districtHandler = new District()

  await teamHandler.findById(teamId)
  const clubTournament = await districtHandler.findByAlias(clubAlias)
  const teamClub = await teamHandler.club()

  return clubTournament.ShippingCountry === teamClub.ShippingCountry;
}

async function createTournamentOrder(req, res) {
  const { user } = req.session
  if (!user) {
    res.status(400).send('Auth Required')
    return
  }
  const { id } = req.params
  const { id: fitboxerId } = user
  const { firstName, lastName, email, phone, userToken } = req.body
  const tournament = new Tournament({ mongodb });
  const districtEntity = new District()
  const fitboxer = new Fitboxer()
  const teamHandler =  new Team();

  await fitboxer.findById(fitboxerId)
  await tournament.findById(id)
  if (!tournament.get()) {
    return res.status(404).json({
      status: false,
      message: 'TOURNAMENT_NOT_FOUND'
    })
  }
  const { clubMerchantAlias, currency, price, id: tournamentId } = tournament.get()
  const district = await districtEntity.findByAlias(clubMerchantAlias)
  const { paymentMethod } = district
  let paymentInfo = {}

  if (paymentMethod === 'A') {
    paymentInfo = await Adyen.getPaymentMethods(district, fitboxerId, currency, price)
  } else {
    console.error(`No payment method defined for district: ${district.alias}`)
    return res.status(500).json({
      status: false,
      message: `No payment method defined for district: ${district.alias}`
    })
  }
  if (!fitboxer.get()) {
    return {
      "errors": [
        { "message": 'FITBOXER_NOT_FOUND' }
      ]
    }
  }
  const team = await fitboxer.team()
  if (!team) {
    return res.status(404).json({
      status: false,
      message: 'USER_WITHOUT_TEAM'
    })
  }
  teamHandler.set(team)

  let order = await teamHandler.orderAvailable(tournament.get('id'))
  if (order) {
    res.send({ paymentMethod, paymentInfo, order })
    return;
  }
  if (tournament.get('final') && !order) {
    return res.status(403).json({
      status: false,
      message: 'NOT_VALID_ORDER'
    })
  }
  if (!isValidCountry(team.id, tournament.get('clubAlias'))) {
    return res.status(403).json({
      status: false,
      message: 'INVALID_COUNTRY'
    })
  }

  if (await tournament.isTournamentLocked(fitboxer.get())) {
    return res.status(403).json({
      status: false,
      message: 'TOURNAMENT_BLOCKED'
    })
  }

  // if (await tournament.isTeamLocked(team)) {
  //   return res.status(403).json({
  //     status: false,
  //     message: 'TEAM_IN_PROGRESS'
  //   })
  // }

  if (tournament.get('status') === 'FULL') {

    return res.status(403).json({
      status: false,
      message: 'TOURNAMENT_FULL'
    })
  }



  order = await mongodb.createOrder({
    tournamentId,
    paymentMethod,
    currency: currency,
    amount: price,
    firstName,
    lastName,
    email,
    phone,
    status: "IN_PROGRESS",
    userToken,
    teamCode: team.code,
    clubMerchantAlias: clubMerchantAlias
  })
  tournament.lockTournament(user, team)
  tournament.lockTeam(team)


  res.send({ paymentMethod, paymentInfo, order })
}

async function orderError(req, res) {
  const { orderId } = req.params
  if (!orderId) {
    res.status(400).send({ status: false, message: 'INVALID_REQUEST' })
    return
  }
  if (req.body.error && req.body.error.data) {


    let errorMessage = JSON.stringify(req.body.error.data.response || req.body.error.data.response)


    const order = await mongodb.order({ id: orderId })
    if (!order) {
      res.status(404).send({ status: false, message: 'ORDER_NOT_FOUND' })
      return
    }
    await mongodb.updateOrder({
      where: { id: orderId },
      data: { status: 'FAILED', reason: errorMessage }
    })
    res.send(order)
  } else {
    res.status(500).send(req.body)
  }
}

const orderPaymentInfo = async function (req, res) {
  const { id } = req.params
  const { key } = req.query
  if (key !== 'OPVuv30GQY99z9hG') throw new Error('INVALID_KEY')
  const orderEntity = new Order();
  const order = await orderEntity.findById(id)
  if (!order) {
    res.status(404).send({ status: false, message: 'ORDER_NOT_FOUND' });
    return;
  }
  if (!order.userToken || order.userToken !== req.sessionID) {
    order.userToken = req.sessionID
    order = await orderEntity.update({
      userToken: req.sessionID
    })
  }
  if (order.expirationDate && new Date().getTime() >= new Date(order.expirationDate).getTime()) {
    res.status(403).send({ status: false, message: 'ORDER_EXPIRED' });
    return;
  }
  const districtEntity = new District();
  const district = await districtEntity.findByAlias(order.clubMerchantAlias)
  const teamEntity = new Team();
  await teamEntity.findByCode(order.teamCode)
  const captain = await teamEntity.captain();
  if (captain.id !== req.session.user.id) {
    res.status(403).send({ status: false, message: `Payment not allowed for user ${req.session.user.email} on team:${order.teamCode}  ` });
    return;
  }
  const { paymentMethod } = district
  if (paymentMethod === 'A') {
    const paymentInfo = await Adyen.getPaymentMethods(district, captain.id, order.currency, order.amount)
    return res.send({ paymentMethod, paymentInfo, order });
  } else {
    console.error(`No payment method defined for district: ${district.alias}`)
    return res.status(500).json({
      status: false,
      message: `No payment method defined for district: ${district.alias}`
    })
  }
}

async function orderStatus(req, res) {
  try {
    const { id } = req.params;
    const order = await mongodb.order({ id });
    return res.status(200).json(order);
  } catch (ex) {
    console.error(ex)
    return res.status(200).json({
      status: false,
      message: ex.toString()
    })
  }
}


router.post('/tournament/:id', isAuth, createTournamentOrder)
router.get('/order/:id/status', orderStatus)
router.get('/order/:id', isAuth, orderPaymentInfo)
router.post('/order_error/:orderId', orderError)
module.exports = router