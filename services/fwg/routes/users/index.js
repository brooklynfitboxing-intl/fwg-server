const router = require('express').Router();

const { prisma: mongodb } = require('../../../../datasources/mongodb/generated/prisma-client')
const async = require('../../../../lib/asyncMiddleware')
const { isAuth, isAdmin } = require('../../../../middleware/permissions');
const AwsS3 = require('../../../../lib/AwsS3');
const { Trainer, Fitboxer, User } = require('../../../../Entities/User')
const awsS3 = AwsS3.init(process.env)

const userInfo = async (req, res) => {
  const { id } = req.session.user
  const token = req.sessionID
  const user = await mongodb.user({ id })
  const userHandler =  new User().set(user)
  delete user.password
  let teamInfo = null
  const team = (await mongodb.teams({ where: { fitboxers_some: { id } } }))[0]
  if (team) {
    teamInfo = team
  }

  return res.send({
    id,
    session_token: token,
    ...user,
    team: teamInfo,
    photo: userHandler.photo()
  })
}

const listTrainers = async (req, res) => {
  const trainers = await (new Trainer().findAll())
  return res.json(trainers)
}
const listFitboxers = async (req, res) => {
  const fitboxers = await (new Fitboxer().findAll())
  return res.json(fitboxers)
}

const updateProfilePhoto = async (req, res) => {
  const { id } = req.session.user
  const { file } = req
  const fileExt = file.originalname.split('.').pop();
  const destinationFile = `fitboxers/${id}.${fileExt}`;
  await awsS3.copyFile({
    sourceFile: file.originalname,
    sourceBucket: 'brooklyn-fwg',
    destinationBucket: 'brooklyn-fwg',
    destinationFile,
    deleteSource: true
  });

  await mongodb.updateUser({
    where: { id },
    data: {
      photo: destinationFile
    }
  })
  const url = awsS3.getUrlFromBucket(destinationFile)
  return res.send({ url })
};

const updateProfile = async (req, res) => {
  const { id } = req.session.user
  try {
    const user = mongodb.$exists.user({ id })
    const data = req.body
    const allowedTypes = ['FITBOXER', 'TRAINER']
    if (data.type && !allowedTypes.includes(data.type)) {
      return res.status(400).json({
        status: false,
        message: 'BAD_REQUEST'
      })
    }
    if (user) {
      const result = await mongodb.updateUser({
        where: { id },
        data
      })
      delete result.password
      return res.send(result)
    } else {
      return res.status(404).json({
        status: false,
        message: 'USER_NOT_FOUND'
      })
    }
  } catch (ex) {
    console.error(ex)
    return res.status(500).json({
      status: false,
      message: JSON.stringify(ex)
    })
  }

}
router.post('/profile-update-photo', [isAuth, awsS3.uploadMiddleware().single('file')], async(updateProfilePhoto))
router.post('/profile-update', isAuth, async(updateProfile))
router.get('/user-info', isAuth, async(userInfo))
router.get('/fitboxers', isAdmin, async(listFitboxers))
router.get('/trainers', isAdmin, async(listTrainers))

module.exports = router;