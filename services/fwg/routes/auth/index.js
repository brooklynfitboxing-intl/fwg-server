const crypto = require('crypto');
const router = require('express').Router();

const { Fitboxer, User } = require('../../../../Entities/User')
const { prisma } = require('../../../../datasources/mongodb/generated/prisma-client')
const async = require('../../../../lib/asyncMiddleware')
const { isAuth, isAdmin } = require('../../../../middleware/permissions')
const cache = require('../../../../lib/cache.store');
const Team = require('../../../../Entities/Team');

/*
 * Controllers
 */
async function checkEmail(req, res) {
  let { email, env } = req.params
  email = email.toLowerCase().trim()
  const user = new Fitboxer(Fitboxer.fakeCtx());
  let result = null

  try {
    await user.findByEmail(email)
    if (user.get() && user.get('id')) {
      res.send({ status: true, id: user.get('externalId'), v7_id: user.get('v7Id'), })
      return
    }
    result = await user.checkEmailBFI(email)
    return res.send(result)
  } catch (err) {
    console.error('Check email error', err)
    if (err.statusCode) {
      return res.status(err.statusCode).json(err)
    }
    return res.status(500).json({ status: false, message: err })
  }
}

async function useAutoLoginToken(req, res) {
  const { token } = req.params
  const data = await cache.get(`logintoken:${token}`)
  if (!data) return res.status(403).json({ message: 'TOKEN_INVALID_SECRET_KEY' })
  await cache.del(`logintoken:${token}`)
  res.json(JSON.parse(data))
}

async function signUp(req, res) {
  const allowedTypes = ['FITBOXER', 'TRAINER']
  try {
    const data = req.body
    const requiredfields = ['firstName', 'lastName', 'email', 'password', 'phone'];
    let missingFields = requiredfields.filter(it => (data[it] === undefined || data[it] === ""))
    if (missingFields.length) {
      throw { status: false, message: `[${missingFields.join(', ')}] field(s) must be specified` }
    }
    const {
      firstName,
      lastName,
      email,
      nickName,
      password,
      type,
      phone,
      locale,
      v7Id,
      externalId,
      env
    } = data
    const hashedPassword = crypto.createHash('sha256').update(password).digest('hex');
    const exists = await prisma.$exists.user({ email });
    if (exists) {
      return res.status(400).json({
        status: false,
        message: `User with email ${email} already exists`
      })
    }
    if (!allowedTypes.includes(type)) {
      throw { status: false, message: `User type:[${type}] Not Allowed` }
    }
    const { id } = await prisma.createUser({
      email: email.toLowerCase(),
      firstName,
      lastName,
      nickName,
      type,
      phone,
      password: hashedPassword,
      isCaptain: false,
      locale,
      v7Id,
      externalId,
      env,
      status: 'ACTIVE'
    })
    // Generar token
    let buffer = crypto.randomBytes(22)
    const token = buffer.toString('hex')
    await cache.set(`logintoken:${token}`, {
      email,
      password: hashedPassword,
    }, 3600)

    res.json({
      status: true,
      id,
      session_token: token
    })
  } catch (ex) {
    console.error(ex)
    if (ex.status === false) {
      return res.status(400).json({
        status: false,
        message: ex.message
      })
    }
    return res.status(500).json({
      status: false,
      message: ex.message
    })
  }
}

async function login(req, res) {

  let { email, password } = req.body;
  let lang = req.body.lang
  lang = (lang || 'es-es').substring(0, 2)
  if (!email) {
    return res.status(422).send({ message: "USER_MISSING_EMAIL" });
  }
  email = email.toLowerCase()
  const user = await prisma.user({ email: email });
  if (!user) {
    res.status(404).json({
      status: false,
      message: 'USER_NOT_FOUND'
    })
  } else {
    const { id, firstName, lastName, type, nickName, v7Id, externalId, env } = user
    const hashedPassword = crypto.createHash('sha256').update(password).digest('hex')
    const isMatch = hashedPassword === user.password
    if (isMatch) {
      if (type === 'TRAINER' && !user.verified) {
        return res.status(403).json({ status: false, message: 'TRAINER_NOT_VERIFIED' })
      }
      const token = req.sessionID

      req.session.isAuth = true
      req.session.user = { id, nickName, email, firstName, lastName, type, isCaptain: user.isCaptain, locale: user.locale }
      req.session.lang = lang
      req.session.currentRole = type
      req.session.userAgent = req.headers['user-agent']
      req.session.ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
      await cache.set(`logintoken:${token}`, {
        email,
      }, 3600)

      return res.json({
        id,
        session_token: token,
        email,
        lastName,
        firstName,
        name: `${user.firstName} ${user.lastName}`.trim().replace(/\s+/, ' '),
        photo: user.photo,
        phone: user.phone,
        type: user.type,
        locale: user.locale,
        v7Id,
        externalId,
        env
      })

    } else {

      return res.status(401).json({ message: "USER_WRONG_PASSWORD" })
    }
  }
}

async function logout(req, res) {
  await req.session.destroy(req.sessionID)
  res.send('OK')
}

async function verify(req, res) {
  const { id, email, name, avatar } = req.session.user
  const { permissions, currentRole } = req.session
  res.json({
    id,
    email,
    name,
    avatar,
    currentRole,
    permissions
  })
}

async function switchLangRoute(req, res) {
  // const id = req.session.user.id
  let lang = req.params.lang.substring(0, 2)

  const response = 1 //switchLang(id, lang)

  if (response) {
    req.session.lang = lang
    await req.session.save(req.sessionID, req.session)
    res.sendStatus(200)
  }
  else res.sendStatus(400)

}

async function switchRole(req, res) {
  const permissions = req.session.permissions
  const roleId = req.params.id
  const newRole = permissions.find((el) => el.id === roleId)
  if (newRole) {
    req.session.currentRole = newRole.id
    await req.session.save(req.sessionID, req.session)

    res.json(newRole)
  } else {
    res.send(403) // Forbidden
  }
}

/*
 * Email Management
 */

async function verifyEmail(req, res) {
  const email = req.body.email
  const user = req.session.user
  // Check email en myH4C
  const exists = await prisma.$exists.user({
    email
  })
  if (exists) {
    return res.status(403).json({ message: 'user.alreadyExists' })
  }
  const id = user.id
  res.json({ id })
}

async function changeEmailWithToken(req, res) {
  const { id, token } = req.params
  // Verificar Token
  const event = (await prisma.userEvents({
    where: {
      type: 'CHG_EMAIL',
      status: 202,
      token,
      user: {
        id
      }
    }
  }))[0]

  if (event) {
    if (event.tokenExpiration < new Date()) {
      return res.status(404).json({ message: 'user.tokenExpire' })
    }
    // Realizar Cambios
    const email = event.value
    const userFragment = `
      fragment UserData on User {
        id
        profile {
          id
          v7_id
        }
      }`

    const user = await prisma.user({ id }).$fragment(userFragment)
    if (user) {
      try {
        await prisma.updateUserEvent({
          where: { id: event.id },
          data: { status: 410 }
        })
        // Actualizar en Prisma
        await prisma.updateUser({
          where: { id },
          data: { email }
        })
        return res.json({ email })
      } catch (ex) {
        console.error(ex)
        return res.status(500).send({ message: 'user.cannotUpdated' })
      }
    }

  } else {
    return res.status(404).send({ message: 'user.tokenNotfound' })
  }
}

/*
 * Password Management
 */

async function changePassword(req, res) {
  const { oldPassword, newPassword, _key } = req.body
  if (_key !== 'f1074b9556a8efa4acf210f') {
    return res.status(403).send({ message: 'INVALID_SECRET_KEY' })
  }

  if (!newPassword || newPassword.length === 0) {
    return res.status(400).send({ message: 'USER_PASSWORD_MUST_BE_PROVIDED' })
  }

  const id = req.session.user.id
  const user = await prisma.user({ id });

  if (user) {
    // Check password
    const hashOldPassword = crypto.createHash('sha256').update(oldPassword).digest('hex');

    const isMatch = hashOldPassword === user.password
    let status = 200
    if (!isMatch) {
      status = 401
    }

    if (status === 200) {
      const hashedPassword = crypto.createHash('sha256').update(newPassword).digest('hex');
      const updated = await prisma.updateUser({
        where: { id },
        data: { password: hashedPassword }
      })
      if (updated) {
        return res.send({ status: true, message: 'USER_PASSWORD_CHANGED_OK' })
      } else {
        return res.status(status).send({ status: false, message: 'USER_PASSWORD_CANNOT_BE_UPDATED' })
      }
    } else {
      return res.status(status).send({ status: false, message: "USER_WRONG_PASSWORD" })
    }
  }
}

async function resetPassword(req, res) {
  const { email } = req.params

  const user = await prisma.user({ email })

  if (!user) {
    return res.status(404).json({ message: 'USER_NOT_FOUND' })
  }

  let buffer = crypto.randomBytes(22)
  const token = buffer.toString('hex')
  const tokenExpiration = 60 * 60 * 24
  const id = user.id
  await cache.setCache(`:resetpass:${token}`, {
    id: user.id,
    email: user.email,
  }, tokenExpiration)

  res.json({ id, token })
}

async function checkResetPasswordToken(req, res) {
  const { token, password } = req.body
  const cacheData = await cache.getCache(`:resetpass:${token}`)
  if (cacheData) {
    const { id } = JSON.parse(cacheData)
    if (!password || password.length === 0) {
      return res.status(400).send({ message: 'USER_PASSWORD_MUST_BE_PROVIDED' })
    }
    const hashedPassword = crypto.createHash('sha256').update(password).digest('hex');
    try {
      await prisma.updateUser({
        where: { id },
        data: { password: hashedPassword }
      })
      await cache.delCache(`:resetpass:${token}`)
      res.send({ status: true, message: 'USER_PASSWORD_CHANGED_OK' })
    } catch (err) {
      console.error(err)
      res.status(500).json({ status: false, message: 'USER_PASSWORD_CANNOT_BE_UPDATED' })
    }
  } else {
    res.status(403).json({ status: false, message: 'INVALID_TOKEN' })

  }
}

async function setNewPassword(req, res) {
  const { id, _key, password } = req.body
  if (!password || password.length === 0) {
    return res.status(400).send({ message: 'USER_PASSWORD_MUST_BE_PROVIDED' })
  }
  if (_key !== 'f1074b9556a8efa4acf210f') {
    return res.status(403).send({ message: 'INVALID_SECRET_KEY' })
  }
  const hashedPassword = crypto.createHash('sha256').update(password).digest('hex');
  const updated = await prisma.updateUser({
    where: { id },
    data: { password: hashedPassword }
  })
  if (updated) {
    res.send({ status: true, message: 'USER_PASSWORD_CHANGED_OK' })
  } else {
    res.status(500).json({ status: false, message: 'USER_PASSWORD_CANNOT_BE_UPDATED' })
  }

}

/*
 * Router
 */
router.post('/signup', async(signUp))
router.post('/login', async(login));
router.post('/logout', isAuth, async(logout))

router.get('/verify', isAuth, async(verify))
router.get('/switch_lang/:lang', isAuth, async(switchLangRoute))
router.get('/switch_role/:id', isAuth, async(switchRole))

router.post('/verify_email_change/', isAuth, async(verifyEmail))
router.get('/change_email_with_token/:id/:token', async(changeEmailWithToken))
router.get('/check_email/:env/:email', async(checkEmail))

router.get('/reset_password/:email', async(resetPassword))
router.post('/reset_password_token', async(checkResetPasswordToken));
router.post('/set_new_password', async(setNewPassword));
router.post('/change_password', isAuth, async(changePassword));


router.post('/generate_pass/:pass', isAdmin, (req, res) => {
  const { pass } = req.params
  const hex = crypto.createHash('sha256').update(pass).digest('hex');
  res.send({ hex })
});

router.get('/autologin/:token', async(useAutoLoginToken))

module.exports = router;