const router = require('express').Router();
const { prisma: mongodb } = require('../../../../datasources/mongodb/generated/prisma-client')
const AwsS3 = require('../../../../lib/AwsS3')
const async = require('../../../../lib/asyncMiddleware')
const { isTrainer, isFitboxer, isAuth, isTrainerOrCaptain } = require('../../../../middleware/permissions')

const Team = require('../../../../Entities/Team')
const { Trainer, Fitboxer } = require('../../../../Entities/User');
const { Fitboxer: FitboxerBrk, District } = require('../../../../../../brooklyn-server/abstractTypes');
const Tournament = require('../../../../Entities/Tournament');

const awsS3 = AwsS3.init(process.env)
const MAX_TEAM_SIZE = 5;
const BUCKET = 'brooklyn-fwg';
// Deprecated, use Team entity instead
const TeamWithFitboxersAndTrainerFragment = `fragment TeamWithFitboxersAndTrainerD on Team {
  id
  name
  code
  logo
  trainer:createdBy {
    id
    email
    firstName
    lastName
  }
  fitboxers {
    id
    nickName
    firstName
    lastName
    isCaptain
    email
    photo
  }
}`
/**
 * @param {object} req 
 * @param {object} res 
 */
const teamUpdateLogo = async (req, res) => {
  const { file } = req
  const { code } = req.params
  const team = new Team();
  if (!file) {
    return res.status(400).json({ status: false, message: 'TEAM_LOGO_MUST_BE_PROVIDED' })
  }
  if (!code) {
    return res.status(400).json({ status: false, message: 'TEAM_CODE_MUST_BE_PROVIDED' })
  }
  await team.findByCode(code)
  if (!team.get()) {
    return res.status(404)({ status: false, message: 'TEAM_CODE_NOT_FOUND' })
  }
  const fileExt = file.originalname.split('.').pop();
  const destinationFile = `teams/${team.get('id')}.${fileExt}`;
  await awsS3.copyFile({
    sourceFile: file.originalname,
    sourceBucket: BUCKET,
    destinationBucket: BUCKET,
    destinationFile,
    deleteSource: true
  });
  const url = awsS3.getUrlFromBucket(destinationFile)
  await mongodb.updateTeam({
    where: { code },
    data: {
      logo: destinationFile
    }
  })
  return res.send({ url })
}



const teamRegistration = async (req, res) => {
  const { name, motto, userId } = req.body
  const { code } = req.params
  const fitboxerId = userId || req.session.user.id
  try {
    const team = await (new Team().findByCode(code))
    if (!team.get()) {
      return res.status(404).json({
        status: false,
        message: 'TEAM_CODE_NOT_FOUND'
      })
    }
    const fitboxers = await team.fitboxers()

    if (fitboxers.length === MAX_TEAM_SIZE) {
      return res.json({
        status: false,
        message: 'TEAM_REACHED_MAX_SIZE'
      })
    }
    if (!name) {
      return res.status(400).send({ status: false, message: 'TEAM_NAME_MUST_BE_PROVIDED' })
    }
    const fitboxerHandler = await (new Fitboxer().findById(fitboxerId))
    if (!fitboxerHandler.get()) {
      throw new Error(`Fitboxer not found: ${fitboxerHandler.get('email')}`)
    }
    await team.addFitboxer(fitboxerHandler.get('email'))
    await team.updateTeam(name, motto)
    await team.refresh()
    await fitboxerHandler.setTeam(team.get('id'))
    if (fitboxers.length === 0) {
      await fitboxerHandler.setCaptain()

      req.session.user = { ...req.session.user, isCaptain: true }
      req.session.save(req.sessionID, req.session)

      try {
        const fbBrooklyn = await new FitboxerBrk().findById(fitboxerHandler.get('externalId'))
        if (fbBrooklyn) {
          const club = await new District().findByV7Id(fbBrooklyn.district)
          team.update({
            clubId: club.id,
            city: club.ShippingCity
          })
        }
      } catch (err) {
        console.error(`Error setting city on team: ${team.get('code')}`)
      }
    }
    res.send({ status: true, message: 'FITBOXER_ADDED_TO_TEAM', result: team.get() });
  } catch (err) {
    if (err.statusCode) {
      res.status(err.statusCode).send({ status: false, message: err.message })
      return
    }
    return res.status(500).send({ message: err.message })
  }
}
const teamUpdate = async (req, res) => {
  const { name, motto, trainerEmail } = req.body
  const { code } = req.params
  const team = await (new Team().findByCode(code))

  try {
    if (!team.get()) {
      return res.status(404).json({
        status: false,
        message: 'TEAM_NOT_FOUND'
      })
    }
    let trainerId = null
    if (trainerEmail) {
      const trainer = await new Trainer().findByEmail(trainerEmail)
      if (!trainer.get()) {
        return res.status(404).json({
          status: false,
          message: 'TRAINER_NOT_FOUND'
        })
      }
      trainerId = trainer.email !== team.get('trainer').email ? trainer.id : null
    }
    await team.updateTeam(name, motto, trainerId)
    await team.refresh();
    return res.send(team.get());
  } catch (err) {
    console.error(err)
    if (err.statusCode) {
      return res.status(err.statusCode).send({ status: false, message: err.message })
    }
    return res.status(500).send({ message: err.message })
  }
}

const addFitboxerToTeam = async function (req, res) {
  const { code } = req.params
  const { userId } = req.body
  try {
    const team = await (new Team().findByCode(code))
    if (!team.get()) {
      return res.status(404).json({
        status: false,
        message: 'TEAM_CODE_NOT_FOUND'
      })
    }
    const fitboxers = await team.fitboxers()

    if (fitboxers.length === MAX_TEAM_SIZE) {
      return res.json({
        status: false,
        message: 'TEAM_REACHED_MAX_SIZE'
      })
    }
    const fitboxer = await new Fitboxer().findById(userId);
    await team.addFitboxer(fitboxer.get('email'))
    await team.refresh()
    return res.send(team.get())
  } catch (err) {
    console.error(err)
    res.status(500).json({ message: err })

  }

}

const removeFitboxer = async function (req, res) {
  const { code } = req.params
  const { userId } = req.body
  try {
    const team = await (new Team().findByCode(code))
    if (!team.get()) {
      return res.status(404).json({
        status: false,
        message: 'TEAM_CODE_NOT_FOUND'
      })
    }
    await team.removeFitboxer(userId)
    await team.refresh()
    return res.send(team.get())
  } catch (err) {
    console.error(err)
    if (err.statusCode) {
      return res.status(err.statusCode).json(err)
    }
    res.status(500).json({ message: err })
  }
}

const teamList = async (req, res) => {
  const teamList = await new Team().findAll()

  return res.send(teamList);
}


const teamOfTrainer = async (req, res) => {
  const { trainerId } = req.params
  const teamList = (await mongodb.teams({ where: { createdBy: { id: trainerId } } }).$fragment(TeamWithFitboxersAndTrainerFragment)).map(({ id, name, fitboxers, code, createdBy: trainer }) => {
    return {
      id,
      name,
      code,
      size: fitboxers.length,
      trainer,
      fitboxers
    }
  })
  return res.send(teamList);
}

const verifyTeamCode = async (req, res) => {
  const { code } = req.params

  const team = await new Team().findByCode(code)
  if (!team.get()) {
    return res.status(404).json({
      status: false,
      message: 'TEAM_CODE_NOT_FOUND'
    })
  } else {
    return res.send({
      status: true,
      teamInfo: team.get()
    })
  }
}


const teamInfo = async (req, res) => {
  const { code } = req.params
  const team = new Team()
  if (!code) {
    const teamInfo = await team.findByFitboxerId(req.session.user.id)
    team.set(teamInfo)
  } else {
    await team.findByCode(code)
  }
  if (!team.get()) {
    return res.status(404).send({
      status: false,
      message: 'TEAM_NOT_FOUND'
    })
  }
  const { ShippingCountryCode: countryCode, alias } = await team.club()
  await team.include(['trainer', 'fitboxers'])

  delete team.get('trainer').password
  team.get('fitboxers').every(fb => {
    delete fb.password
  })

  const response = {
    status: true,
    teamInfo: {
      ...team.get(),
      logo: await team.logo(),
      club: {
        alias,
        countryCode
      }
    },
  };
  let order = null
  const captain = await team.captain()
  if (req.session && req.session.user && captain && captain.id === req.session.user.id) {
    const tournamentEntity = new Tournament();
    const tournament = (await tournamentEntity.find({ final: true }))[0]
    if (tournament) {
      order = await team.orderAvailable(tournament.id);
      if (order) {
        response.teamInfo.orderId = order.id
        response.teamInfo.tournamentId = tournament.id
      }
    }
  }
  return res.send(response)

}


const createTeamCode = async (req, res) => {
  try {
    const trainerId = req.session.user.id
    const teamCode = await (new Team().createCode(trainerId))
    return res.send(teamCode)
  } catch (err) {
    delete err.result.status
    return res.status(500).json(err)
  }
}

const deleteCode = async (req, res) => {
  try {
    const { code } = req.params
    if (!code)
      return res.status(422).json({ message: 'NO_CODE_PROVIDED' })

    const team = new Team()
    await team.findByCode(code)
    if (!team.get()) {
      return res.status(404).json({
        status: false,
        message: 'TEAM_CODE_NOT_FOUND'
      })
    }
    await team.include('fitboxers')
    if (team.get().fitboxers.length) {
      return res.send({
        status: false,
        message: 'TEAM_CODE_IN_USE'
      })
    }
    await team.delete()
    return res.send({
      id: team.get('id'),
      code
    })
  } catch (err) {
    console.error(err)
    return res.status(500).json(err)
  }
}


/*
 * Router
 */
router.get('/info/:code?', isAuth, async(teamInfo))
router.post('/create-code', isTrainer, async(createTeamCode))
router.get('/verify-code/:code', isAuth, async(verifyTeamCode))
router.post('/delete-code/:code', isTrainer, async(deleteCode))
router.post('/update/:code', isTrainerOrCaptain, async(teamUpdate))
router.post('/upload-logo/:code', [isTrainerOrCaptain, awsS3.uploadMiddleware().single('file')], async(teamUpdateLogo))

router.get('/team-list', isTrainer, async(teamList))
router.get('/team-list/:trainerId', isTrainer, async(teamOfTrainer))
router.post('/team-registration/:code', isFitboxer, async(teamRegistration))

router.post('/:code/add-fitboxer', isAuth, async(addFitboxerToTeam))
router.post('/:code/remove-fitboxer', isTrainerOrCaptain, async(removeFitboxer))

module.exports = router;