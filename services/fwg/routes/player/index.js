const router = require('express').Router()

const async = require('../../../../lib/asyncMiddleware')

const Tournament = require('../../../../Entities/Tournament')
const Team = require('../../../../Entities/Team')
const Combat = require('../../../../Entities/Combat')

async function getCombats(req, res) {
  try {
    const id = req.params.tournamentId
    const ctx = { req }
    const handler = new Tournament(ctx)
    await handler.findById(id)
    if (!handler.get()) {
      res.status(404).json({message: `Tournament ${id} not found`})
      return
    }
    const combats = await handler.combats(['P', 'C'])
    const phasesList = await handler.phases()
    if(combats.length === 0 ){
      res.json([])
      return
    }
    const phase = phasesList.find(it => it.id === combats[0].phase)
    let arenaSize = /* phase.arenaQty || */ handler.get('arenaQty') || 1
    const result = []
    let i = 0;
    const arenaSizeMap = {
      SEMIFINALS: 1,
      FINALS: 1
    }
    arenaSize = arenaSizeMap[phase.name] ? arenaSizeMap[phase.name] : arenaSize
    const teamHandler1 = new Team()
    const teamHandler2 = new Team()
    for (const combat of combats) {
      if (i === arenaSize) {
        break;
      }
      const combatHandler = new Combat()
      await combatHandler.findById(combat.id)
      await combatHandler.include(['participants', 'team1', 'team2', 'phase'])
      const team1 = await teamHandler1.set(combatHandler.get('team1')).include('logo')
      const team2 = await teamHandler2.set(combatHandler.get('team2')).include('logo')

      const combatDto = { 
        ...combatHandler.get(),
        team1,
        team2
      }

      result.push(combatDto)
      i++;
    }
    result.sort((a, b) => a.arena - b.arena)

    return res.json(result)
  } catch (ex) {
    console.error(ex)
    return res.status(500).json({ message: ex.message })
  }
}
// async function getParticipants(req, res) {
//   try {
//     const { combatId } = req.params
//     const handler = new Combat()
//     await handler.findById(combatId)
//     const result = await handler.participants()
//     result.team1.forEach(el => el.nick = el.name)
//     result.team2.forEach(el => el.nick = el.name)
//     const team1 = result.team1.filter(el => el.bag > 0).sort((a, b) => a.bag - b.bag)
//     const team2 = result.team2.filter(el => el.bag > 0).sort((a, b) => a.bag - b.bag)
//     return res.json({ team1, team2 })
//   } catch (ex) {
//     console.error(ex)
//     return res.status(500).json({ message: ex.message })
//   }
// }
async function getParticipants(req, res) {
  try {
    const { combatId } = req.params
    const handler = new Combat()
    await handler.findById(combatId)
    const result = await handler.participants()
    result.team1.forEach(el => el.nick = el.name)
    result.team2.forEach(el => el.nick = el.name)
    const team1 = result.team1.filter(el => el.bag > 0).sort((a, b) => a.bag - b.bag)
    const team2 = result.team2.filter(el => el.bag > 0).sort((a, b) => a.bag - b.bag)

    return res.json({ team1, team2 })
  } catch (ex) {
    console.error(ex)
    return res.status(500).json({ message: ex.message })
  }
}

async function getScores(req, res) {
  try {
    const { combatTime, comboNumber, tournamentId } = req.params
    const handler = new Combat()
    const combats = await handler.findByTime(tournamentId, parseInt(combatTime))
    const result = [];
    await Promise.all(combats.map(async combat => {
      let currentCombo
      const handlerCombat = new Combat()
      await handlerCombat.findById(combat.id)
      const summary = await handlerCombat.scoresSummary()
      if (comboNumber) {
        currentCombo = await handlerCombat.scoresSummary(parseInt(comboNumber))
        currentCombo.arena = combat.arena
      }
      const participants = await handlerCombat.participants();
      const [team1Handler, team2Handler ] = [new Team().set(currentCombo.team1), new Team().set(currentCombo.team2)]
      const team1 =  await team1Handler.set(handlerCombat.get('team1')).include('logo')
      const team2 =  await team2Handler.set(handlerCombat.get('team1')).include('logo')
      
      currentCombo.team1 = { ...currentCombo.team1, logo: team1.logo };
      currentCombo.team2 = { ...currentCombo.team2, logo: team2.logo };
      
      summary.team1 = { ...summary.team1, logo: team1.logo };
      summary.team2 = { ...summary.team2, logo: team2.logo };

      result.push({ summary, arena: combat.arena, currentCombo, winner: handlerCombat.get('winner') || null, participants, team1, team2 })
    }))
    if(comboNumber){
      result.sort((a,b) => a.currentCombo.arena - b.currentCombo.arena)
    }
    return res.json(result)
  } catch (ex) {
    console.error(ex)
    return res.status(500).json({ message: ex.message })
  }
}

async function setGame(req, res) {
  try {
    const { combatTime, tournamentId, game } = req.params
    const handler = new Combat()
    const combats = (await handler.findByTime(tournamentId, parseInt(combatTime)))
    if (combats.length === 0) {
      res.status(404).json({ status: false, message: `No Combats found for ${tournamentId}, at time:${combatTime} not found` })
      return
    }
    for (const combat of combats) {
      handler.set(combat)
      await handler.update({ game, status: 'C' })
    }
    return res.json(true)
  } catch (ex) {
    console.error(ex)
    return res.status(500).json({ message: ex.message })
  }
}

async function setResults(req, res) {
  try {
    const { combatTime, tournamentId } = req.params;
    const combatsHandler = new Combat()
    const combats = await combatsHandler.findByTime(tournamentId, parseInt(combatTime))
    if (combats.length === 0) {
      res.status(404).json({ status: false, message: `No Combats found for this time: ${combatTime}, tournamentId: ${tournamentId}` })
      return
    }
    let { round, scores } = req.body
    console.log(`tournament: :${tournamentId}, time:${combatTime}, combo:${round} Setting results for combats: `, combats.map(it=> ({ id: it.id, arena: it.arena, time:it.time})))
    scores = JSON.parse(scores)
    scores.sort((a, b) => a.sb - b.sb)
    console.log(combats.length)
    await Promise.all(combats.map( (combat) => {
      const handler = new Combat()
      handler.set(combat)
      return handler.setResults(parseInt(round), scores)
    }))
    return res.json('OK')
  } catch (ex) {
    console.error(ex)
    return res.status(500).json({ message: ex.message })
  }
}
router.get('/participants/:combatId', async(getParticipants))
router.get('/:tournamentId/combats/', async(getCombats))
router.get('/:tournamentId/participants/:combatId', async(getParticipants))

// router.post('/results/:combatId/:arenaID?', async(setResults))

router.post('/:tournamentId/results/:combatTime', async(setResults))
router.post('/:tournamentId/game/:combatTime/:game', async(setGame))

router.get('/:tournamentId/scores/:combatTime', async(getScores))
router.get('/:tournamentId/scores/:combatTime/:comboNumber', async(getScores))

module.exports = router;  