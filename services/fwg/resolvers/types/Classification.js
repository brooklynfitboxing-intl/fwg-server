const Classification = require('../../../../Entities/Classification')

const Type = {
  tournament: async(_parent, args, ctx) => {
    const handler = new Classification(ctx).set(_parent)
    return handler.tournament()
  },

  groupId: async(_parent) => {
    return _parent.group
  },

  group: async(_parent, args, ctx) => {
    if (!_parent.group) return null
    const handler = new Classification(ctx).set(_parent)
    return handler.group()
  },

  phaseId: async(_parent) => {
    return _parent.phase
  },

  phase: async(_parent, args, ctx) => {
    const handler = new Classification(ctx).set(_parent)
    return handler.phase()
  },

  team: async(_parent, args, ctx) => {
    const handler = new Classification(ctx).set(_parent)
    return handler.team()
  }
}

module.exports = {
  ...Type
}