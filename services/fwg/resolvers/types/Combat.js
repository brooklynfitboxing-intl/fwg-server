const Combat = require('../../../../Entities/Combat')

const Type = {
  arena: async (_parent) => {
    return _parent.arena || 0
  },
  tournament: async(_parent, args, ctx) => {
    const handler = new Combat(ctx).set(_parent)
    return handler.tournament()
  },

  time: async(_parent) => {
    const time = _parent.time.toString().padStart(4, "0");
    const timeStr =`${time.substr(0,2)}:${time.substr(2,2)}` 
    return timeStr
  },

  groupId: async(_parent) => {
    return _parent.group
  },

  group: async(_parent, args, ctx) => {
    if (!_parent.group) return null
    const handler = new Combat(ctx).set(_parent)
    return handler.group()
  },

  phaseId: async(_parent) => {
    return _parent.phase
  },

  phase: async(_parent, args, ctx) => {
    const handler = new Combat(ctx).set(_parent)
    return handler.phase()
  },

  team1: async(_parent, args, ctx) => {
    const handler = new Combat(ctx).set(_parent)
    return handler.team1()
  },

  team1Desc: async (_parent, args, ctx) => {
    if (!_parent.team1Pos) return null
    const handler = new Combat(ctx).set(_parent)
    return handler.teamDesc('team1')
  },

  team2Desc: async (_parent, args, ctx) => {
    if (!_parent.team1Pos) return null
    const handler = new Combat(ctx).set(_parent)
    return handler.teamDesc('team2')
  },

  team2: async(_parent, args, ctx) => {
    const handler = new Combat(ctx).set(_parent)
    return handler.team2()
  },

  participants: async(_parent, args, ctx) => {
    const handler = new Combat(ctx).set(_parent)
    return handler.participants()
  },
  referees: async(_parent, args, ctx) => {
    const handler = new Combat(ctx).set(_parent)
    return handler.referees()
  },

  scoresData: async(_parent, args, ctx) => {
    const handler = new Combat(ctx).set(_parent)
    const scores = await handler.scores()
    return scores
  },
}

module.exports = {
  ...Type
}