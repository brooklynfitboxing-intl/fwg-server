const Team = require('../../../../Entities/Team')

const Type = {
  fitboxers: async(_parent, args, ctx) => {
    const team = new Team(ctx).set(_parent)
    return team.fitboxers()
  },

  clubAlias: async(_parent, args, ctx) => {
    const team = new Team(ctx).set(_parent)
    return (await team.club()).alias
  },

  captain: async(_parent, args, ctx) => {
    const team = new Team(ctx).set(_parent)
    return team.captain()
  },
  
  trainer: async(_parent, args, ctx) => {
    const team = new Team(ctx).set(_parent)
    return team.trainer()
  },
  
  logo: async(_parent, args, ctx) => {
    const team = new Team(ctx).set(_parent)
    return team.logo()
  },

  tournament: async(_parent, args, ctx) => {
    const { mongodb } = ctx
    const team = new Team(ctx).set(_parent)
  
    const tournament = (await mongodb.tournaments({ where: {
      final: true
    }}) )[0];

    const order = (await mongodb.orders({
      where: {
        teamCode: _parent.code,
        tournamentId: tournament.id,
        status_in: ['CREATED', 'IN_PROGRESS'],
        expirationDate_gte: new Date()
      }
    }))[0]

    if (order) {
      return tournament
    }
    return team.tournament()
  },
}

module.exports = {
  ...Type
}