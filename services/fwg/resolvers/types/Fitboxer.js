const { Fitboxer } = require('../../../../Entities/User')

const Type = {
  team: async (_parent) => {
    const fitboxer = new Fitboxer().set(_parent);
    const team = await fitboxer.team();
    if (team)
    return team
  },
  photo: (_parent) => {
    const fitboxer = new Fitboxer().set(_parent);
    return fitboxer.photo()
  } 
}

module.exports = {
  ...Type
}