const Type = {
  name: async(_parent) => {
    return _parent.Name
  },
  
  address: async(_parent) => {
    return _parent.ShippingAddress
  },

  phone: async(_parent) => {
    return _parent.Phone
  },
}

module.exports = {
  ...Type
}