const District = require('../../../../../../brooklyn-server/abstractTypes/District')
const Tournament = require('../../../../Entities/Tournament')
const { get, getCache } = require('../../../../lib/cache.store')

const Type = {
  arenaQty: async (_parent) => {
    return _parent.arenaQty || 0
  },
  club: async (_parent) => {
    const district = new District()
    return district.findByAlias(_parent.clubAlias)
  },
  referees: async (_parent) => {
    const tournament = new Tournament().set(_parent)
    return tournament.referees()
  },
  teams: async (_parent) => {
    const tournament = new Tournament().set(_parent)
    return tournament.teams()
  },
  phases: async (_parent) => {
    const tournament = new Tournament().set(_parent)
    return tournament.phases()
  },
  groups: async (_parent) => {
    const tournament = new Tournament().set(_parent)
    return tournament.groups()
  },
  combats: async (_parent, params, ctx) => {
    const tournament = new Tournament(ctx).set(_parent)
    return tournament.combats()
  },

  classification: async (_parent) => {
    const tournament = new Tournament().set(_parent)
    return tournament.classification()
  },

  status: async (_parent, params, { mongodb, session}) => {
    const tournament = new Tournament().set(_parent)
    if (session.user && session.client !== 'myH4C') {
      const user = await mongodb.user({ id: session.user.id })
      if (await tournament.isTournamentLocked(user)) {
        return 'BLOCKED'
      }
    } else {
      const tournamentLockedFlag = `Tournament:${_parent.id}:Users`;
      const ordersInProgressCache = JSON.parse(await getCache(tournamentLockedFlag) || "[]")
      if (ordersInProgressCache.length > 0){
        return 'BLOCKED'
      }
    }
    return _parent.status;
  },

  /**
   * 
   * @param {*} _parent 
   * @param {*} params 
   * @param {{mongodb } ctx 
   * @returns 
   */
  orders: async (_parent, params, { mongodb, session }) => {
    if (session.user) {
      const orders = await ctx.mongodb.orders({
        where: {
          tournamentId: _parent.id,
          email: session.user.email
        }
      })

      return orders;
    }
    return [];
  },
}

module.exports = {
  ...Type
}