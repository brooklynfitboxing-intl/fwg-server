const { Trainer } = require('../../../../Entities/User')

const Type = {
  teams: (_parent) => { 
    const trainer = new Trainer().set(_parent);
    return trainer.teams()
     
  }
}

module.exports = {
  ...Type
}