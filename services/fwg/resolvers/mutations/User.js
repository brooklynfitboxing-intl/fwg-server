const { User, Referee, Fitboxer, Trainer } = require('../../../../Entities/User')
const Team = require('../../../../Entities/Team')
const axios = require('axios')
const crypto = require('crypto')
const Mutations = {
  createUser: async (root, data, { mongodb }) => {
    const user = new User({ mongodb })
    try {
      const defaultPassword = crypto.createHash('sha256').update(process.env.REFEREE_DEFAULT_PASSWORD || 'FWG2023.').digest('hex');
      user.set({ ...data, password: defaultPassword })
      await user.save()
    } catch (err) {
      console.error(err)
      throw err
    }
  },

  updateProfile: async (root, data, { mongodb }) => {
    const user = new User({ mongodb }, data)
    try {
      await user.save(data)
      await user.refresh()
    } catch (err) {
      console.error(err)
      throw err
    }
    return user.get()
  },
  deleteReferee: async (root, { id }, { mongodb }) => {
    const referee = new Referee({ mongodb })
    try {
      await referee.findById(id)
      const resp = await referee.delete()
      return resp
    } catch (err) {
      console.error(err)
      throw err
    }
  },
  deleteFitboxer: async (root, { id }, { mongodb }) => {
    const fitboxer = new Fitboxer({ mongodb })
    try {
      await fitboxer.findById(id)
      const resp = await fitboxer.delete()
      return resp
    } catch (err) {
      console.error(err)
      throw err
    }
  },
  deleteTrainer: async (root, { id }, { mongodb }) => {
    const trainer = new Trainer({ mongodb })
    try {
      await trainer.findById(id)
      const resp = await trainer.delete()
      return resp
    } catch (err) {
      console.error(err)
      throw err
    }
  },

  deleteUser: async (root, { id }, { mongodb }) => {
    const user = new User({ mongodb })
    try {
      await user.findById(id)
      const resp = await user.delete()
      return resp
    } catch (err) {
      console.error(err)
      throw err
    }
  },
  verifyUser: async (root, { id }, { mongodb }) => {
    const user = new User({ mongodb })
    try {
      await user.findById(id)
      if (!user.get()) {
        throw new Error('USER_NOT_FOUND')
      }
      const resp = await user.updateProfile({
        verified: true
      })
      return resp
    } catch (err) {
      console.error(err)
      throw err
    }
  },
  requestVerification: async (root, { id }, { mongodb }) => {
    const user = new User({ mongodb })
    try {
      await user.findById(id)
      if (!user.get()) {
        throw new Error('USER_NOT_FOUND')
      }
      if (user.get('verified')) {
        throw new Error('USER_ALREADY_VERIFIED')
      }
      const url = process.env.PUBLIC_BASE_URL
      await user.updateProfile({
        verified: true
      })
      const verificationUrl = `${url}/${user.get('locale')}/sign-up/verify-trainer-email/${id}`
      const response = await axios.get(verificationUrl)
      return response

    } catch (err) {
      console.error(err)
      throw err
    }
  },
  /**
   * 
   * @param {*} root 
   * @param {*} param1 
   * @param {{mongodb}} param2 
   */
  setCaptain: async (root, { id, teamCode }, { mongodb }) => {
    const user = new User({ mongodb })
    const team = new Team({ mongodb })
    try {
      await user.findById(id)
      await team.findByCode(teamCode)
      if (!user.get()) {
        throw new Error('USER_NOT_FOUND')
      }
      if (!team.get()) {
        throw new Error('TEAM_NOT_FOUND')
      }
      if (user.get('type') !== 'FITBOXER') {
        throw new Error('USER_ALREADY_VERIFIED')
      }
      const fitboxers = await team.fitboxers();
      const captainsIdList = fitboxers.filter(it => it.isCaptain).map(it => it.id)
      await mongodb.updateManyUsers({
        where: {
          id_in: captainsIdList
        },
        data: {
          isCaptain: false
        }
      })
      await user.updateProfile({ isCaptain: true })
      return user.get()
      // return resp  
    } catch (err) {
      console.error(err)
      throw err
    }
  }
}

module.exports = {
  ...Mutations
}