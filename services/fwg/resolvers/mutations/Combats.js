const Combat = require('../../../../Entities/Combat')
const { Referee } = require('../../../../Entities/User')

const Mutations = {
  setParticipants: async (_parent, { id, data }, ctx) => {
    const participants = JSON.parse(data || '{}')
    const handler = new Combat(ctx)
    await handler.findById(id)
    await handler.setParticipants(participants)
    return true
  },

  setParticipantsDummy: async (_parent, { id }, ctx) => {
    const bagsMap = {
      1: 1,
      2: 9,
      3: 17,
      4: 25,
    }
    const handler = new Combat(ctx)
    const team2 = await handler.team2().fitboxers();
    const team1 = await handler.team1().fitboxers();
    const participants = {}
    team1.forEach(it => {
      participants[it.id] = "0"
    })
    team2.forEach(it => {
      participants[it.id] = "0"
    })
    team1.splice(4, 1)
    team2.pop(4, 1)
    let bag = bagsMap[handler.get('arena')]
    for (const fitboxer of [...team1, ...team2]) {
      participants[fitboxer.id] = `${bag}`
      bag++
    }
    await handler.findById(id)
    await handler.setParticipants(participants)
    return true
  },

  setVotes: async (_parent, { id, comboNumber, data, review }, ctx) => {
    const votes = JSON.parse(data || '{}')
    const { session } = ctx;
    const handler = new Combat(ctx)
    await handler.findById(id)
    await handler.setVotes(comboNumber, votes, session.user.id)
    if (review) await handler.setReview(comboNumber, 'Votes', review)
    await handler.calcScores(comboNumber)
    return true
  },

  setScores: async (_parent, { id, data }, ctx) => {
    const combos = JSON.parse(data || '{}')
    const handler = new Combat(ctx)
    await handler.findById(id)
    combos.forEach(c => c._new = true)
    await handler._upsertScore(combos)
    return true
  },

  enableEnergies: async (_parent, { id, comboNumber, data, review }, ctx) => {
    const handler = new Combat(ctx)
    await handler.findById(id)
    const energies = JSON.parse(data || '{}')
    const scores = await handler.scores(comboNumber)
    const results = []
    for (const team of ['team1', 'team2']) {
      scores.results[team].detail.filter(el => !!el).forEach(el => {
        results.push({
          sb: el.bag,
          workout: el.energy,
          status: energies[el.id] === true ? 'OK' : 'KO'
        })
      })
    }

    await handler.setResults(comboNumber, results)
    await handler.calcScores(comboNumber)

    if (review) await handler.setReview(comboNumber, 'Energy', review)

    return true
  },
  /**
   * 
   * @param {object} _parent 
   * @param {{id:string, winner:number, review:string}} param1 
   * @param {*} ctx 
   * @returns 
   */
  finishCombat: async (_parent, { id, winner, review }, ctx) => {
    const handler = new Combat(ctx)
    await handler.findById(id)
    const data = { status: 'F', winner }
    if (winner === 0) {
      // Nulo
      data.score1 = 0
      data.score2 = 0
    } else {
      // Ganador predeterminado
      data.score1 = winner === 1 ? 21 : 0
      data.score2 = winner === 2 ? 21 : 0
    }
    if (review) {
      // Guardar review
      await handler.setReview(1, winner === 0 ? 'setNull' : 'setWinner', review)
    }
    await handler.finishCombat(data)
    return true
  },

  forceFinishCombat: async (_parent, { id }, ctx) => {
    const handler = new Combat(ctx)
    await handler.findById(id)
    await handler.finishCombat();
    return true
  },
  restartCombat: async (_parent, { id }, ctx) => {
    const handler = new Combat(ctx)
    await handler.findById(id)
    await handler.restartCombat()
    return true
  },
  addRefereeToCombat: async (_parent, { combatId, email }) => {
    const combat = new Combat()
    await combat.findById(combatId)
    const referee = new Referee();
    await referee.findByEmail(email)
    if (!referee.get()) {
      throw new Error('REFEREE_NOT_FOUND')
    }
    return (await combat.addReferee(referee.get('email')))
  },
  removeRefereeFromCombat: async (_parent, { combatId, email }) => {
    const combat = new Combat()
    await combat.findById(combatId)
    const referee = new Referee();
    await referee.findByEmail(email)
    if (!referee.get()) {
      throw new Error('REFEREE_NOT_FOUND')
    }
    return (await combat.removeReferee(referee.get('email')));

  },
}

module.exports = {
  ...Mutations
}