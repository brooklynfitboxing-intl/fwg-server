
const Order = require('../../../../Entities/Order');
const Team = require('../../../../Entities/Team');
const Tournament = require('../../../../Entities/Tournament');
const { Trainer, Fitboxer } = require('../../../../Entities/User')

const Mutations = {
  createCode: async (root, { trainerId }, { mongodb }) => {
    const team = new Team({ mongodb });
    const { id, code } = await team.createCode(trainerId)
    return {
      id,
      code
    }
  },

  deleteCode: async (root, { code }, { mongodb }) => {
    const team = new Team({ mongodb });
    await team.findByCode(code)
    await team.delete(code)
    return team
  },

  updateTeam: async (root, { code, name, motto, trainerEmail }) => {
    const team = new Team()
    await team.findByCode(code)
    if (!team.get()) {
      throw new Error('TEAM_NOT_FOUND')
    }
    const teamTrainer = await team.trainer()
    if (trainerEmail && trainerEmail !== teamTrainer.email) {
      const trainer = await new Trainer().findByEmail(trainerEmail)
      if (trainer && !trainer.get('id')) {
        throw new Error('TRAINER_NOT_FOUND')
      }
      await team.updateTeam(name, motto, trainer.get('id'))
    }
    return await team.updateTeam(name, motto)
  },

  addFitboxerToTeam: async (root, { code, email }) => {
    const team = new Team()
    await team.findByCode(code)
    if (!team.get()) {
      throw new Error('TEAM_NOT_FOUND')
    }
    await team.addFitboxer(email)
    const fitboxerHandler = await (new Fitboxer().findByEmail(email))
    if (!fitboxerHandler.get()) {
      throw new Error(`Fitboxer not found: ${email}`)
    }
    await fitboxerHandler.setTeam(team.get('id'))
    await team.refresh()
    return team.get()
  },
  removeFitboxer: async (root, { code, fitboxerId }) => {
    try {
      const team = await (new Team().findByCode(code))
      if (!team.get()) {
        throw {
          status: false,
          message: 'TEAM_CODE_NOT_FOUND'
        }
      }
      await team.removeFitboxer(fitboxerId)
      await team.refresh()
      return team.get()
    } catch (err) {
      throw new Error(err.message)
    }
  },

  createPaymentOrder: async (root, { code, expirationDate }) => {
    const team = new Team()
    await team.findByCode(code)
    if (!team.get()) {
      throw new Error('TEAM_NOT_FOUND')
    }
    try {
      const captain = await team.captain();
      if (!captain) {
        throw new Error('NO_CAPTAIN_FOUND_IN_TEAM')
      }
      const tournament = new Tournament();
      const finalTournament = (await tournament.find({ clubAlias: 'BFI', final: true }))[0]
      if (!finalTournament) {
        throw new Error('FINAL_TOURNAMENT_NOT_FOUND')
      }
      const orderData = {
        tournamentId: finalTournament.id,
        clubMerchantAlias: finalTournament.clubMerchantAlias,
        paymentMethod: "Card",
        amount: finalTournament.price,
        currency: "EUR",
        exchangeRate: 1.0,
        teamCode: code,
        email: captain.email,
        firstName: captain.firstName,
        lastName: captain.lastName,
        phone: captain.phone,
        description: "The Finals - FWG - 2024",
        expirationDate
      };
      await Order.generateOrderForTeam(orderData);
      return true;
    } catch (err) {
      console.error(`Error trying to generate payment order `, err)
      throw err
    }
  }
}

module.exports = {
  ...Mutations
}