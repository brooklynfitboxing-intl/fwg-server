const Team = require('../../../../Entities/Team');
const District = require('../../../../../../brooklyn-server/abstractTypes/District')
const Order = require('../../../../Entities/Order');
const Tournament = require('../../../../Entities/Tournament');
const Combat = require('../../../../Entities/Combat');
const { getPaymentMethods } = require('../../../../lib/Adyen');
const { Fitboxer } = require('../../../../Entities/User')

const MAX_TEAM_SIZE_ON_TOURNAMENT = 8
const Mutations = {
  createTournament: async (root, { clubAlias, clubMerchantAlias, name, date, price, status, capacity }, { mongodb }) => {
    status = status || 'AVAILABLE'
    const tournament = new Tournament({ mongodb });
    const district = new District()
    if (!clubAlias || !clubMerchantAlias) {
      const clubMissing = !clubMerchantAlias ? '_MERCHANT_' : ''
      throw new Error(`CLUB${clubMissing}NOT_PROVIDED`)
    }
    await district.findByAlias(clubAlias)
    if (!district.get()) {
      throw new Error('CLUB_ALIAS_NOT_FOUND')
    }
    const { currency } = await district.findByAlias(clubMerchantAlias)
    if (!currency) {
      throw new Error(`No currency defined for center: ${clubMerchantAlias}, contact with support`)
    }
    const data = { clubAlias, clubMerchantAlias, name, date, price, status, currency, capacity }
    if (!district.get()) {
      throw new Error('CLUB_MERCHANT_ALIAS_NOT_FOUND')
    }
    tournament.set(data)
    const result = await tournament.save()
    return result
  },

  createTournamentOrder: async (root, { id, fitboxerId }, { mongodb }) => {
    const tournament = new Tournament({ mongodb });
    const district = new District()
    const fitboxer = new Fitboxer()
    await tournament.findById(id)
    if (!tournament.get()) {
      throw new Error('TOURNAMENT_NOT_FOUND')
    }
    await fitboxer.findById(fitboxerId)
    if (!fitboxer.get()) {
      return {
        "errors": [
          { "message": 'FITBOXER_NOT_FOUND' }
        ]
      }
    }
    const { team } = fitboxer.get()
    if (!team) {
      return {
        "errors": [
          { "message": 'FITBOXER_WITHOUT_TEAM' }
        ]
      }
    }
    const { clubMerchantAlias } = tournament.get()
    const club = await district.findByAlias(clubMerchantAlias)
    const { paymentMethod } = club
    let paymentInfo = {}
    if (paymentMethod === 'A') {
      paymentInfo = await getPaymentMethods(club, fitboxerId, tournament.get('currency'), tournament.get('price'))
    }
    return paymentInfo
  },

  deleteTournament: async (root, { id }, { mongodb }) => {
    const tournament = new Tournament({ mongodb });
    await tournament.findById(id)
    if (!tournament.get()) {
      throw new Error('TOURNAMENT_NOT_FOUND')
    }
    let size = (await tournament.teams()).length
    if (size) {
      throw new Error('TOURNAMENT_WITH_TEAMS')
    }
    await tournament.delete()
    return tournament.get()
  },

  updateTournament: async (root, { id, clubAlias, clubMerchantAlias, name, date, price, capacity }, { mongodb }) => {
    const tournament = new Tournament({ mongodb });
    const district = new District()
    await tournament.findById(id)

    if (!clubAlias || !clubMerchantAlias) {
      const clubMissing = !clubMerchantAlias ? '_MERCHANT_' : ''
      throw new Error(`CLUB${clubMissing}NOT_PROVIDED`)
    }
    await district.findByAlias(clubAlias)
    if (!district.get()) {
      throw new Error('CLUB_ALIAS_NOT_FOUND')
    }
    const { currency } = await district.findByAlias(clubMerchantAlias)
    if (!currency) {
      throw new Error(`No currency defined for center: ${clubMerchantAlias}, contact with support`)
    }
    const data = { clubAlias, clubMerchantAlias, name, date, price, capacity }

    tournament.set(data)
    await tournament.update(data)
    return tournament
  },

  addTeam: async (root, { code, id }, { mongodb }) => {
    const tournament = new Tournament({ mongodb });
    const team = new Team({ mongodb });
    await team.findByCode(code)
    if (!team.get()) {
      throw new Error('TEAM_CODE_NOT_FOUND')
    }
    await tournament.findById(id)
    if (!tournament.get()) {
      throw new Error('TOURNAMENT_NOT_FOUND')
    }
    let size = (await tournament.teams()).length
    if (size === MAX_TEAM_SIZE_ON_TOURNAMENT) {
      throw new Error('TOURNAMENT_FULL')
    }
    const teamInAnotherTournament = await tournament.hasTeam(team)
    if (teamInAnotherTournament) {
      throw new Error('TEAM_IN_ANOTHER_TOURNAMENT')
    }
    const result = await tournament.addTeam(team)
    const orderHandler = new Order();
    orderHandler.set({
      tournamentId: tournament.get('id'),
      email: "ADMIN",
      paymentReference: "ADMIN",
      amount: 0,
      status: "COMPLETED",
      teamCode: code,
    })

    await orderHandler.save()

    size = (await tournament.teams()).length
    if (size === MAX_TEAM_SIZE_ON_TOURNAMENT) {
      await tournament.update({
        status: 'FULL'
      })
    }
    return result
  },

  removeTeam: async (root, { code, id }, { mongodb }) => {
    const tournament = new Tournament({ mongodb });
    const team = new Team({ mongodb });
    await team.findByCode(code)
    if (!team.get()) {
      throw new Error('TEAM_CODE_NOT_FOUND')
    }
    await tournament.findById(id)
    if (!tournament.get()) {
      throw new Error('TOURNAMENT_NOT_FOUND')
    }
    const result = await tournament.removeTeam(team.get())
    await team.removeFromTournament()
    if (tournament.get('status') === 'FULL') {
      return await tournament.update({
        status: 'AVAILABLE'
      })
    }
    return result
  },

  publishTournament: async (root, { id, published }, { mongodb }) => {
    const tournament = new Tournament({ mongodb });
    await tournament.findById(id)
    await tournament.update({ published })
    return tournament
  },


  buildCalendar: async (root, { id }, ctx) => {
    const tournament = new Tournament(ctx)
    await tournament.findById(id)
    if (tournament.get('final')) {
      await tournament.buildCustomCalendar()
    } else {
      await tournament.buildCalendar()
    }
    return tournament
  },

  updateClassification: async (_parent, { id }, ctx) => {
    const tournament = new Tournament()
    await tournament.findById(id)
    const groups = await tournament.groups()
    const phase = (await tournament.phases()).find(el => el.type === 'L') // Fase de grupos
    const teams = []
    for (const group of groups) {
      const combats = await tournament.combats('F', phase.id, group.id)
      for (const combat of combats) {
        const handler = new Combat(ctx)
        await handler.findById(combat.id)
        const team1 = await handler.team1()
        const team2 = await handler.team2()
        const team1Index = teams.indexOf(team1.id)
        const team2Index = teams.indexOf(team2.id)
        if (team1Index === -1 || team2Index === -1) { // in order to ignore teams already calculated
          await handler.updateCompetition()
          if (team1Index === -1) teams.push(team1.id)
          if (team2Index === -1) teams.push(team2.id)
        }
      }
    }

    return true
  },

  updateGlobalRanking: async () => {
    await Tournament.updateGlobalRanking()
    return true
  },

  addRefereeToTournament: async (_parent, { tournamentId, email }) => {
    const tournament = new Tournament()
    await tournament.findById(tournamentId)
    return (await tournament.addReferee(email));
  },

  removeRefereeFromTournament: async (_parent, { tournamentId, email }) => {
    const tournament = new Tournament()
    await tournament.findById(tournamentId)
    return (await tournament.removeReferee(email));
  },

  restartTournament: async (_parent, { id }) => {
    await Tournament.restartTournament(id);
    return true
  }
}

module.exports = {
  ...Mutations
}