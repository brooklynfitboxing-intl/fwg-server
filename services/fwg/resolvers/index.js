const fs = require('fs')

/* Queries */
const Query = {}
fs.readdirSync(`${__dirname}/queries`).forEach((file) => {
  if ( file.substr(-3) === '.js' ) {
    const queries = require(`./queries/${file}`)
    for(var query in queries) {
      Query[query] = queries[query]
    }
  }
})

/* Mutations */
const Mutation = {}
fs.readdirSync(`${__dirname}/mutations`).forEach((file) => {
  if ( file.substr(-3) === '.js' ) {
    const mutations = require(`./mutations/${file}`)
    for(var mutation in mutations) {
      Mutation[mutation] = mutations[mutation]
    }
  }
})

const resolvers = {
  Query,
  Mutation
}

/* Types */
fs.readdirSync(`${__dirname}/types`).forEach((file) => {
  const extFile = file.substr(-3)
  if ( extFile === '.js' ) {
    const type = require(`./types/${file}`)
    const typeName = file.replace(extFile, '')
    resolvers[typeName] = type
  }
})

module.exports = resolvers