const Combat = require('../../../../Entities/Combat')
const Tournament = require('../../../../Entities/Tournament')
const { Trainer, Referee } = require('../../../../Entities/User')


const Queries = {

  currentCombats: async (_parent, { tournamentId }, { mongodb, session }) => {
    let combats = []
    const tournamenEntity = new Tournament()
    await tournamenEntity.findById(tournamentId)
    if(!tournamenEntity.get()){
      throw new Error('TOURNAMENT_NOT_FOUND')
    }
    combats = await tournamenEntity.combats('C')
    if(combats.length){
      combats.sort((a,b) => a.time - b.time)
      return combats.slice(0, tournamenEntity.get('arenaQty') || 1 )
    } else {
      return []
    }
  },
  nextCombats: async (_parent, { tournamentId }, { mongodb, session }) => {
    let combats = []
    const tournamenEntity = new Tournament()
    await tournamenEntity.findById(tournamentId)
    if(!tournamenEntity.get()){
      throw new Error('TOURNAMENT_NOT_FOUND')
    }
    if (session.currentRole === 'REFEREE') {
      const refereeEntity = new Referee({ mongodb })
      await refereeEntity .findById(session.user.id)
      combats = await refereeEntity.combats(tournamentId, ['P','C'])
    } else if (session.currentRole === 'TRAINER') {
      const trainerEntity = new Trainer({ mongodb })
      await trainerEntity.findById(session.user.id)
      combats = await trainerEntity.combats(tournamentId, ['P','C'])
    }
    if(combats.length){
      combats.sort((a,b) => a.time - b.time)
      return combats.slice(0, tournamenEntity.get('arenaQty') || 1 )
    } else {
      return []
    }
  },

  completedCombats: async (_parent, { tournamentId }, { mongodb }) => {
    const tournamenEntity = new Tournament({ mongodb })
    await tournamenEntity.findById(tournamentId)
    const combats = await tournamenEntity.combats('F')
    return combats
  },

  combat: async (_parent, { id }, ctx) => {
    return new Combat(ctx).findById(id)
  },

  energyResults: async (_parent, { id, comboNumber }, ctx) => {
    const handler = new Combat(ctx)
    await handler.findById(id)
    const score = await handler.scores(comboNumber)
    return score.results
  },

  refereeResults: async (_parent, { id, comboNumber }, ctx) => {
    const handler = new Combat(ctx)
    await handler.findById(id)
    let score = await handler.scores(comboNumber)
    score = score.votes[ctx.session.user.id]
    return {
      technique: score.technique,
      coordination: score.coordination,
      ranges: score.ranges,
      hits: score.hits
    }
  },
}

module.exports = {
  ...Queries
}