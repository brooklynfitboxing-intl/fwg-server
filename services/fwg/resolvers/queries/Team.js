const Team = require('../../../../Entities/Team')
const Queries = {
  teamsCount: async (root, { size, page }, { mongodb }) => {
    const team = new Team({ mongodb })
    const result = await team.findAll(null, { count: true })
    return result
  },
  teams: async (root, { size, page }, { mongodb }) => {
    const team = new Team({ mongodb })
    const result = await team.findAll(null, { size, page })
    return result
  },
  team: async (root, { code }, { mongodb }) => {
    const team = new Team({ mongodb })
    await team.findByCode(code)
    return team.get()
  },
}

module.exports = {
  ...Queries
}