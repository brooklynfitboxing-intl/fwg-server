const Tournament = require('../../../../Entities/Tournament')
const { Referee, Trainer } = require('../../../../Entities/User')
// const Combat = require('../../../../Entities/Combat')

const Queries = {

  tournaments: async () => {
    const tournamentList = (await new Tournament().findAll())
    return tournamentList;
  },

  myTournaments: async (_parent, args, { session, mongodb }) => {
    // Si soy trainer: Torneos en los que participe al menos un equipo al que el trainer lleva
    // Si soy referee: Torneos del torneo asignados a mi como referee
    if (session.currentRole && session.currentRole === 'TRAINER') {
      const trainer = new Trainer({ mongodb })
      await trainer.findById(session.user.id)
      return trainer.tournaments()
    } else if (session.currentRole && session.currentRole === 'REFEREE') {
      const referee = new Referee({ mongodb })
      await referee.findById(session.user.id)
      return referee.tournaments()
    } else {
      return []
    }
  },

  tournament: async (root, { id, final }, ctx) => {
    const tournament = new Tournament(ctx)
    if(id) {
      await tournament.findById(id)
    } else if (final) {
      await tournament.final()
    }
    return tournament.get()
  },
  teamsGlobalClassification: async (root, { id }, ctx) => {
    return Tournament.teamsGlobalClassification()
  },
  globalRanking: async (root, { standing, tournament }, ctx) => {
    return Tournament.globalRanking(standing, tournament)
  }

}

module.exports = {
  ...Queries
}