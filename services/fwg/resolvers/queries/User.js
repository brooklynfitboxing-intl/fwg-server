const { Trainer, Fitboxer, Referee, User } = require('../../../../Entities/User')

const Queries = {

  trainers: async (root, { page, size }, { mongodb }) => {
    const trainerList = (await new Trainer({ mongodb }).findAll({ page, size }))
    return trainerList
  },
  trainersCount: async (root, _, { mongodb }) => {
    const trainerList = (await new Trainer({ mongodb }).findAll({ count: true}))
    return trainerList
  },
  trainer: async (root, { id }, { mongodb }) => {
    const trainer = (await new Trainer({ mongodb }).findById(id))
    return trainer
  },

  fitboxers: async (root, { page, size }, { mongodb }) => {
    const fbList = await (new Fitboxer({ mongodb }).findAll({ page, size }))
    return fbList
  },
  fitboxersCount: async (root, _, { mongodb }) => {
    const fbList = await (new Fitboxer({ mongodb }).findAll({ count: true }))
    return fbList
  },

  referees: async (root, data, { mongodb }) => {
    const fbList = await (new Referee({ mongodb }).findAll())
    return fbList
  },

  fitboxer: async (root, { id }, { mongodb }) => {
    const fitboxer = (await new Fitboxer({ mongodb }).findById(id))
    return fitboxer
  },

  user: async (root, { id }, { mongodb }) => {
    const user = (await new User({ mongodb }).findById(id))
    const result = user.get();
    return result
  },
}

module.exports = {
  ...Queries
}