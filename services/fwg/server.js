const bodyParser = require('body-parser')
const { GraphQLServer } = require('graphql-yoga')
const { prisma: mongodb } = require('../../datasources/mongodb/generated/prisma-client')
const velneo = require('../../../../brooklyn-server/datasources/velneo')

const sessionStore = require('../../lib/session.store')
const resolvers = require('./resolvers')

// Authentication
const permissions = require('./shield/permissions')
// const { applyMiddlewareToDeclaredResolvers } =  require('graphql-middleware')
const server = new GraphQLServer({
  typeDefs: __dirname + '/schema.graphql',
  resolvers,
  middlewares: [permissions],
  async context(req) {
    const session = req.request.session
    
    // console.log("context",session , req.request.sessionID)
    // let role = null
    // if (session) {
    //   role = sessionStore.getCurrentRole(session)
    //   if(role)
    //     console.log(role)
    // } 
    return {
      mongodb, velneo,
      req,
      session,
      // role
    }
  }
})

// applyMiddlewareToDeclaredResolvers(server.executableSchema , permissions)
// Express
server.express.use(bodyParser.urlencoded({ extended: true }))
server.express.use(bodyParser.json()) // handle json data
const cors = {
  origin: ["http://localhost:8080", "http://10.10.0.4"]
}

server.express.use(function (req, res, next) {
  let origin = req.headers.origin;
  if (cors.origin.indexOf(origin) >= 0) {
    res.header("Access-Control-Allow-Origin", origin);
  }
  // res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  next();
})
// Auth
sessionStore.init(server.express)

const authRoutes = require('./routes/auth')
const teamRoutes = require('./routes/team')
const usersRoutes = require('./routes/users')
const paymentsRoutes = require('./routes/payments/index')
const adyenRoutes = require('./routes/payments/adyen')
const unitellerRoutes = require('./routes/payments/uniteller')
const playerRoutes = require('./routes/player')
// const webRoutes = require('./routes/web')

// // Routes
server.express.use('/auth', authRoutes)
server.express.use('/team', teamRoutes)
server.express.use('/users', usersRoutes)
server.express.use('/payments', paymentsRoutes)
server.express.use('/payments/adyen', adyenRoutes)
server.express.use('/payments/uniteller', unitellerRoutes)
server.express.use('/player', playerRoutes)
// server.express.use('/web', webRoutes)

// Statics

const port = process.env.PORT || 3030
server.start({
  port,
  playground: process.env.PLAYGROUND
}, () => { console.log(`🚀 Fitboxing world games, is running on http://localhost:${port}`) })