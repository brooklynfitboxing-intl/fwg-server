
/**
 * @param {import('express').Request } req
 * @param {import('express').Response } res
 * @param {import('express').NextFunction } next
 */
function isAuth(req, res, next) {
  if (req.query.token === '.M]Uq?-jZl;xRw(4Yo/-EL8ITl:T') {
    next()
    return
  } else if (!req.session.isAuth) {
    return res.status(403).send("AUTH_IS_REQUIRED")
  }
  next()
}

function isTrainer(req, res, next) {
  const session = req.session
  if (session.currentRole === 'ADMIN') {
    next()
    return
  } else if (session.currentRole !== 'TRAINER') {
    return res.status(403).send('NOT_AUTHORISED')
  }
  next()
}

function isReferee(req, res, next) {
  const session = req.session
  if (session.currentRole === 'ADMIN') {
    next()
    return
  } else if (session.currentRole !== 'REFEREE') {
    return res.status(403).send('NOT_AUTHORISED')
  }
  next()
}

function isTrainerOrCaptain(req, res, next) {
  const session = req.session
  if ('TRAINER' === session.currentRole || session.user.isCaptain) {
    next()
  } else {
    return res.status(403).send('NOT_AUTHORISED')
  }
}



function isCaptain(req, res, next) {
  const session = req.session
  if (session.currentRole === 'ADMIN') {
    next()
    return
  } else if (!session.user.isCaptain) {
    return res.status(403).send('NOT_AUTHORISED')
  }
  next()
}
function isFitboxer(req, res, next) {
  const session = req.session

  if (session.currentRole === 'ADMIN') {
    next()
    return
  } else if (session.currentRole !== 'FITBOXER') {
    return res.status(403).send('NOT_AUTHORISED')
  }
  next()
}

function isAdmin(req, res, next) {
  const session = req.session
  if (session.currentRole !== 'ADMIN') {
    return res.status(403).send('NOT_AUTHORISED')
  }
  next()
}


module.exports = {
  isAuth,
  isTrainer, isFitboxer, isCaptain, isTrainerOrCaptain, isReferee,
  isAdmin
}