const SuperClass = require('./main/SuperClass')
const Tournament = require('./Tournament')
const Classification = require('./Classification')
const Team = require('./Team')

class Combat extends SuperClass {
  /**
   * @type {import('../datasources/mongodb/generated/prisma-client').Combat}
   */
  root;
  constructor(ctx) {
    super(ctx || SuperClass.fakeCtx())
  }

  async findById(id) {
    const result = await this.mongodb.combat({ id })
    this.set(result)
    return this.get()
  }

  async findByTime(tournamentId, time) {
    return this.mongodb.combats({
      where: {
        time,
        tournament: { id: tournamentId }
      }
    })
  }


  async update(data) {
    this._checkRoot()
    const id = this.get('id')
    const result = await this.mongodb.updateCombat({ where: { id }, data })
    this.set(result)
    return this.get()
  }

  /**
   * 
   * @param {{[string]: number}} data 
   * @returns 
   */
  async setParticipants(data) {
    const participants = { ...this.get('participants') || {}, ...data }
    return this.update({
      participants
    })
  }

  async setResults(comboNumber, data) {
    this._checkRoot()
    const participants = await this.participants()
    const bags = this.get('participants')
    if (!bags) {
      console.warn(`No bags assigned for this combat, combat: [arena: ${this.get('arena')}, time: ${this.get('time')}, id: ${this.get('id')}`)
      return;
    }
    // Se establecen los resultados del round
    // Se distribuye la energy para cada uno de los equipos en funcion de participans
    // Se establece la parte correspondiente al energy

    function mapResults(sTeam) {
      const detail = participants[sTeam].map(el => {
        const bag = parseInt(bags[el.id])
        if (!bag) return

        const score = {
          ...el,
          bag,
          energy: (data.find(el => el.sb === bag) || { workout: 0 }).workout,
          status: (data.find(el => el.sb === bag) || { status: 'OK' }).status || 'OK',
        }
        return score
      })

      const valid = detail.filter(el => !!el && el.status === 'OK')
      return {
        detail,
        total: (Math.round((valid.reduce((acc, el) => acc + el.energy, 0) / valid.length) * 100) / 100) || 0
      }

    }

    const team1 = mapResults('team1')
    const team2 = mapResults('team2')


    let energy = 0
    if (team1.total > team2.total) energy = 1
    else if (team1.total < team2.total) energy = 2

    let combo = await this.scores(comboNumber)
    if (!combo) {
      combo = {
        _new: true,
        number: comboNumber,
        energy
      }
    }
    const combo2save = { ...combo, results: { team1, team2 }, energy }
    await this._upsertScore(combo2save)
    await this.calcScores(comboNumber)
  }

  async setVotes(comboNumber, vote, refereeId) {
    this._checkRoot()
    // Se establece la parte correspondiente al referre
    let combo = await this.scores(comboNumber)
    if (!combo) {
      combo = {
        _new: true
      }
    }
    let newVotes = { [refereeId]: vote }
    const votes = await this.votes(comboNumber)
    if (votes) {
      newVotes = { ...votes, ...newVotes };
    }
    const votesAgg = this.calculateVotes(newVotes)
    const voteEntries = Object.keys(votesAgg.team1)
    const scores = {}
    for (const key of voteEntries) {
      const winner = (votesAgg.team1[key] < votesAgg.team2[key]) + 1
      scores[key] = winner === 1 ? 1 : 2
    }
    combo = {
      ...combo,
      number: comboNumber,
      votes: {
        ...combo.votes,
        [refereeId]: vote
      },
      ...scores,
    }
    await this._upsertScore(combo)
    await this.calcScores(comboNumber)
  }


  async _upsertScore(combo2save) {
    this._checkRoot()
    const data = {}
    if (combo2save._new) {
      // crear
      delete combo2save._new
      data.scores = {
        create: [combo2save]
      }
    } else {
      // Actualizar
      data.scores = {
        updateMany: [{
          where: { number: combo2save.number },
          data: combo2save
        }]
      }
    }
    const result = await this.mongodb.updateCombat({
      where: { id: this.get('id') },
      data
    })
    return result
  }

  async calcScores(comboNumber) {
    this._checkRoot()
    // Cargar Combo Scores
    await this.include('scores')
    const hasEnergyPending = this.get('scores').some(it => it.energy === null)
    const scores = await this.scoresSummary()
    let lastCombo = this.get('lastCombo')

    const referees = await this.referees()
    const votes = await this.votes(comboNumber)
    if (votes && Object.keys(votes).length === referees.length) {
      lastCombo = Math.max(comboNumber, lastCombo)
    }
    const isFinished = lastCombo === 3 && !hasEnergyPending
    await this.update({
      score1: scores.team1.total,
      score2: scores.team2.total,
      lastCombo,
    })

    if (isFinished) await this.finishCombat()
  }

  async setWinner(team) {
    this._checkRoot()
    await this.update({
      winner: team
    })
  }

  async setReview(number, type, reason) {
    await this.update({
      reviews: {
        create: {
          number,
          type,
          userEmail: this.session.user.email,
          reason,
        }
      }
    })
  }

  async finishCombat(data) {
    this._checkRoot()
    if (data) {
      // Se permite recibir datos especiales como status, score1, score2 y winner
      await this.update({ ...data })
    } else {
      await this.update({
        status: 'F',
        winner: this.get('score1') > this.get('score2') ? 1 : 2
      })
    }
    const key = `Combat:${this.get('time')}-arena:${this.get('arena')}`
    const exist = await this.cache.getCache(key) 
    if(!exist){
      // Auditoria
      this.cache.setCache(key, JSON.stringify(this.get())) 
    }

    const phase = await this.phase()
    if (phase.type === 'L') {
      await this.updateCompetition()
    } else {
      const pendingCombatsOnThisPhase = await new Tournament().set((await this.tournament())).combats(['P,C'], phase.id)
      if (!pendingCombatsOnThisPhase.length) {
        this.updateNextCombats()
      } else if (phase.name === 'FINAL' || phase.name === 'Final Four') {
        await this.updateTournamentRanking()
      } 
    }
  }

  async restartCombat() {
    // Establecer Status y LastCombo y Scores
    // Eliminar scores
    await this.update({
      status: 'P',
      lastCombo: 0,
      score1: null,
      score2: null,
      winner: null,
      participants: null,
      game: null,
      scores: {
        deleteMany: [{ number_not: 0 }]
      },
      reviews: {
        deleteMany: [{ number_not: 0 }]
      }
    })
  }

  async updateCompetition() {
    this._checkRoot()
    // Actualizar clasificacion si fase de Liga

    const tournament = await this.tournament()
    const team1 = await this.team1()
    const team2 = await this.team2()
    const groupId = this.get('group')

    const dataClassificationTeams = {
      tournament: { connect: { id: tournament.id } },
      phase: this.get('phase'),
      group: groupId
    }

    const classificationTeam = {
      [team1.id]: { won: 0, lose: 0, pf: 0, pa: 0, dif: 0, maxAvgEnergy: 0, energy: 0, technique: 0 },
      [team2.id]: { won: 0, lose: 0, pf: 0, pa: 0, dif: 0, maxAvgEnergy: 0, energy: 0, technique: 0 }
    }

    for (const team of [team1, team2]) {
      const combatsTeam1 = await this.mongodb.combats({
        where: { team1: { id: team.id }, tournament: { id: tournament.id }, phase: this.get('phase'), group: groupId, status: 'F' }
      })
      const combatsTeam2 = await this.mongodb.combats({
        where: { team2: { id: team.id }, tournament: { id: tournament.id }, phase: this.get('phase'), group: groupId, status: 'F' }
      })
      const combatsMerged = combatsTeam1.concat(combatsTeam2).reduce((acc, it) => {
        return {
          ...acc,
          [it.id]: {
            ...it
          }
        }
      }, {})
      for (const combatId in combatsMerged) {
        const currentCombat = combatsMerged[combatId]
        const combat = new Combat().set(currentCombat)

        const combatTeam1 = await combat.team1()
        const isLocal = team.id === combatTeam1.id
        // 1. Número de Combates Ganados: El equipo que gane más combates quedará por encima en la clasificación.
        if (isLocal) {
          classificationTeam[team.id].won += currentCombat.winner === 1
          classificationTeam[team.id].lose += currentCombat.winner !== 1
        }
        else {
          classificationTeam[team.id].won += currentCombat.winner === 2
          classificationTeam[team.id].lose += currentCombat.winner !== 2
        }

        // 2. Puntuación Total: Mayor diferencia de puntos sumando las puntuaciones a favor y en contra de los tres combates disputados.

        if (isLocal) {
          classificationTeam[team.id].pf += currentCombat.score1
          classificationTeam[team.id].pa += currentCombat.score2
        }
        else {
          classificationTeam[team.id].pf += currentCombat.score2
          classificationTeam[team.id].pa += currentCombat.score1
        }

        classificationTeam[team.id].dif = classificationTeam[team.id].pf - classificationTeam[team.id].pa

        const scoresSummary = await combat.scoresSummary()

        const score = scoresSummary[isLocal ? 'team1' : 'team2']
        // 3. Puntuación Técnica: Mayor puntuación acumulada en los 4 apartados de ejecución técnica en sus tres combates.
        classificationTeam[team.id].technique += (
          score.technique +
          score.coordination +
          score.hits +
          score.ranges);
        // 4. Puntuación de Energy: Mayor puntuación acumulada de Energy en sus tres combates.
        classificationTeam[team.id].energy += score.energy

        // 5. Mayor Media de Energy: Mayor media de Energy en uno de los tres combates.
        const energyScores = await combat.scores()
        const avgEnergy = Math.round((
          energyScores.reduce((acc, el) => acc + (el.results && el.results[isLocal ? 'team1' : 'team2'].total) || 0, 0)
          / (energyScores.length || 1)
        ) * 100) / 100
        classificationTeam[team.id].maxAvgEnergy = Math.max(
          classificationTeam[team.id].maxAvgEnergy,
          avgEnergy
        )
      }
    }
    for (const teamId in classificationTeam) {
      const classifications = (await this.mongodb.classifications({
        where: {
          team: { id: teamId },
          tournament: { id: tournament.id },
          group: groupId,
          phase: this.get('phase')
        }
      }))
      if (classifications.length) {
        const classification = classifications[0]
        await this.mongodb.updateClassification({
          where: { id: classification.id },
          data: {
            ...dataClassificationTeams, ...classificationTeam[teamId],
          }
        })
      }
    }

    // Actualizar orden de la clasificacion del grupo
    await this.updateClassificationOrder()
    await this.updateNextCombats()
  }

  async updateClassificationOrder() {
    this._checkRoot();
    const tournament = await this.tournament()
    const groups = await (new Tournament().set(tournament)).groups()
    groups.forEach(async (group) => {
      let order = 1;
      const teamsClassifications = await this.mongodb.classifications({
        where: {
          phase: this.get('phase'),
          tournament: { id: tournament.id },
          group: group.id,
        }
      })
      // SORT para los criterios calculados previamente
      teamsClassifications.sort((a, b) => {
        return (
          b.won - a.won
          || b.dif - a.dif
          || b.technique - a.technique
          || b.energy - a.energy
          || (b.maxAvgEnergy || 0) - (a.maxAvgEnergy || 0)
        )
      })

      teamsClassifications.forEach(it => {
        it.order = order++
      })

      return Promise.all(teamsClassifications.map((tC) => {
        return this.mongodb.updateClassification({
          where: { id: tC.id }, data: { order: tC.order }
        })
      }))
    })

  }

  async updateNextCombats() {
    this._checkRoot();
    const group = this.get('group');
    const combatsToUpdate = {};
    const classificacionEntity = new Classification();
    if (group) {
      const combatsTeam1Group = await this.mongodb.combats({ where: { team1Group: group, status: 'P' } })
      const combatsTeam2Group = await this.mongodb.combats({ where: { team2Group: group, status: 'P' } })
      const combatsMerged = combatsTeam1Group.concat(combatsTeam2Group).reduce((acc, combat) => {
        return {
          ...acc,
          [combat.id]: {
            ...combat
          }
        }
      }, {})
      for (const combatId in combatsMerged) {
        const combat = combatsMerged[combatId];
        combatsToUpdate[combat.id] = { team1: null, team2: null }
        const classification1 = (await this.mongodb.classifications({ where: { group: combat.team1Group, order: parseInt(combat.team1Pos) } }))[0]
        const classification2 = (await this.mongodb.classifications({ where: { group: combat.team2Group, order: parseInt(combat.team2Pos) } }))[0]
        if (classification1) {
          await classificacionEntity.findById(classification1.id)
          const team1 = await classificacionEntity.team()
          combatsToUpdate[combat.id].team1 = team1.id

        }
        if (classification2) {
          await classificacionEntity.findById(classification2.id)
          const team2 = await classificacionEntity.team()
          combatsToUpdate[combat.id].team2 = team2.id
        }
      }
    } else {
      const team1 = await this.team1();
      const team2 = await this.team2();
      const winner = this.get('winner');
      const teamsCombats1 = await this.mongodb.combats({ where: { team1Combat: { id: this.get('id') }, } });
      const teamsCombats2 = await this.mongodb.combats({ where: { team2Combat: { id: this.get('id') }, } });
      const combatsMerged = teamsCombats1.concat(teamsCombats2);
      for (const combat of combatsMerged) {
        const combatEntity = new Combat().set(combat)
        const team1Combat = await combatEntity.team1Combat()
        const team2Combat = await combatEntity.team2Combat()
        combatsToUpdate[combat.id] = {};
        if (combat.team1Pos === 'W' && combat.team2Pos === 'W') { // FINAL
          if (this.get('id') === team1Combat.id) {
            combatsToUpdate[combat.id].team1 = winner == 1 ? team1.id : team2.id;
          } else if (this.get('id') === team2Combat.id) {
            combatsToUpdate[combat.id].team2 = winner == 2 ? team2.id : team1.id;
          }
        } else { // 3th & 4th
          if (this.get('id') === team1Combat.id) {
            combatsToUpdate[combat.id].team1 = winner == 2 ? team1.id : team2.id;
          } else if (this.get('id') === team2Combat.id) {
            combatsToUpdate[combat.id].team2 = winner == 1 ? team2.id : team1.id;
          }
        }
      }
    }
    await Promise.all(Object.keys(combatsToUpdate).map((combatId) => {
      const data = {}
      if (combatsToUpdate[combatId].team1) {
        data.team1 = { connect: { id: combatsToUpdate[combatId].team1 } }
      }
      if (combatsToUpdate[combatId].team2) {
        data.team2 = { connect: { id: combatsToUpdate[combatId].team2 } }
      }
      return this.mongodb.updateCombat({
        where: { id: combatId },
        data
      });
    }));

  }

  async updateTournamentRanking() {
    try {

      const type = parseInt(this.get('type'))
      const team1Position = this.get('winner') === 1 ? type : type + 1
      const team2Position = this.get('winner') === 2 ? type : type + 1
      const team1 = await this.team1()
      const team2 = await this.team2()

      const team = new Team()
      await team.findById(team1.id)
      await team.update({
        rankingPosition: team1Position
      })
      await team.findById(team2.id)
      await team.update({
        rankingPosition: team2Position
      })
      if (type === 1) {
        // TODO usar entidad torneo
        const tournament = await this.tournament()
        await this.mongodb.updateTournament({
          where: { id: tournament.id }, data: {
            finished: true
          }
        });
        // Finalizar torneo -> Calcular 5 al 8vo 
        const teamsClassifications = await this.mongodb.classifications({ where: { tournament: { id: tournament.id }, team: { rankingPosition: null } } })
        teamsClassifications.sort((teamA, teamB) => {
          return (
            teamA.order - teamB.order   // ASC y el resto DESC
            || teamB.won - teamA.won
            || teamB.pf - teamA.pf
            || teamB.technique - teamA.technique
            || teamB.energy - teamA.energy
            || (teamB.maxAvgEnergy || 0) - (teamA.maxAvgEnergy || 0)
          )
        })
        const teamMap = {}
        let ranking = 5;
        for (const teamClassiffication of teamsClassifications) {
          const teamClassDB = await this.mongodb.classification({ id: teamClassiffication.id }).team()
          if (!teamMap[teamClassDB.id]) {
            teamMap[teamClassDB.id] = {
              rankingPosition: ranking
            }
            ranking++;
          }
        }
        await Promise.all(Object.keys(teamMap).map(async teamId => {
          await this.mongodb.updateTeam({
            where: { id: teamId }, data: { rankingPosition: teamMap[teamId].rankingPosition }
          })
        }))
        /** Calcular GlobalRanking, cuando ya no hayan torneos pendientes en el dia*/

        /*         
                const startOfDay = new Date(tournament.date);
                startOfDay.setUTCHours(0, 0, 0, 0); 
                const endOfDay = new Date(tournament.date);
                endOfDay.setUTCHours(23, 59, 59, 999);
                  const existsTournamentsPending = await this.mongodb.$exists.tournament({ 
                      date_gte: new Date(tournament.date).getDate(), 
                      date_gte: new Date(tournament.date).getDate(), 
                      date_lte: new Date(tournament.date).getDate(), 
                      finished: false,
                  })
                if (!existsTournamentsPending) {
                  Tournament.updateGlobalRanking()
                } 
        */
      }
    } catch (error) {
      console.error(`Error trying to updateTournamentRanking while finishing combat:${this.get('id')}`, error)
    }
  }

  /**
   * 
   * @param {import('../datasources/mongodb/generated/prisma-client').User} user 
   */
  async addReferee(email) {
    this._checkRoot()
    const referee = await this.mongodb.user({ email });
    if (!referee) {
      throw new Error('REFEREE_NOT_FOUND')
    }
    return this.mongodb.updateCombat({
      where: { id: this.get('id') },
      data: {
        referees: {
          connect: {
            id: referee.id
          }
        }
      }
    }).referees()

  }
  /**
   * 
   * @param {import('../datasources/mongodb/generated/prisma-client').User} user 
   */
  async removeReferee(email) {
    this._checkRoot()
    const referee = await this.mongodb.user({ email });
    if (!referee) {
      throw new Error('REFEREE_NOT_FOUND')
    }
    return this.mongodb.updateCombat({
      where: { id: this.get('id') },
      data: {
        referees: {
          disconnect: {
            id: referee.id
          }
        }
      }
    }).referees()

  }

  async tournament() {
    this._checkRoot()
    return this.mongodb.combat({ id: this.get('id') }).tournament()
  }

  async phase() {
    this._checkRoot()
    // Recuperar Fase de un combate
    const tournament = await this.tournament()
    const handler = new Tournament({ mongodb: this.mongodb }).set(tournament)
    const result = await handler.phases()
    return result.find(el => el.id === this.get('phase'))
  }

  async group() {
    this._checkRoot()
    // Recuperar grupo de un combate
    const tournament = await this.tournament()
    const handler = new Tournament({ mongodb: this.mongodb }).set(tournament)
    const result = await handler.groups()
    return result.find(el => el.id === this.get('group'))
  }
  /**
   * 
   * @returns {import('../datasources/mongodb/generated/prisma-client').TeamPromise}
   */
  async team1() {
    this._checkRoot()
    return this.mongodb.combat({ id: this.get('id') }).team1()
  }
  async team1Combat() {
    this._checkRoot()
    return this.mongodb.combat({ id: this.get('id') }).team1Combat()
  }
  async team2Combat() {
    this._checkRoot()
    return this.mongodb.combat({ id: this.get('id') }).team2Combat()
  }

  async teamDesc(sTeam) {
    this._checkRoot()
    if (['W', 'L'].indexOf(this.get(`${sTeam}Pos`)) > -1) {
      const combat = await this.mongodb.combat({ id: this.get('id') })[`${sTeam}Combat`]()
      return combat ? combat.name : '????'
    } else {
      const tournament = await this.tournament()
      const handler = new Tournament({ mongodb: this.mongodb }).set(tournament)
      const result = await handler.groups()

      const group = result.find(el => el.id === this.get(`${sTeam}Group`))

      return group ? group.name : '????'
    }

  }
  /**
   * 
   * @returns {import('../datasources/mongodb/generated/prisma-client').TeamPromise}
   */
  async team2() {
    this._checkRoot()
    return this.mongodb.combat({ id: this.get('id') }).team2()
  }

  async referees() {
    this._checkRoot()
    return this.mongodb.combat({ id: this.get('id') }).referees()
  }

  async participants() {
    const bags = this.get('participants') || {}
    const ctx = this._getCtx()

    const team1Data = await this.team1()
    const team2Data = await this.team2()

    const result = { team1: [], team2: [] }

    if (team1Data) {
      const team1Handler = await new Team(ctx)
      await team1Handler.findById(team1Data.id)
      await team1Handler.include('fitboxers')

      for (const it of team1Handler.get('fitboxers')) {
        result.team1.push({
          id: it.id,
          name: it.nickname || it.firstName,
          logo: it.photo ? `${process.env.S3_URL}${it.photo}`: null,
          bag: bags[it.id] ? parseInt(bags[it.id]) : 0,
          power: 100
        })
      }
    }

    if (team2Data) {
      const team2Handler = await new Team(ctx)
      await team2Handler.findById(team2Data.id)
      await team2Handler.include('fitboxers')

      for (const it of team2Handler.get('fitboxers')) {
        result.team2.push({
          id: it.id,
          name: it.nickname || it.firstName,
          logo: it.photo ? `${process.env.S3_URL}${it.photo}`: null,
          bag: bags[it.id] ? parseInt(bags[it.id]) : 0,
          power: 100
        })
      }
    }

    return result
  }
  /**
   * Returns referee vote for team, votes values could be 1 for team 1 and 2 for team 2 
   * @param {number} comboNumber 
   * @returns {Promise<Object.<string, {coordination:number, technique:number, hits:number, ranges: number}>>}
   * @example
   *  // returns { '6295f7db0274390009e02afd' : {coordination:1,  technique: 1, hits: 1, ranges: 2} }
   */
  async votes(comboNumber) {
    this._checkRoot()
    const scores = (await this.mongodb.combat({ id: this.get('id') }).scores())
    if (comboNumber) {
      const combo = scores.find(it => it.number === comboNumber)
      if (combo) return combo.votes
      return null
    } else {
      throw new Error('combo number must be defined to get votes')
    }

  }

  /**
   * @typedef {import('../datasources/mongodb/generated/prisma-client').ComboScore  ComboScore}
   * @param {*} comboNumber 
   * @param {*} refereeId 
   * @returns {Array<ComboScore> | ComboScore }
   */
  async scores(comboNumber, refereeId) {
    this._checkRoot()
    const result = await this.mongodb.combat({ id: this.get('id') }).scores()
    if (comboNumber) {
      const combo = result.find(el => el.number === comboNumber)
      if (combo && refereeId) {
        return combo.votes[refereeId]
      }
      return combo
    }
    return result
  }
  /**
   * 
   * @param {Array<object>} scores
   * @returns { 
   *  ['team1' | 'team2']: {
   *    technique: number
   *    coordination: number
   *    hits: number
   *    ranges: number
   *  }
   * }
   */
  calculateVotes(votes) {
    this._checkRoot()
    const teamAggregation = {
      team1: {
        technique: 0,
        coordination: 0,
        hits: 0,
        ranges: 0
      }, team2: {
        technique: 0,
        coordination: 0,
        hits: 0,
        ranges: 0
      }
    }
    for (const refereeId in votes) {
      const vote = votes[refereeId]
      if (vote) {
        teamAggregation.team1.technique += vote.technique === 1 && 1
        teamAggregation.team1.coordination += vote.coordination === 1 && 1
        teamAggregation.team1.hits += vote.hits === 1 && 1
        teamAggregation.team1.ranges += vote.ranges === 1 && 1

        teamAggregation.team2.technique += vote.technique === 2 && 1
        teamAggregation.team2.coordination += vote.coordination === 2 && 1
        teamAggregation.team2.hits += vote.hits === 2 && 1
        teamAggregation.team2.ranges += vote.ranges === 2 && 1
      }
    }
    return teamAggregation
  }

  async scoresSummary(comboNumber) {
    this._checkRoot()
    const scores = comboNumber ? [await this.scores(comboNumber)] : await this.scores()
    const acc = {
      team1: {
        energy: 0,
        technique: 0,
        coordination: 0,
        hits: 0,
        ranges: 0
      }, team2: {
        energy: 0,
        technique: 0,
        coordination: 0,
        hits: 0,
        ranges: 0
      }
    }
    for (const sc of scores) {
      if(sc) {
        acc.team1.energy += sc.energy === 0 ? 1 : (sc.energy === 1 ? 2 : 0)
        acc.team1.technique += sc.technique === 1 ? 2 : 0
        acc.team1.coordination += sc.coordination === 1
        acc.team1.hits += sc.hits === 1
        acc.team1.ranges += sc.ranges === 1
        // Team 2
        acc.team2.energy += sc.energy === 0 ? 1 : (sc.energy === 2 ? 2 : 0)
        acc.team2.technique += sc.technique === 2 ? 2 : 0
        acc.team2.coordination += sc.coordination === 2
        acc.team2.hits += sc.hits === 2
        acc.team2.ranges += sc.ranges === 2
      }
    }
    let total = 0
    for (const key in acc.team1) {
      total += acc.team1[key]
    }
    acc.team1.total = total

    total = 0
    for (const key in acc.team2) {
      total += acc.team2[key]
    }
    acc.team2.total = total
    const team1 = await this.team1();
    const team2 = await this.team2();
    acc.team2 = { ...acc.team2, ...team2 }
    acc.team1 = { ...acc.team1, ...team1 }
    return acc
  }
}

module.exports = Combat