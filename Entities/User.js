const SuperClass = require('./main/SuperClass')
const Team = require('./Team')
const { prisma: mongoBF } = require('../../../brooklyn-server/mongodb/generated/prisma-client')

const FITBOXER_TYPE = {
  1: "WEB OFFER",
  2: "FIRST BOOKING",
  3: "DEBUTANT",
  4: "CLIENT",
  5: "MEMBER",
  6: "EXMEMBER"
}

class User extends SuperClass {

  constructor(ctx) {
    super(ctx || SuperClass.fakeCtx())
  }

  hasTeam() {
    return this.get('team') !== undefined
  }

  isCaptain() {
    this._checkRoot()
    return this.get('isCaptain')
  }

  async findAll() {
    const result = await this.mongodb.users()
    return result
  }

  async listTrainers() {
    const result = await this.mongodb.users({ where: { type: 'TRAINER' } })
    return result
  }

  // async listFitboxers() {
  //   const list = await this.mongodb.users({ where: { type: 'FITBOXER' } })
  //   const result = []
  //   let i, j, temporary, chunk = 20;
  //   for (i = 0, j = list.length; i < j; i += chunk) {
  //     temporary = list.slice(i, i + chunk);
  //     await Promise.all(temporary.map(async it => {
  //       const hasTeam = (await this.mongodb.teams({ where: { fitboxers_some: { email: it.email } } }))[0]
  //       result.push({
  //         ...it,
  //         team: hasTeam
  //       })
  //     }))
  //   }
  //   return result
  // }

  async findById(id) {
    const result = await this.mongodb.user({ id })
    this.set(result);
    return this
  }

  async findByNickname(nickName) {
    const result = await this.mongodb.user({ nickName })
    const team = await this.mongodb.teams({ where: { fitboxers_some: { nickName } } })[0]
    this.set({ ...result, team });
    return this
  }

  async findByEmail(email) {
    const result = await this.mongodb.user({ email })
    const team = await this.mongodb.teams({ where: { fitboxers_some: { email } } })[0]
    this.set({ ...result, team });
    return this
  }

  async checkEmailBFI(email) {
    try {
      const v7response = await this.velneo.$Process.exec('4rklzpyg.vca/WEB_CHK_EML', {
        EML: email
      })
      if (v7response.code === 2) {
        const mongoRecord = await mongoBF.fitboxer({ v7_id: v7response.id })
        let mongoId = null
        if (mongoRecord) {
          mongoId = mongoRecord.id
        }
        return {
          status: true,
          id: mongoId, // MongoId, null si no se ha migrado
          v7_id: v7response.id,
          type: FITBOXER_TYPE[mongoRecord.status]
        }
      } else {
        return { status: false, message: 'USER_NOT_FOUND' }
      }
    } catch (err) {

      throw { statusCode: 500, message: err }
    }
  }

  /** */
  async updateProfile(data) {
    this._checkRoot()
    const { id } = this.get()
    const user = await this.mongodb.updateUser({
      data,
      where: {
        id
      }
    })
    return user
  }

  async save() {
    this._checkRoot()
    const data = this.get()
    // const sanitizeFields = (it) => it.toLowerCase().replace(/\s+/, ' ').trim()
    const requiredfields = ['email', 'firstName', 'lastName', 'password', 'phone'];
    let missingFields = requiredfields.filter(it => (data[it] === undefined || data[it] === ""))
    if (missingFields.length > 0) {
      throw { status: false, message: `${missingFields.join(',')} must be specified` }
    }
    const res = await this.mongodb.createUser(data)
    return res
  }

  async delete() {
    this._checkRoot()
    const res = await this.mongodb.deleteUser({
      email: this.get('email')
    })
    return res
  }

  photo() {
    this._checkRoot()
    // const teamEntity = new Team();
    if (this.get('photo')){
      return `${process.env.S3_URL}${this.get('photo')}`
    } else {
      return null
    }
  }

}

class Trainer extends User {
  constructor(ctx, data) {
    super(ctx || SuperClass.fakeCtx())
    this.set({ ...data, type: 'TRAINER' })
  }

  async findAll({ size = 100, page = 1, count = false }) {
    if (count) {
      const result = await this.mongodb.usersConnection({ where: { type: 'TRAINER' } }).aggregate().count()
      return result
    }

    const skip = size * (page - 1)
    const result = await this.mongodb.users({ where: { type: 'TRAINER' }, first: size, skip })
    return result
  }

  async findById(id) {
    const result = await this.mongodb.user({ id })
    if (result) {
      const team = await this.mongodb.teams({ where: { createdBy: { id } } })
      this.set({ ...result, team });
    }
    return this
  }

  async findByEmail(email) {
    const result = await this.mongodb.user({ email })
    const team = await this.mongodb.teams({ where: { createdBy: { email } } })[0]
    this.set({ ...result, team });
    return this
  }

  async combats(tournamentId, status /* Opcional */) {
    this._checkRoot()
    const where = {
      tournament: { // tournmnt
        id: tournamentId,
      },
      createdBy: { // Trainer
        id: this.get('id')
      }
    }
    const trainerTeamsOnTournament = (await this.mongodb.teams({
      where: where
    })).map(it => it.id)

    const statusCondition = {}
    if (status) {
      if (Array.isArray(status)) {
        statusCondition.status_in = status
      } else {
        statusCondition.status = status
      }
    }
    const combats1 = await this.mongodb.combats({
      where: {
        tournament: {
          id: tournamentId,
        },
        team1: {
          id_in: trainerTeamsOnTournament,
        },
        ...statusCondition,
      },
      orderBy: 'time_ASC'
    })
    const combats2 = await this.mongodb.combats({
      where: {
        tournament: {
          id: tournamentId,
        },
        team2: {
          id_in: trainerTeamsOnTournament,
        },
        ...statusCondition,
      },
      orderBy: 'time_ASC'
    })

    const result = [...combats2]
    const idCombats2List = combats2.map(it => it.id)
    for (const combat of combats1) {
      if (!idCombats2List.includes(combat.id)) {
        result.push(combat)
      }
    }
    return result.sort((a, b) => a.time - b.time)
  }

  async tournaments(teamId) {
    let teamsIdToSearch = [teamId]
    if (!teamId) {
      const teams = await this.teams()
      teamsIdToSearch = teams.map(it => it.id)
    }
    return await this.mongodb.tournaments({
      where: {
        teams_some: {
          id_in: teamsIdToSearch
        },
      }
    })
  }

  async teams() {
    this._checkRoot()
    return await this.mongodb.teams({
      where: {
        clubId_not: null,
        createdBy: {
          id: this.get('id')
        }
      }
    })
  }

  async delete() {
    this._checkRoot()
    const teamsWithFitboxer = []
    const teams = await this.teams()
    for (const team of teams) {
      const fitboxers = await this.mongodb.team({ id: team.id }).fitboxers()
      if (fitboxers.length) {
        teamsWithFitboxer.push(team)
      }
    }
    if (teamsWithFitboxer.length) {
      throw Error(`${this.root.email} tiene equipos con fitboxers, teams:[${JSON.stringify(teamsWithFitboxer.map(t => t.code))}]`)
    } else {
      teams.forEach(async (team) => {
        const teamHandler = new Team();
        await teamHandler.findById(team.id)
        await teamHandler.delete()
      })
      super.delete()
    }
  }

}

class Fitboxer extends User {
  constructor(ctx, data) {
    super(ctx || SuperClass.fakeCtx())
    this.set({ ...data, type: 'FITBOXER' })
  }

  async findAll({ size = 100, page = 1, count = false }) {
    if (count) {
      const result = await this.mongodb.usersConnection({ where: { type: 'FITBOXER' } }).aggregate().count()
      return result
    }

    const skip = size * (page - 1)
    const result = await this.mongodb.users({
      where: { type: 'FITBOXER' },
      first: size,
      skip
    })
    return result
  }

  async findById(id) {
    const result = await this.mongodb.user({ id })
    // const teams = await this.mongodb.teams({ where: { fitboxers_some: { id } } })
    this.set(result);
    return this
  }

  async team() {
    this._checkRoot()
    // const teamEntity = new Team();
    if (this.get('teamId')) {
      return this.mongodb.team({ id: this.get('teamId') })
    }
    return null
  }

  async delete() {
    this._checkRoot()
    if (this.get('teamId')) {
      const teamEntity = new Team()
      await teamEntity.findById(this.get('teamId'))
      await teamEntity.removeFitboxer(this.get('id'))
    }
    return super.delete()
  }
  /**
   * 
   * @param {string} teamId
   * @returns 
   */
  async setTeam(teamId) {
    this._checkRoot()
    const result = await this.mongodb.updateUser({ where: { id: this.get('id') }, data: { teamId } })
    return result
  }
  /**
   * 
   * @param {string} teamId
   * @returns 
   */
  async setCaptain() {
    this._checkRoot()
    const result = await this.mongodb.updateUser({ where: { id: this.get('id') }, data: { isCaptain: true } })
    return result
  }
}

class Referee extends User {

  constructor(ctx, data) {
    super(ctx || SuperClass.fakeCtx())
    this.set({ ...data, type: 'REFEREE' })
  }

  async findAll() {
    const result = await this.mongodb.users({ where: { type: 'REFEREE' } })
    return result || []
  }

  async tournaments() {
    this._checkRoot()
    const tournamentList = await this.mongodb.tournaments({
      where: {
        referees_some: {
          id: this.get('id')
        }
      }
    })
    return tournamentList
  }

  async combats(tournamentId, status) {
    this._checkRoot()
    let idsToSearch = [tournamentId]
    let where = {}

    if (status) {
      if (Array.isArray(status)) {
        where.status_in = status
      } else {
        where.status = status
      }
    }
    if (!tournamentId) {
      const tournamentList = await this.tournaments()  // referee's tournaments
      idsToSearch = tournamentList.map(it => it.id)
    }
    const combatsList = await this.mongodb.combats({
      where: {
        tournament: {
          id_in: idsToSearch
        },
        referees_some: {
          id_in: [this.get('id')]
        },
        ...where,
      },
      orderBy: 'time_ASC'
    })
    return combatsList
  }
}

module.exports = {
  User,
  Fitboxer,
  Trainer,
  Referee
}

