const SuperClass = require('./main/SuperClass')
const crypto = require('crypto');
const MAX_TEAM_SIZE = 5;
const AwsS3 = require('../lib/AwsS3');
const District = require('../../../brooklyn-server/abstractTypes/District');
const awsS3 = AwsS3.init(process.env)

class Team extends SuperClass {

  constructor(ctx) {
    super(ctx || SuperClass.fakeCtx())
  }

  // async isLockedForRegistration() {

  // }
  // async lock() {
    
  // }
  
  // async unlock() {
    
  // }

  async update(data) {
    this._checkRoot()
    const { id } = this.get()
    const result = await this.mongodb.updateTeam({ where: { id }, data })
    this.set(result);
    await this.deleteCache()
    return result;
  }
  /**
   * 
   * @param {string} tournamentId 
   * @returns 
   */
  async assignTournament(tournamentId) {
    this._checkRoot()
    await this.mongodb.updateTeam({
      where: {
        id: this.get('id')
      },
      data: {
        registrationDate: new Date(),
        tournament: {
          connect: {
            id: tournamentId
          }
        }
      }
    })
  }
  /**
  * 
  * @param {string} tournamentId 
  * @returns 
  */
  async removeFromTournament(tournamentId) {
    this._checkRoot()
    await this.mongodb.updateTeam({
      where: {
        id: this.root.id
      },
      data: {
        tournament: {
          disconnect: true
        }
      }
    })
    await this.mongodb.updateManyOrders({
      where: {
        teamCode: this.get('code'),
        status: 'COMPLETED',
        tournamentId
      },
      data: {
        status: 'CANCELED',
        reason: 'Team removed from tournament'
      }
    })
  }
  async createCode(trainerId) {
    if (!trainerId) {
      throw new Error("trainer must be defined")
    }
    const trainer = await this.mongodb.user({ id: trainerId })
    const { id, code } = await this.mongodb.createTeam({
      code: Math.random().toString(36).slice(4).substring(0, 9),
      createdBy: {
        connect: {
          id: trainer.id
        }
      }
    })
    return {
      id,
      code
    }
  }
  async delete() {
    this._checkRoot()
    const team = this.get()
    if (team.fitboxers && team.fitboxers.length) {
      throw { statusCode: 400, message: 'TEAM_WITH_FITBOXERS' }
    }
    await this.mongodb.deleteTeam({ code: team.code })
    return team

  }

  async createTeamOrder(fitboxerId, code, name, motto) {
    try {
      const user = await this.mongodb.users({ where: { id: fitboxerId, type: 'FITBOXER' } });
      if (!user.length) {
        throw {
          status: false,
          message: 'FITBOXER_NOT_FOUND'
        }
      }
      const team = await this.mongodb.team({ code })
      if (!team) {
        throw {
          status: false,
          message: 'TEAM_CODE_NOT_FOUND'
        }
      }
      const checkFitboxerAlreadyInAnotherTeam = await this.mongodb.teams({ where: { fitboxers_some: { id: fitboxerId } } })
      if (checkFitboxerAlreadyInAnotherTeam.length) {
        throw {
          status: false,
          message: 'FITBOXER_IN_ANOTHER_TEAM'
        }
      }
      const fitboxers = team.fitboxers;
      if (fitboxers.length === MAX_TEAM_SIZE) {
        throw {
          status: false,
          message: 'TEAM_REACHED_MAX_SIZE'
        }
      }

      let buffer = crypto.randomBytes(22)
      const tokenOrder = buffer.toString('hex')
      await this.cache.setCache(`team-order:${tokenOrder}`, {
        fitboxerId, code, name, motto
      }, 3600 * 24)

      return tokenOrder
    } catch (err) {
      console.error(err)
      throw err
    }
  }
  /**
   * 
   * @returns {import('../../../brooklyn-server/mongodb/generated/prisma-client/index').District}
   */
  async club() {
    this._checkRoot()

    if (!this.get('clubId')) {
      return ''
    }
    const districtHandler = new District();

    return districtHandler.findById(this.get('clubId'))
  }

  async hasFitboxers() {
    const res = await this.get('fitboxers')
    return res.length > 0;
  }

  async addFitboxer(email) {
    this._checkRoot();
    const fitboxers = await this.fitboxers()
    const checkFitboxerAlreadyInAnotherTeam = await this.mongodb.teams({ where: { fitboxers_some: { email } } })
    if (checkFitboxerAlreadyInAnotherTeam.length) {
      throw {
        status: false,
        message: 'FITBOXER_IN_ANOTHER_TEAM'
      }
    }
    if (fitboxers.length >= MAX_TEAM_SIZE) {
      throw {
        status: false,
        message: 'TEAM_REACHED_MAX_SIZE'
      }
    }
    await this.mongodb.updateTeam({
      where: { code: this.get('code') },
      data: {
        fitboxers: {
          connect: {
            email
          }
        }
      }
    })
    return true
  }

  async removeFitboxer(fitboxerId, code) {
    if (!code) {
      this._checkRoot()
    } else {
      await this.findByCode(code)
    }
    if (!this.get()) {
      throw { statusCode: 404, message: 'TEAM_NOT_FOUND' }
    }
    const fitboxers = await this.fitboxers()
    const exists = fitboxers.some(it => { return it.id === fitboxerId })
    if (!exists) {
      throw { statusCode: 404, message: 'FITBOXER_NOT_FOUND_IN_TEAM' }
    }
    const user = (await this.mongodb.users({ where: { id: fitboxerId, type: 'FITBOXER' } }))[0];

    if (!user) {
      throw { statusCode: 404, message: 'FITBOXER_NOT_FOUND' }
    }

    await this.mongodb.updateUser({
      where: { id: user.id },
      data: {
        isCaptain: false,
        teamId: null
      }
    })
    const resTeam = await this.mongodb.updateTeam({
      where: { code: this.get('code') },
      data: {
        fitboxers: {
          disconnect: {
            id: user.id
          }
        }
      }
    })
    await this.refresh()
    const newCaptain = (await this.fitboxers()).sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt))[0]
    if (newCaptain) {
      await this.mongodb.updateUser({
        where: { id: newCaptain.id },
        data: {
          isCaptain: true
        }
      })
    }
    return resTeam
  }

  async updateTeam(name, motto, trainerId) {
    this._checkRoot()
    let data = {
      name, motto
    }
    if (trainerId) {
      data.createdBy = {
        connect: {
          id: trainerId
        }
      }
    }
    const res = await this.mongodb.updateTeam({
      where: { id: this.get('id') },
      data
    })
    await this.deleteCache()
    return res
  }

  /**
   * 
   * @param {import('../datasources/mongodb/generated/prisma-client').TeamWhereInput} where 
   * @param {import('../datasources/mongodb/generated/prisma-client').Tea} where 
   * @returns 
   */
  async findAll(where = {}, { size = 100, page = 1, count = false }) {
    if (count) {
      return this.mongodb.teamsConnection({
        where,
      }).aggregate().count()
    }
    const skip = size * (page - 1)
    return this.mongodb.teams({ where, skip, first: size })
  }

  async findById(id) {
    if (await this.getFromCache(id)) return this.get()
    const result = await this.mongodb.team({ id })
    this.set(result)
    await this.updateCache(60 * 60 * 4)
    return this.get();
  }

  async findByCode(code) {
    const result = await this.mongodb.team({ code })
    this.set(result)
    return this;
  }

  async findByName(name) {
    return this.mongodb.teams({ name })
  }


  async findByFitboxerId(fitboxerId) {
    const result = (await this.mongodb.teams({ where: { fitboxers_some: { id: fitboxerId } } }))[0]
    if (result === undefined) return null
    return result
  }

  // Includes

  /**
   * 
   * @returns {Array<import('../datasources/mongodb/generated/prisma-client').User>}
   */
  async fitboxers() {
    this._checkRoot()
    return this.mongodb.team({ id: this.get('id') }).fitboxers({ orderBy: 'createdAt_ASC' })
  }

  async trainer() {
    this._checkRoot()
    return this.mongodb.team({ id: this.get('id') }).createdBy()
  }

  async logo() {
    this._checkRoot()
    if (this.get('logo')) {
      return awsS3.getUrlFromBucket(this.get('logo'))
    }
    return null
  }

  async captain() {
    this._checkRoot()
    const captain = (await this.fitboxers()).find((fb) => fb.isCaptain === true)
    return captain
  }

  async orderAvailable(tournamentId) {
    this._checkRoot()
    const where = {}
    if (tournamentId) {
      where.tournamentId = tournamentId
    }

    const order = (await this.mongodb.orders({
      where: {
        teamCode: this.get('code'),
        status_in: ['CREATED', 'IN_PROGRESS'],
        expirationDate_gte: new Date(),
        ...where
      }
    }))[0]
    return order
  }

  async tournament() {
    this._checkRoot()
    return this.mongodb.team({ id: this.get('id') }).tournament()
  }
  /**
    * Return combats for team, without duplicates 
    * @param {{status:string,group:string,phase:string, tournament:{id:string}}} object 
    */
  async combats({ status, group, phase, tournament } = {}) {
    this._checkRoot()
    const conditions = { status, group, phase, tournament };
    const where = {};
    for (const key in conditions) {
      if (conditions[key]) {
        where[key] = conditions[key];
      }
    }
    const where1 = { ...where, team1: { id: this.get('id') } }
    const where2 = { ...where, team2: { id: this.get('id') } }
    const combatsTeam1 = await this.mongodb.combats({
      where: where1
    })
    const combatsTeam2 = await this.mongodb.combats({
      where: where2
    })
    const combatsMerged = combatsTeam1.concat(combatsTeam2).reduce((acc, it) => {
      return {
        ...acc,
        [it.id]: {
          ...it
        }
      }
    }, {})

    return Object.keys(combatsMerged).map(combatId => combatsMerged[combatId])
  }

}

module.exports = Team
