const { prisma: mongodb } = require('../../datasources/mongodb/generated/prisma-client')
const velneo = require('../../../../brooklyn-server/datasources/velneo')


const cache = require('../../lib/cache.store')

class SuperClass {
    /**
     * @type {cache}
     */
    cache;
    /**
     * @type {mongodb}
     */
    mongodb;
    /**
     * 
     * @param {object?} ctx 
     */
    constructor(ctx, cachePolicy) {

        this.mongodb = ctx.mongodb || mongodb
        this.velneo = ctx.velneo || velneo
        this.cache = ctx.cache || cache
        this.session = ctx.session
        this.role = ctx.role
        this.root = null
        this.oldRoot = null
        if (cachePolicy) {
            this.cachePolicy = cachePolicy
        } else {
            this.cachePolicy = {
                id: 'id', expireIn: 60 * 60 * 4 // 4 horas
            }
        }
    }
    /**
     * @description Establece el Data Transfer Object del modelo 
     * @param {object} data 
     * @returns {this}
     */
    set(data) {
        if (typeof data === 'object') {
            if (this.root) {
                const oldData = { ...this.root }
                this.oldRoot = oldData
                this.root = {
                    ...oldData,
                    ...data
                }
            } else {
                this.root = data
            }
        }
        return this // chain
    }
    /**
     * @description Devuelve el Data Transfer Object del modelo establecido o el valor de una propiedad si se indica
     * @param {string} property Optional 
     * @returns {object} Data Transfer Object
     */
    get(prop) {
        return prop ? this.root[prop] : this.root
    }

    get data() {
        return this.root
    }

    propChanges() {
        const changes = []
        for (const prop in this.root) {
            if (this.root[prop] !== this.oldRoot[prop]) changes.push(prop)
        }
        return changes
    }

    propHasChanged(prop) {
        return this.root[prop] !== this.oldRoot[prop]
    }

    somePropHasChanged(props) {
        return props && props.some((f) => this.propHasChanged(f))
    }

    oldProp(prop) {
        return this.oldRoot[prop]
    }

    /**
     * @description Fuerza un refresco del Data Transfer Object que está en caché y lo devuelve
     * @returns {Data Transfer Object}
     */
    async refresh() {
        this._checkRoot()
        const cacheString = `${this.constructor.name}:${this.root[this.cachePolicy.id]}`
        await this.cache.delCache(cacheString)
        return this.findById(this.root.id)
    }

    /**
    * @description Fuerza un refresco del Data Transfer Object que está en caché y lo devuelve
    * @returns {Data Transfer Object}
    */
    async getFromCache(id) {
        if (!id) this._checkRoot()
        const cacheString = `${this.constructor.name}:${id || this.root[this.cachePolicy.id]}`
        const cacheInfo = await this.cache.getCache(cacheString)
        if (!cacheInfo) {
            return null
        }
        this.set(JSON.parse(cacheInfo))
        return this.get()
    }

    async updateCache(expireIn) {
        this._checkRoot()
        const cacheString = `${this.constructor.name}:${this.root[this.cachePolicy.id]}`
        await this.cache.setCache(cacheString, this.root, expireIn || this.cachePolicy.expireIn)
        return this.get()
    }

    async deleteCache(id) {
        if (!id) this._checkRoot()
        const cacheString = `${this.constructor.name}:${id || this.root[this.cachePolicy.id]}`
        return this.cache.delCache(cacheString)
    }
    /**
     * 
     * @param {*} m 
     * @returns 
     */
    async include(m) {
        const methods = typeof m === 'string' ? [m] : m
        for (const fn of methods) {
            const data = await this[fn]()
            this.root[fn] = data
        }
        return this.get()
    }
    /**
     * 
     * @param {*} data 
     * @param {*} MAP_FIELDS 
     * @param {*} MAP_TRANSFORM 
     * @returns 
     */
    formatFields(data, MAP_FIELDS, MAP_TRANSFORM) {
        if (MAP_FIELDS) {
            for (const key in MAP_FIELDS) {
                const value = data[key]
                const field = MAP_FIELDS[key]
                if (typeof field === 'string') data[field] = value
                delete data[key]
            }
        }
        if (MAP_TRANSFORM) {
            for (const key in MAP_TRANSFORM) {
                const fn = MAP_TRANSFORM[key]
                if (typeof fn === 'function') data[key] = fn(data)
            }
        }

        return data
    }
    static fakeCtx(session) {
        const ctx = {
            mongodb,
            velneo,
            cache,
            session,
            // role: {
            //   role: 'SERVER'
            // }
        }
        return ctx
    }

    _checkRoot() {
        if (!this.root) {
            throw new Error('Data Transfer Object needs to be provided')
        }
    }

    _getCtx() {
        return {
            mongodb: this.mongodb,
            cache: this.cache,
            session: this.session,
            role: this.role
        }
    }
}

module.exports = SuperClass