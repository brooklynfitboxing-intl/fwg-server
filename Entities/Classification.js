const SuperClass = require('./main/SuperClass')
const Tournament = require('./Tournament')

class Classification extends SuperClass {

  constructor(ctx) {
    super(ctx || SuperClass.fakeCtx())
  }

  /**
   * 
   * @param {string} teamCode 
   * @returns {Classification}
   */
  async findById(id) {
    if (await this.getFromCache(id)) return this.get()
    const result = await this.mongodb.classification({ id })
    this.set(result)
    await this.updateCache(60 * 60 * 4)
    return this.get();
  }

  async tournament() {
    this._checkRoot()
    return this.mongodb.classification({ id: this.get('id') }).tournament()
  }

  async phase() {
    this._checkRoot()
    // Recuperar Fase de un combate
    const tournament = await this.tournament()
    const handler = new Tournament({ mongodb: this.mongodb }).set(tournament)
    const result = await handler.phases()
    return result.find(el => el.id === this.get('phase'))
  }

  async group() {
    this._checkRoot()
    // Recuperar grupo de un combate
    const tournament = await this.tournament()
    const handler = new Tournament({ mongodb: this.mongodb }).set(tournament)
    const result = await handler.groups()
    return result.find(el => el.id === this.get('group'))
  }

  async team() {
    this._checkRoot()
    return this.mongodb.classification({ id: this.get('id') }).team()
  }
}

module.exports = Classification