const { emailService } = require('../../../brooklyn-server/queues');
const { delCache, getCache, setCache } = require('../lib/cache.store');
const SuperClass = require('./main/SuperClass')
const Team = require('./Team');
const Tournament = require('./Tournament');
const { Fitboxer } = require('./User');

class Order extends SuperClass {
  /**
   * @type {import('../datasources/mongodb/generated/prisma-client').Order}
   */
  root;

  constructor(ctx) {
    super(ctx || SuperClass.fakeCtx())
  }

  async findById(id) {
    const result = await this.mongodb.order({ id })
    this.set(result)
    return this.get()
  }

  async save() {
    this._checkRoot()
    const data = this.get()
    const res = await this.mongodb.createOrder(data)
    return res
  }
  /**
   * @returns {import('../datasources/mongodb/generated/prisma-client/index').User}
   */
  async user() {
    this._checkRoot();
    return this.mongodb.user({ email: this.get('email') })
  }

  async team() {
    this._checkRoot();
    const user = await this.user()
    const userHandler = new Fitboxer();
    userHandler.set(user)
    return userHandler.team()
  }

  static async generateOrderForTeam(data) {
    try {
      const { tournamentId, teamCode } = data
      const { mongodb } = this.fakeCtx()
      const orders = await mongodb.orders({ where: { tournamentId, teamCode, status_in: ['IN_PROGRESS', 'FAILED', 'CREATED'] } })
      if (orders.length) {
        for (const order of orders) {
          await mongodb.updateOrder({ where: { id: order.id }, data: { status: 'CANCELED' } })
        }
      }
      const order = new Order();
      order.set(data)
      const orderCreated = await order.save();
      await order.findById(orderCreated.id)
      await order.sendOrderMail();
    } catch (err) {
      console.error(`Error trying to generate payment order for team:${data.teamCode} `, err)
    }
  }

  async sendOrderMail() {
    this._checkRoot();
    const teamEntity = new Team();
    await teamEntity.findByCode(this.get('teamCode'))
    const captain = await teamEntity.captain();
    const locale = captain.locale || 'en';
    emailService.add({
      to: captain.email,
      template: `fwg_team_registration_open_${locale}`,
      global_merge_vars: [
        { name: 'name', content: captain.firstName },
        { name: 'link', content: `${process.env.PUBLIC_BASE_URL}/${locale}/tournament/join/${this.get('tournamentId')}` }
      ]
    })
  }

  async sendOrderConfirmation() {
    this._checkRoot();
    const teamEntity = new Team();
    await teamEntity.findByCode(this.get('teamCode'));
    const captain = await teamEntity.captain();
    const fitboxers = await teamEntity.fitboxers();

    emailService.add({
      to: captain.email,
      template: `fwg_team_registration_confirmation_${captain.locale || 'en'}`,
      global_merge_vars: [
        { name: 'name', content: captain.firstName },
        { name: 'link', content: ` ` },
        { name: 'code', content: captain.id }
      ]
    })
    for (const fitboxer of fitboxers) {
      const locale = fitboxer.locale || 'en';
      emailService.add({
        to: fitboxer.email,
        template: `fwg_fitboxer_registration_confirmation_${locale || 'en'}`,
        global_merge_vars: [
          { name: 'name', content: captain.firstName },
          { name: 'link', content: ` ` },
          { name: 'code', content: fitboxer.id }
        ]
      })
    }
  }
  /**
    * @param {import('../datasources/mongodb/generated/prisma-client/index').OrderUpdateInput} data
    */
  async update(data) {
    this._checkRoot();
    await this.mongodb.updateOrder({
      where: { id: this.get('id') },
      data
    })
    return this.findById(this.get('id'))
  }

  /**
   * @typedef { 'FAILED' | 'COMPLETED' } OrderCompletionStatus
   * @param {OrderCompletionStatus} completionStatus
   * @param {import('../datasources/mongodb/generated/prisma-client/index').OrderUpdateInput} data
   */
  async completeOrder(completionStatus, data = {}) {
    this._checkRoot();
    const tournament  =  new Tournament();
    const team  =  await this.team();
    const teamHandler = new Team()
    teamHandler.set(team);
    const user  =  await this.user();
    await tournament.findById(this.get('tournamentId'));

    tournament.unlock(team, user)
    /**
     * Cancel rest of orders related to this tournament and captain
     */
    if (completionStatus === 'COMPLETED') {

      const hasTeam = await tournament.hasTeam(teamHandler)
      if (!hasTeam) {
        await tournament.addTeam(teamHandler)
        if (tournament.get('final')) {
          await this.sendOrderConfirmation();
        }
      }
  
      await this.mongodb.updateManyOrders({
        where: {
          tournamentId: this.get('tournamentId'),
          email: this.get('email'),
          id_not: this.get('id'),
          status_not_in: ['COMPLETED', 'FAILED'],
        }, data: {
          ...data,
          status: 'CANCELED',
        }
      })
    }
    await this.mongodb.updateOrder({
      where: { id: this.get('id') },
      data: {
        ...data,
        status: completionStatus
      }
    })
    return this.findById(this.get('id'));
  }

  paymentCompleteOrderUrl() {
    this._checkRoot();
    const FWG_API_URL = process.env.FWG_API_URL;
    return `${FWG_API_URL}/payments/adyen/complete_order/${this.get('tournamentId')}/${this.get('id')}`
  } 
  async redirectFailedUrl() {
    this._checkRoot();
    const FWG_API_URL = process.env.FWG_API_URL;
    const PUBLIC_BASE_URL = process.env.PUBLIC_BASE_URL;
    const user = await this.user();
    return `${PUBLIC_BASE_URL}/${user.locale}/tournament/confirm-registration/${this.get('tournamentId')}/` 
  }

  async paymentReturnUrl() {
    this._checkRoot();
    const PUBLIC_BASE_URL = process.env.PUBLIC_BASE_URL;
    const user = await this.user();
    return `${PUBLIC_BASE_URL}/${user.locale}/tournament/confirm-registration/${this.get('tournamentId')}/${this.get('id')}`
  }

  /**
   * @returns {import('../datasources/mongodb/generated/prisma-client').Order}
   */
  get(prop) {
    return super.get(prop);

  }

  async delete() {
    this._checkRoot()
    const res = await this.mongodb.order({ id: this.get('id') })
    return res
  }

}
module.exports = Order