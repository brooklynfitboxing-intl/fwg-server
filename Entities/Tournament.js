const SuperClass = require('./main/SuperClass')
const District = require('../../../brooklyn-server/abstractTypes/District');
const Team = require('./Team');
const { getCache, setCache, delCache } = require('../lib/cache.store');

const EXPIRATION_TIME_FLAG = 60 * 5
const MAX_TEAM_SIZE_ON_TOURNAMENT = 8
class Tournament extends SuperClass {
  static LOCK_LAST_SLOT = false;

  /**
   * 
   * @type {import('../datasources/mongodb/generated/prisma-client').Tournament }
   */
  root;
  tournamentLockedFlag 

  constructor(ctx) {
    super(ctx || SuperClass.fakeCtx())
  }
  /**
   * 
   * @param {string} teamCode 
   * @returns {Tournament}
   */
  async findById(id) {
    const result = await this.mongodb.tournament({ id })
    this.set(result)
    return this.get();
  }

  /**
  * 
  * @param {import('../datasources/mongodb/generated/prisma-client').TournamentWhereInput} where 
  * @returns {Tournament}
  */
  async find(where) {
    const result = await this.mongodb.tournaments({ where: where })
    return result;
  }

  /**
   * 
   * @returns {Array}
   */
  async findAll() {
    return this.mongodb.tournaments()
  }

  async final() {
    const result = (await this.mongodb.tournaments({ where: { final: true } }))[0]
    this.set(result)
    return this
  }

  /**
   * 
   * @param {string} teamCode 
   * @returns {Promise<Array<import('../datasources/mongodb/generated/prisma-client').Tournament>>}
   */
  async findByClubAlias(clubAlias) {
    const result = (await this.mongodb.tournaments({
      where: { clubAlias, finished_not: true, published: true }, orderBy: 'date_ASC'
    }))[0]
    this.set(result);
    return result
  }

  /**
   * @param {Team}
   * @param {{}}
   * 
   */
  async hasTeam(teamHandler, { final } = { final: false }) {
    this._checkRoot()
    const where = {
      id: this.get('id')
    }
    if (final) {
      where.final = final
    }
    const result = await this.mongodb.tournaments({ where: { ...where, teams_some: { id: teamHandler.get('id') } } })
    return result.length ? result[0] : false
  }
  /**
   * @param {Team}
   */
  async addTeam(teamEntity) {
    this._checkRoot()
    const teams = await this.mongodb.updateTournament({
      where: { id: this.root.id },
      data: {
        teams: {
          connect: {
            code: teamEntity.get('code')
          }
        }
      }
    }).teams()
    await teamEntity.assignTournament(this.get('id'))
    if (teams.length === (this.get('capacity') || MAX_TEAM_SIZE_ON_TOURNAMENT)) {
      await this.mongodb.updateTournament({
        where: {
          id: this.root.id
        },
        data: {
          status: 'FULL'
        }
      })
    }
    return this.get()
  }


  async removeTeam(team) {
    this._checkRoot()
    const exists = await this.mongodb.tournaments({ where: { id: this.get('id'), teams_some: { id: team.id } } })
    if (exists.length === 0) {
      throw new Error('TEAM_NOT_FOUND_IN_TOURNAMENT')
    }
    const result = await this.mongodb.updateTournament({
      where: { id: this.root.id },
      data: {
        teams: {
          disconnect: {
            code: team.code
          }
        }
      }
    })
    return result
  }
  /**
   * 
   * @param {import('../datasources/mongodb/generated/prisma-client').User} user 
   */
  async addReferee(email) {
    this._checkRoot()
    const referee = await this.mongodb.user({ email });
    if (!referee) {
      throw new Error('REFEREE_NOT_FOUND')
    }
    return this.mongodb.updateTournament({
      where: { id: this.get('id') },
      data: {
        referees: {
          connect: {
            id: referee.id
          }
        }
      }
    }).referees()

  }
  /**
   * 
   * @param {import('../datasources/mongodb/generated/prisma-client').User} user 
   */
  async removeReferee(email) {
    this._checkRoot()
    const referee = await this.mongodb.user({ email });
    if (!referee) {
      throw new Error('REFEREE_NOT_FOUND')
    }
    return this.mongodb.updateTournament({
      where: { id: this.get('id') },
      data: {
        referees: {
          disconnect: {
            id: referee.id
          }
        }
      }
    }).referees()

  }

  async save() {
    this._checkRoot()
    const data = this.get()
    const result = await this.mongodb.createTournament(data);
    return result
  }
  async update(data) {
    this._checkRoot()
    const { id } = this.get()
    const result = await this.mongodb.updateTournament({ where: { id }, data })
    this.set(result);
    await this.deleteCache()
    return result
  }
  async delete() {
    this._checkRoot()
    const { id } = this.get()
    await this.mongodb.deleteTournament({ id })
    return 'OK'
  }

  static async teamsGlobalClassification() {
    const { mongodb } = this.fakeCtx()
    const standings = [1, 2, 3, 4, 5, 6, 7, 8];
    const teams = await mongodb.teams({ where: { rankingPosition_in: standings, tournament: { finished: true } } })
    const result = [];
    for (const team of teams) {
      const classificationTeam = (await mongodb.classifications({ where: { team: { id: team.id } } }))[0]
      const combats = await new Team().set(team).combats()
      const teamScores = await this.calculateTeamScores(team, combats)
      result.push({ ...team, ...teamScores, order: classificationTeam.order })
    }
    return result;
  }

  static async updateGlobalRanking() {
    const { mongodb } = this.fakeCtx()
    const rankingTeams = {}
    const teamsGlobalClass = await this.teamsGlobalClassification()
    for (const team of teamsGlobalClass) {
      if (!rankingTeams[team.rankingPosition]) {
        rankingTeams[team.rankingPosition] = []
      }
      rankingTeams[team.rankingPosition].push(team)
    }
    Object.keys(rankingTeams).forEach(ranking => {
      rankingTeams[ranking].sort((a, b) => {
        return (
          b.won - a.won
          || b.pf - a.pf
          || b.technique - a.technique
          || b.energy - a.energy
          || (b.maxAvgEnergy || 0) - (a.maxAvgEnergy || 0)
        )
      })
    })
    let standingPosition = 1;
    const result = []
    for (const ranking in rankingTeams) {
      for (const team of rankingTeams[ranking]) {
        team.standingPosition = standingPosition
        standingPosition++
        result.push(team)
      }
      await Promise.all(rankingTeams[ranking].map((team) => {
        return mongodb.updateTeam({ where: { id: team.id }, data: { standingPosition: team.standingPosition } })
      }));
    }
  }

  static async globalRanking(standing, tournamentId) {
    const { mongodb } = this.fakeCtx()
    let standings = []
    if (standing) {
      standings = [standing]
    } else {
      standings = [1, 2, 3, 4, 5, 6, 7, 8];
    }
    // const teams = (await mongodb.tournament({ id: tournamentId }).teams()).filter(it => standings.includes(it.rankingPosition)).sort((a,b) => a.standingPosition - b.standingPosition || a.rankingPosition - b.rankingPosition)
    const teams = (await mongodb.teams({ orderBy: 'standingPosition_ASC', where: { rankingPosition_in: standings } })).sort((a, b) => a.standingPosition - b.standingPosition)
    return teams
  }

  /**
   * @param {string} id  Tournament id
   */
  static async restartTournament(id) {
    const { mongodb } = this.fakeCtx()
    try {
      await mongodb.updateManyCombats({
        where: { tournament: { id: id } }, data: {
          winner: null,
          game: null,
          lastCombo: 0,
          score1: null,
          score2: null,
          status: 'P',
          participants: null
        }
      })

      const groupPhase = (await (mongodb.tournament({ id })).phases()).find(it => it.name === 'GROUP_STAGE')
      const combats = await mongodb.combats({ where: { tournament: { id }, group: groupPhase.id } })

      combats.forEach(combat => {
        mongodb.updateCombat({
          where: {
            id: combat.id
          }, data: {
            winner: null,
            game: null,
            lastCombo: 0,
            score1: null,
            score2: null,
            status: 'P',
            team1: {
              disconnect: true
            }, team2: {
              disconnect: true
            },
            participants: null
          }
        })
      })
      console.log(`Combats reset sucessfully, tournament:${id}`)
    } catch (resetCombatsException) {
      console.error(`Trying to reset combats for tournament ${id}`, resetCombatsException.message)
    }
    await mongodb.updateManyClassifications({
      where: {
        tournament: { id },
        //     group: group.id,
      }, data: {
        dif: 0, energy: 0, lose: 0, won: 0, order: 0, pa: 0, pf: 0, technique: 0, total: 0, maxAvgEnergy: 0
      }
    })
    const tournament = new Tournament()
    await tournament.findById(id)
    const groups = await tournament.groups()
    const otherPhases = (await tournament.phases()).filter(phase => phase.name !== 'GROUP_STAGE').map(p => p.id)
    const combatsInOtherPhasesT1 = await mongodb.combats({
      where: {
        phase_in: otherPhases,
        team1: {
          id_not: null,
        }
      }
    })
    const combatsInOtherPhasesT2 = await mongodb.combats({
      where: {
        phase_in: otherPhases,
        team2: {
          id_not: null,
        }
      }
    })
    combatsInOtherPhasesT1.forEach(async (combat) => {
      await mongodb.updateCombat({
        where: { id: combat.id }, data: {
          team1: {
            disconnect: true
          }

        }
      })
    });
    combatsInOtherPhasesT2.forEach(async (combat) => {
      await mongodb.updateCombat({
        where: { id: combat.id }, data: {
          team2: {
            disconnect: true
          }
        }
      })
    });
    Promise.all(groups.map(async (group) => {
      console.log('resetting group', group)
      const combats = await mongodb.combats({ where: { group: group.id } })
      await Promise.all(combats.map((combat) => {
        return mongodb.updateCombat({
          where: {
            id: combat.id
          },
          data: {
            reviews: null,
            scores: { deleteMany: { number_not: null } },

          }
        })
      }))
    }))
    // tournament.generateClassificationData()
    return true
  }
  /**
   * @typedef {import('../datasources/mongodb/generated/prisma-client').TournamentPhase} TournamentPhase
   * @param {Array<TournamentPhase>} phasesList 
   * @returns {Array<TournamentPhase>} phasesList
   */
  async createPhases(COMBATS_CONFIG) {
    const phases = [];
    COMBATS_CONFIG.forEach((it, idx) => {
      const groups = [];
      it.combats.forEach(combat => {
        if (combat.group && groups.indexOf(combat.group) === -1) {
          groups.push(combat.group)
        }
      })
      const phase = {
        name: it.phase,
        order: idx,
        type: idx === 0 ? 'L' : 'P',
        arena: it.arenaQty
      }
      if (groups.length) {
        phase.groups = {
          create: groups.map(el => ({
            name: `Group ${el}`
          }))
        }
      }
      phases.push(phase)
    })
    await this.update({
      phases: {
        create: phases
      }
    })
    return this.phases();
  }

  async checkReferees() {
    this._checkRoot()
    const referees = await this.referees();
    if (referees.length === 0) throw new Error(`No refereees assigned to this tournament: ${this.get('id')}`)
  }

  async buildCustomCalendar() {
    this._checkRoot()
    const { COMBATS_CONFIG, GROUP_SIZE } = require('../datasources/combats_config');
    function chunkArrayInGroups(ar, num) {
      return ar.reduce(function (r, v, i) {
        if (i % num == 0) r.push(ar.slice(i, i + num));
        return r;
      }, []);
    }
    await this.checkReferees()
    await this.removeCalendar()

    const phasesList = await this.createPhases(COMBATS_CONFIG)
    const referees = await this.referees()

    // objeto con phases, horas y pistas de combates
    const mapCombats = COMBATS_CONFIG.reduce((acc, it) => {
      if (!acc[it.phase]) {
        acc[it.phase] = {}
      }
      it.combats.forEach((combat) => {
        if (!acc[it.phase][combat.time]) {
          acc[it.phase][combat.time] = {}
        }
        if (!acc[it.phase][combat.time][combat.arena]) {
          acc[it.phase][combat.time][combat.arena] = { ...combat }
        }
      })
      return acc;
    }, {});

    const randomTeams = chunkArrayInGroups(await this.sortTeamsRandom(), GROUP_SIZE);
    const groups = await this.groups()
    const GroupsTeams = {};
    for (let grpNumber = 1; grpNumber <= groups.length; grpNumber++) {
      GroupsTeams[grpNumber] = { group: groups[grpNumber - 1], teams: randomTeams[grpNumber - 1] }
    }
    for (const phase of phasesList) {
      const i = phase.order
      const phaseConfig = COMBATS_CONFIG[i]

      for (const combatInfo of phaseConfig.combats) {

        const data = {
          name: combatInfo.name,
          arena: combatInfo.arena,
          tournament: { connect: { id: this.get('id') } },
          time: combatInfo.time,
          phase: phase.id,
          referees: { connect: [{ id: referees[0].id }] },
          type: combatInfo.type
        }
        if (phase.name === 'GROUP_STAGE') {
          data.group = groups[combatInfo.group - 1].id
          data.team1 = { connect: { id: GroupsTeams[combatInfo.group].teams[combatInfo.A - 1].id } }
          data.team2 = { connect: { id: GroupsTeams[combatInfo.group].teams[combatInfo.B - 1].id } }
          // GroupsTeams[combatInfo.group].phase = phase.id
        } else if (combatInfo.A.combat) {
          // Builder basado en combates de la fase anterior/actual
          const combatA = mapCombats[combatInfo.A.combat.phase][combatInfo.A.combat.time][combatInfo.A.combat.arena]
          const combatB = mapCombats[combatInfo.A.combat.phase][combatInfo.B.combat.time][combatInfo.B.combat.arena]
          data.team1Combat = { connect: { id: combatA.id } }
          data.team2Combat = { connect: { id: combatB.id } }
          data.team1Pos = combatInfo.A.winner ? 'W' : 'L'
          data.team2Pos = combatInfo.B.winner ? 'W' : 'L'

        } else {
          // Builder basado en groups
          data.team1Group = groups[combatInfo.A.group - 1].id
          data.team2Group = groups[combatInfo.B.group - 1].id
          data.team1Pos = combatInfo.A.pos.toString()
          data.team2Pos = combatInfo.B.pos.toString()
        }
        const result = await this.mongodb.createCombat(data)
        combatInfo.id = result.id // Guardamos el id de los combates generado en la instrucciones del Builder 
        mapCombats[phase.name][combatInfo.time][combatInfo.arena] = {
          ...combatInfo,
        }
      }
    }
    this.generateClassificationData();
    return this.combats()
  }

  async generateClassificationData() {
    this._checkRoot();
    const groupMap = {}
    await this.mongodb.deleteManyClassifications({
      tournament: {
        id: this.get('id')
      }
    })
    const phases = await this.phases();
    const groupPhase = phases.find(it => it.name = 'Group Stage')
    const combats = await this.combats(undefined, groupPhase.id)
    for (const combat of combats) {
      const team1 = await this.mongodb.combat({ id: combat.id }).team1()
      if (!groupMap[combat.group]) {
        groupMap[combat.group] = []
      }
      if (groupMap[combat.group].indexOf(team1.id) === -1) {
        groupMap[combat.group].push(team1.id)
      }
    }
    for (const group in groupMap) {
      for (const teamId of groupMap[group]) {
        await this.mongodb.createClassification({
          tournament: { connect: { id: this.get('id') } },
          group: group,
          phase: groupPhase.id,
          team: { connect: { id: teamId } },
          dif: 0, energy: 0, lose: 0, won: 0, order: 0, pa: 0, pf: 0, technique: 0, total: 0
        })
      }
    }
    console.log('Classification generated succesfully for:', this.get('id'))
  }

  async sortTeamsRandom() {
    // Establecer ordenamiento de Equipos
    this._checkRoot()
    function shuffle(arr) {
      const array = [...arr] // Inmutable
      let currentIndex = array.length,
        randomIndex

      // While there remain elements to shuffle...
      while (currentIndex != 0) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
          array[randomIndex], array[currentIndex]];
      }

      return array
    }
    const teams = await this.mongodb.tournament({ id: this.get('id') }).teams()
    let randomTeams = teams.sort((a, b) => a.order - b.order)
    if (teams[0].order !== 1) {
      randomTeams = shuffle(teams)
      for (let i = 0, n = randomTeams.length; i < n; i++) {
        await this.mongodb.updateTeam({
          where: { id: randomTeams[i].id },
          data: { order: i + 1 }
        })
      }
    }
    return randomTeams
  }

  /**
   * 
   * @param {{name:string, type:string}} phase 
   */
  async buildCalendar() {
    this._checkRoot()
    await this.removeCalendar()
    // Creación de Fases y grupos
    const PHASE_NAMES = ['Group Stage', 'Final Four']
    const phases = []
    for (let i = 0; i < 2; i++) {
      const groups = i === 0 ? ['GROUP 1', 'GROUP 2'] : []
      const phase = {
        name: PHASE_NAMES[i],
        type: i === 0 ? 'L' : 'P',
        order: i,
        groups: {
          create: groups.map(el => {
            return {
              name: el
            }
          })
        }
      }
      phases.push(phase)
    }
    await this.update({
      phases: {
        create: phases
      }
    })


    const referees = await this.referees()
    // Crear combates Phase Liga, Qualifiers, Finals
    let startTime = new Date(this.get('date'))

    startTime = (startTime.getHours() * 100) + startTime.getMinutes()
    // startTime = 1500
    const combats = [{
      0: { A: 1, B: 4, group: 0 },
      10: { A: 5, B: 8, group: 1 },
      20: { A: 2, B: 3, group: 0 },
      30: { A: 6, B: 7, group: 1 },
      40: { A: 4, B: 3, group: 0 },
      50: { A: 8, B: 7, group: 1 },
      100: { A: 1, B: 2, group: 0 },
      110: { A: 5, B: 6, group: 1 },
      120: { A: 2, B: 4, group: 0 },
      130: { A: 6, B: 8, group: 1 },
      140: { A: 3, B: 1, group: 0 },
      150: { A: 7, B: 5, group: 1 }
    }, {
      200: { A: { g: 0, p: 1 }, B: { g: 1, p: 2 }, name: 'Semifinal 1' },
      210: { A: { g: 1, p: 1 }, B: { g: 0, p: 2 }, name: 'Semifinal 2' },
      220: { A: { c: 200, p: 'L' }, B: { c: 210, p: 'L' }, name: '3rd-4th Combat', type: 3 },
      230: { A: { c: 200, p: 'W' }, B: { c: 210, p: 'W' }, name: 'Final', type: 1 }
    }]
    const phasesList = await this.phases()
    const groups = await this.groups()
    const randomTeams = await this.sortTeamsRandom();
    for (const it of phasesList) {
      const i = it.order
      const metainfo = combats[i]

      for (const combatTime in metainfo) {
        const combatInfo = metainfo[combatTime]

        const data = {
          name: combatInfo.name,
          tournament: { connect: { id: this.get('id') } },
          time: startTime + parseInt(combatTime),
          phase: it.id,
          referees: { connect: { id: referees[0].id } },
          type: combatInfo.type
        }
        if (it.type === 'L') {

          data.group = groups[combatInfo.group].id
          data.team1 = { connect: { id: randomTeams[combatInfo.A - 1].id } }
          data.team2 = { connect: { id: randomTeams[combatInfo.B - 1].id } }
        } else if (combatInfo.A.c) {
          // Builder basado en combates de la fase anterior/actual
          data.team1Combat = { connect: { id: combats[i][combatInfo.A.c].id } }
          data.team2Combat = { connect: { id: combats[i][combatInfo.B.c].id } }
          data.team1Pos = combatInfo.A.p
          data.team2Pos = combatInfo.B.p
        } else {
          // Builder basado en groups


          data.team1Group = groups[combatInfo.A.g].id
          data.team2Group = groups[combatInfo.B.g].id
          data.team1Pos = combatInfo.A.p.toString()
          data.team2Pos = combatInfo.B.p.toString()
        }
        const result = await this.mongodb.createCombat(data)
        combatInfo.id = result.id // Guardamos el id de los combates generado en la instrucciones del Builder 
      }
    }
    this.generateClassificationData()
    return this.combats()
  }

  async removeCalendar() {
    this._checkRoot()
    await this.mongodb.deleteManyCombats({
      tournament: { id: this.get('id') }
    })
    await this.mongodb.deleteManyClassifications({
      tournament: { id: this.get('id') }
    })
    await this.mongodb.updateTournament({
      where: { id: this.get('id') },
      data: {
        phases: { deleteMany: [{ type_not: 'foo' }] },
        finished: false
      }
    })
  }

  /**
   * 
   * @param {Team} team 
   * @returns 
   */
  static async calculateTeamScores(team, combats) {
    const CombatEntity = require('./Combat')
    const combatEntity = new CombatEntity();

    const teamScores = { order: 0, won: 0, lose: 0, pf: 0, pa: 0, dif: 0, maxAvgEnergy: 0, energy: 0, technique: 0 }
    for (const combatObj of combats) {
      combatEntity.set(combatObj)
      const combat = combatEntity.get()
      const team1 = await combatEntity.team1()
      const isLocal = team.id === team1.id
      // 1. Número de Combates Ganados: El equipo que gane más combates quedará por encima en la clasificación.
      if (isLocal) {
        teamScores.won += combat.winner === 1
        teamScores.lose += combat.winner !== 1
      }
      else {
        teamScores.won += combat.winner === 2
        teamScores.lose += combat.winner !== 2
      }
      // 2. Puntuación Total: Mayor diferencia de puntos sumando las puntuaciones a favor y en contra de los tres combates disputados.

      if (isLocal) {
        teamScores.pf += combat.score1
        teamScores.pa += combat.score2
      }
      else {
        teamScores.pf += combat.score2
        teamScores.pa += combat.score1
      }

      teamScores.dif = teamScores.pf - teamScores.pa

      const scoresSummary = await combatEntity.scoresSummary()

      const score = scoresSummary[isLocal ? 'team1' : 'team2']
      // 3. Puntuación Técnica: Mayor puntuación acumulada en los 4 apartados de ejecución técnica en sus tres combates.
      teamScores.technique += (
        score.technique +
        score.coordination +
        score.hits +
        score.ranges);
      // 4. Puntuación de Energy: Mayor puntuación acumulada de Energy en sus tres combates.
      teamScores.energy += score.energy

      // 5. Mayor Media de Energy: Mayor media de Energy en uno de los tres combates.
      const energyScores = await combatEntity.scores()
      const avgEnergy = Math.round((
        energyScores.reduce((acc, el) => acc + (el.results && el.results[isLocal ? 'team1' : 'team2'].total) || 0, 0)
        / (energyScores.length || 1)
      ) * 100) / 100
      teamScores.maxAvgEnergy = Math.max(
        teamScores.maxAvgEnergy,
        avgEnergy
      )
    }
    return teamScores;
  }

  /**
   * 
   * Includes:
   */
  async club() {
    this._checkRoot()
    if (this.get('clubAlias')) {
      const district = new District();
      const { Name, ShippingAddress: address, Phone: phone, email } = await district.findByAlias(this.get('clubAlias'))
      return {
        Name, ShippingAddress: address, Phone: phone, email
      }
    }
  }

  async teams() {
    return this.mongodb.tournament({ id: this.get('id') }).teams()
  }

  async referees() {
    return this.mongodb.tournament({ id: this.get('id') }).referees()
  }

  async combats(status, phaseId, groupId) {
    const where = {
      tournament: { id: this.get('id') }
    }
    if (status) {
      if (Array.isArray(status)) {
        where.status_in = status
      } else {
        where.status = status
      }
    }
    if (phaseId) {
      where.phase = phaseId
    }
    if (groupId) {
      where.group = groupId
    }
    const combats = await this.mongodb.combats({ where, orderBy: 'time_ASC' })
    return combats

  }

  async phases() {
    this._checkRoot()
    const result = await this.mongodb.tournament({ id: this.get('id') }).phases()
    return result
  }

  async groups() {
    this._checkRoot()
    const groups = await (this.mongodb.tournament({ id: this.get('id') }).phases().groups())
    if (groups && groups.length) {
      return groups[0].groups
    }
    return []
  }
  // async groups() {
  //   this._checkRoot()
  //   const phases = await this.phases()
  //   let groups = []
  //   for (const phase of phases) {
  //     const groupsPhase = await this.mongodb.phaseGroups({ where:  phase.id })
  //     groups = groups.concat(groupsPhase)
  //   }
  //   return groups;
  // }

  async classification() {
    this._checkRoot()
    const where = {
      tournament: { id: this.get('id') }
    }
    return this.mongodb.classifications({ where, orderBy: 'order_ASC' })
  }


  /**
   * @param {import('../datasources/mongodb/generated/prisma-client/index').User} user 
   * @returns {Promise<boolean>}
   */
  async isTournamentLocked(user) {
    this._checkRoot()
    let isBlocked = 0
    const tournamentLockedFlag = `Tournament:${this.get('id')}:Users`;
    const ordersInProgressCache = JSON.parse(await getCache(tournamentLockedFlag) || "[]")
    const myOrder = ordersInProgressCache.some((it) => it.user === user.id)
    const teamsQty = (await this.teams()).length
    if (!myOrder) {
      if (Tournament.LOCK_LAST_SLOT) {
        isBlocked = (ordersInProgressCache.length + teamsQty) > this.get('capacity') - 1
      } else {
        isBlocked = ordersInProgressCache.length > 0
      }
    }
    return isBlocked;
  }
   /**
   * @param {import('../datasources/mongodb/generated/prisma-client/index').Team} team 
   * @returns {Promise<boolean>}
   */
  async isTeamLocked(team){
    this._checkRoot()
    const teamKey = `Team:${team.id}:Tournament`;
    const teamInTournament = (await getCache(teamKey)) || null
    if (teamInTournament === this.get('id')) {
      return false
    }
    return false
  }

  /**
   * 
   * @param {import('../datasources/mongodb/generated/prisma-client/index').User} User 
   * @param {import('../datasources/mongodb/generated/prisma-client/index').Team} Team 
   * @returns {Promise<boolean>}
   */
  async lockTournament(user, team){
    this._checkRoot()
    const tournamentLockedFlag = `Tournament:${this.get('id')}:Users`;

    const inProgressTeams = JSON.parse((await getCache(tournamentLockedFlag)) || '[]')
    setCache(tournamentLockedFlag, JSON.stringify([...inProgressTeams,{ teamCode: team.code, user: user.id }]), EXPIRATION_TIME_FLAG)
  }
  
  /**
   * 
   * @param {import('../datasources/mongodb/generated/prisma-client/index').Tournament} tournament 
   * @returns {Promise<boolean>}
   */
  async lockTeam(team){
    this._checkRoot()
    const teamKey = `Team:${team.id}:Tournament`;
    setCache(teamKey, this.get('id'), EXPIRATION_TIME_FLAG)
  }

  async unlock(team, user) {
    this._checkRoot()
    const tournamentLockedFlag = `Tournament:${this.get('id')}:Users`;

    const teamKey = `Team:${team.id}:Tournament`;
    delCache(teamKey)
  
    if (Tournament.LOCK_LAST_SLOT){
      const orderInProgress = JSON.parse(await getCache(tournamentLockedFlag) || '[]')
      orderInProgress.splice(orderInProgress.findIndex(it => it.user === user.id))
      setCache(tournamentLockedFlag, JSON.stringify(orderInProgress))
    } else {
      delCache(tournamentLockedFlag)
    }
  }
}

module.exports = Tournament