require('dotenv').config()
module.exports = {
  apps : [{
    script: 'services/fwg/server.js',
    namespace: 'fwg',
    name:'fwg',
    log_date_format : "YYYY-MM-DD HH:mm Z",
    env: {
      PORT: 3030,
      NODE_ENV: 'production',
      PLAYGROUND: false,
      PUBLIC_BASE_URL: 'https://fitboxingworldgames.com/fwg/public',
      FWG_API_URL: 'https://brooklynfitzone.com/fwg/',
      REGION: 'eu-west-1',
      BUCKET_NAME: "brooklyn-fwg",
      ACCESS_KEY_ID: process.env.ACCESS_KEY_ID,
      SECRET_ACCESS_KEY: process.env.SECRET_ACCESS_KEY,
      S3_URL: 'https://brooklyn-fwg.s3.eu-west-1.amazonaws.com/',
      REFEREE_DEFAULT_PASSWORD: "FWGXXXX."   
    }  
  }],

  deploy : {
    production : {
      user : 'SSH_USERNAME',
      host : 'SSH_HOSTMACHINE',
      ref  : 'origin/master',
      repo : 'GIT_REPOSITORY',
      path : 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
