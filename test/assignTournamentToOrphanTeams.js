const { prisma: mongodb } = require('./../datasources/mongodb/generated/prisma-client')
// const { PrismaClient } = require('@prisma/client')

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
async function main() {
    // const client = new PrismaClient();

    // await client.$connect()
    const tournaments = await mongodb.tournaments({
        where:{
            id: "61eaccf60274390008a5dd2e"
        }
    }).$fragment(`
     fragment tournamentTemp on Tournaments {
        id
        teams {
            id
            name
            code
        }
     }
    `)
    for (const tournament of tournaments) {
        console.log(tournament)
        if (tournament.teams && tournament.teams.length) {
            for (const team of tournament.teams) {
                await timeout(3000);
                await mongodb.updateTeam({
                    where: {
                        id: team.id
                    },
                    data: {
                        tournament: {
                            connect: {
                                id: tournament.id
                            }
                        },
                        registrationDate: new Date()
                    }
                })
            }
        }

    }
}
main()