const { prisma: mongodb } = require("../datasources/mongodb/generated/prisma-client");
const { faker } = require('@faker-js/faker');

const crypto = require('crypto');
const fitboxers = require('./mock-fitboxers')
const Tournament = require('../Entities/Tournament')
const trainers = require('./mock-fitboxers')
const teamsMock = require('./mock-teams')
// jose@bftest.com
// juan@bftest.com
// pedro@bftest.com
// raul@bftest.com
// alberto@bftest.com
// fernando@bftest.com
const hashedPassword = crypto.createHash('sha256').update('12345678').digest('hex');
console.log(hashedPassword)
/**
 * 
 * @param {string} tournamentId 
 */

async function generateTeams(tournamentId) {
    const tournamentHandler = new Tournament()
    const tournament = await tournamentHandler.findById(tournamentId);
    if (!tournament) {
        throw new Error('Tournament does not exist')
    }
    for (const team of teamsMock) {
        if (!(await mongodb.$exists.team({ code: team.code }))) {
            const fitboxers = [];
            // const trainer = {
            //     type: "TRAINER",
            //     firstName: faker.name.firstName(),
            //     lastName: faker.name.lastName(),
            // }
            // trainer.email = faker.helpers.unique(faker.internet.email, [
            //     user.firstName,
            //     user.lastName,
            // ]);
            for (let i = 1; i <= 5; i++) {
                const user = {
                    type: "FITBOXER",
                    isCaptain: i === 0 ? true : false,
                    firstName: faker.name.firstName(),
                    lastName: faker.name.lastName(),
                    password: hashedPassword,
                    env: 'DUMMY'

                }
                user.email = faker.helpers.unique(faker.internet.email, [
                    user.firstName,
                    user.lastName,
                ]);
                fitboxers.push(user)
            }
            try {


                await mongodb.createTeam({
                    ...team,
                    tournament: {
                        connect: {
                            id: "6437da2f0274390009c2e995"
                        }
                    },
                    createdBy: {
                        connect: {
                            email: "gleyder4@gmail.com"
                        }
                    },
                    fitboxers: {
                        create: fitboxers
                    }
                })

            } catch (err) {
                console.error('Error creating team', team, err)
                throw err
            } 
        }
    }
    console.log('DONE')
    process.exit(0)
    return null;

}
async function addTeamsOnTournament(){
    const teams = await mongodb.teams({where:{
        code_starts_with:"FWG"
    }})
    for(const team of teams){

        await mongodb.updateTournament({ where: {id: "6437da2f0274390009c2e995"}, data: {
            teams:{
                connect: {
                    id: team.id
                }
            }
        }} )
    }
}

async function createTrainers() {

    for (const trainer of trainers) {

        await mongodb.createUser({
            type: 'TRAINER',
            verified: true,
            password: hashedPassword,
            firstName: trainer,
            lastName: '--',
            email: `${trainer}@bftest.com`,
        })

    }
}
async function disconnectAllFitboxers() {
    const emails = fitboxers.map(it => it.email)

    const fitboxersMDB = await mongodb.users({ where: { type: 'FITBOXER', email_in: emails } })
    console.log(fitboxersMDB.length)
    // for(const fb of fitboxers){
    //     if(!(await mongodb.$exists.user({email:fb.email}))){
    //        await mongodb.createUser({ email: fb.email,
    //                             firstName: fb.firstName,
    //                             lastName: fb.lastName,
    //                             type: 'FITBOXER',
    //                             password: hashedPassword,
    //                             isCaptain: false,
    //                             teamId: null
    //                         })
    //     }
    // }
    const teams = await mongodb.teams({ where: { createdBy: { email_ends_with: '@bftest.com' } } })
    for (const team of teams) {
        const fbs = await mongodb.team({ id: team.id }).fitboxers();
        for (const fb of fbs) {

            await mongodb.updateTeam({ where: { id: team.id }, data: { fitboxers: { disconnect: { id: fb.id } } } })
            await mongodb.updateUser({
                where: { id: fb.id }, data: {
                    password: hashedPassword,
                    teamId: null
                }
            })
        }
    }
}
async function main() {
    const emails = fitboxers.map(it => it.email)
    const fitboxersMDB = await mongodb.users({ where: { type: 'FITBOXER', email_in: emails } })
    const teams = await mongodb.teams({ where: { createdBy: { email_ends_with: '@bftest.com' } } })
    const fbRegistered = [];
    for (const team of teams) {
        let fbTeam = 0;

        for (const fb of fitboxersMDB) {
            if (fbRegistered.includes(fb.id)) {
                continue
            }
            if (fbTeam === 5) {
                break;
            }
            await mongodb.updateUser({
                where: { id: fb.id }, data: {
                    password: hashedPassword,
                    teamId: team.id
                }
            })
            await mongodb.updateTeam({
                where: { id: team.id }, data: {
                    fitboxers: {
                        connect: { id: fb.id }
                    }
                }
            })
            fbRegistered.push(fb.id)
            fbTeam++;
        }
    }

}
//         return 
//     // const trainers = await mongodb.users({where:{ email_ends_with:'@bftest.com'}});
//     // const fitboxers = await mongodb.users({ where: { email_ends_with: '@bftest.com' , type:''}})

//     console.log(fitboxers.length)
//     console.log(teams.length)
//     const fitboxersRegistered = []
//     for (const team of teams) {
//         let teamFbCount = 0
//         for (const fb of fitboxers) {
//             if (teamFbCount === 5) {
//                 console.log('Team ' + team.name + ' full')
//                 break;
//             }

//             const fitboxer = await mongodb.upsertUser({
//                 where: {
//                     email: fb.email,
//                 }, 
//                 create: {
//                     email: fb.email,
//                     firstName: fb.firstName,
//                     lastName: fb.lastName,
//                     type: 'FITBOXER',
//                     password: hashedPassword,
//                     isCaptain: teamFbCount === 0,
//                     teamId: team.id,
//                 },
//                 update:{
//                     isCaptain: teamFbCount === 0,
//                     teamId: team.id,
//                 }
//             })
//             await mongodb.updateTeam({ where: { id: team.id }, data: { createdBy: { connect: { email: 'trainer@bftest.com'}}, fitboxers:{connect:{id:fitboxer.  id}} } })
//             teamFbCount++;
//         }
//     }
// }
// createTrainers()
// main();

// disconnectAllFitboxers();

// await mongodb.createTeam({
//     name: 'Berdonce\'s',
//     motto:'real boxers fighters',
//     createdBy:{
//         connect:{
//             id: '639b2e5c0274390009b918fb'
//         }
//     },
//     code: Math.random().toString(36).slice(4).substring(0,9)
// })



// generateTeams("6437da2f0274390009c2e995")
// addTeamsOnTournament()