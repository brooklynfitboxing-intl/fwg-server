const { prisma } = require("../../datasources/mongodb/generated/prisma-client")
const { prisma: mongobfi } = require("../../../../brooklyn-server/mongodb/generated/prisma-client")

async function main(id) {
    const list = []
    const teamsOnFinal = (await (prisma.tournament({ id }).teams()))
    // const fitboxers = await prisma.users({ where: { type: 'FITBOXER', teamId_in: teamsOnFinal } })
    // console.log('total fitbxers:', fitboxers.length)
    for (const team of teamsOnFinal) {
        const fbxs = await prisma.team({ id: team.id }).fitboxers()
        for (const fb of fbxs) {
            if (fb.env === 'BFI') {
                list.push(fb.id)
            }
        }
    }
    const fbsBFi = await mongobfi.fitboxers({
        where: {
            id_in: list,
            gender: 'F'
        }
    })
    const fbsBfiMale =  fbsBFi.filter(item => item.gender ==='M')
    const fbsBfiFemale =  fbsBFi.filter(item => item.gender ==='F')
    
    console.log('total fitboxers on final:', list.length)
    console.log('total males on fainal:', fbsBfiMale.length)a
    console.log('total female fitboxers on final', fbsBfiFemale.length)

}
main("643d021024aa9a0007876ab4")