const sort = [
    {
        "group": 12,
        "code": "77kp1o14p"
    },
    {
        "group": 10,
        "code": "iostzhpzl"
    },
    {
        "group": 13,
        "code": "3l4iczp2c"
    },
    {
        "group": 3,
        "code": "mkln0k07q"
    },
    {
        "group": 8,
        "code": "kb89gsr1s"
    },
    {
        "group": 6,
        "code": "otw1focy"
    },
    {
        "group": 2,
        "code": "d1j3xwou"
    },
    {
        "group": 1,
        "code": "0dfkl5pnj"
    },
    {
        "group": 9,
        "code": "ljcwj2m3"
    },
    {
        "group": 7,
        "code": "7vfkna0gl"
    },
    {
        "group": 11,
        "code": "n5sk57biv"
    },
    {
        "group": 9,
        "code": "vhpw0cg7"
    },
    {
        "group": 2,
        "code": "k8rkun9ys"
    },
    {
        "group": 12,
        "code": "kec1lwrs"
    },
    {
        "group": 2,
        "code": "5nbn0vf7"
    },
    {
        "group": 12,
        "code": "zmafj16d"
    },
    {
        "group": 15,
        "code": "xdo9cgmen"
    },
    {
        "group": 10,
        "code": "e517faxln"
    },
    {
        "group": 16,
        "code": "67dcv0vee"
    },
    {
        "group": 5,
        "code": "n5gitagc"
    },
    {
        "group": 10,
        "code": "m0tki7km3"
    },
    {
        "group": 8,
        "code": "pyg5xqg5m"
    },
    {
        "group": 1,
        "code": "6bvc1lq7w"
    },
    {
        "group": 3,
        "code": "h9p9r3is"
    },
    {
        "group": 4,
        "code": "n23947voyn"
    },
    {
        "group": 8,
        "code": "efc0jma"
    },
    {
        "group": 4,
        "code": "xxzbhmg73"
    },
    {
        "group": 11,
        "code": "rftjfvhwk"
    },
    {
        "group": 15,
        "code": "z0l0biryn"
    },
    {
        "group": 6,
        "code": "41ubophwd"
    },
    {
        "group": 5,
        "code": "ja7zss9p"
    },
    {
        "group": 5,
        "code": "df2w135p5"
    },
    {
        "group": 13,
        "code": "h44uy47ht"
    },
    {
        "group": 3,
        "code": "gu3qeybed"
    },
    {
        "group": 11,
        "code": "k0ochmlhf"
    },
    {
        "group": 3,
        "code": "rfo5m3p4u"
    },
    {
        "group": 15,
        "code": "yrbzpkats"
    },
    {
        "group": 7,
        "code": "z2m67wyk"
    },
    {
        "group": 1,
        "code": "4236lnnra"
    },
    {
        "group": 8,
        "code": "ex38xvsl"
    },
    {
        "group": 6,
        "code": "burr5a4jg"
    },
    {
        "group": 14,
        "code": "o1gdc9d9"
    },
    {
        "group": 9,
        "code": "kce8f252f"
    },
    {
        "group": 4,
        "code": "l0y3hhw9"
    },
    {
        "group": 15,
        "code": "xi6trf8p6"
    },
    {
        "group": 2,
        "code": "nfnlqsan"
    },
    {
        "group": 14,
        "code": "75c26xisx"
    },
    {
        "group": 5,
        "code": "iqsr4ehur"
    },
    {
        "group": 10,
        "code": "1eikk58pl"
    },
    {
        "group": 6,
        "code": "wzqbfmzel"
    },
    {
        "group": 4,
        "code": "18cum1hsl"
    },
    {
        "group": 16,
        "code": "xh19ryde"
    },
    {
        "group": 16,
        "code": "0m4ib984f"
    },
    {
        "group": 9,
        "code": "w76lji3x"
    },
    {
        "group": 1,
        "code": "sn3ia2dtd"
    },
    {
        "group": 13,
        "code": "6nqwffb5i"
    },
    {
        "group": 7,
        "code": "w95i80iqd"
    },
    {
        "group": 14,
        "code": "l1ft50qjd"
    },
    {
        "group": 13,
        "code": "tncbmt3c"
    },
    {
        "group": 14,
        "code": "9u6hjkx0m"
    },
    {
        "group": 16,
        "code": "qkajlbqte"
    },
    {
        "group": 11,
        "code": "4o3k2kgf7"
    },
    {
        "group": 7,
        "code": "6ohal19jh"
    },
    {
        "group": 12,
        "code": "whmfmi49"
    }
];
async function main() {
    const { prisma } = require('../../datasources/mongodb/generated/prisma-client');
    for (const team of sort) {
        await prisma.updateTeam({
            where: {
                code: team.code,
            },
            data: {
                order: team.group
            }
        })
    }
    process.exit();
}

main();