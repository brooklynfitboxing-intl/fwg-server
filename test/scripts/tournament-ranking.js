const Team = require("../../Entities/Team")
const Tournament = require("../../Entities/Tournament")
const { prisma } = require("../../datasources/mongodb/generated/prisma-client")
const fs = require('fs')
const COMBATS_CACHE = {}
const TEAMS_CACHE = {}

// async function updateStanding(teamList) {
//     const rankingTeams = {}
//     for (const team of teamList) {
//         if (!rankingTeams[team.rankingPosition]) {
//             rankingTeams[team.rankingPosition] = []
//         }
//         rankingTeams[team.rankingPosition].push(team)
//     }
//     // Object.keys(rankingTeams).forEach(ranking => {
//     //   rankingTeams[ranking].sort((a, b) => {
//     //     return (
//     //       b.won - a.won
//     //       || b.pf - a.pf
//     //       || b.technique - a.technique
//     //       || b.energy - a.energy
//     //       || (b.maxAvgEnergy || 0) - (a.maxAvgEnergy || 0)
//     //     )
//     //   })
//     // })
//     let standingPosition = 1;
//     const result = []
//     for (const ranking in rankingTeams) {
//         for (const team of rankingTeams[ranking]) {
//             team.standingPosition = standingPosition
//             standingPosition++
//             result.push(team)
//         }
//         await Promise.all(rankingTeams[ranking].map((team) => {
//             return prisma.updateTeam({ where: { id: team.id }, data: { standingPosition: team.standingPosition } })
//         }));
//     }
// }

function convertTeamToLists(teamMap) {
    const list = []
    for (const teamId in teamMap) {
        if (TEAMS_CACHE[teamId]) {
            list.push({ id: teamId, name: TEAMS_CACHE[teamId].name, ...teamMap[teamId] })
        }
    }
    return list;
}
async function main(id) {
    const tournamentHandler = new Tournament()
    await tournamentHandler.findById(id)
    const teamValues = {}
    const values = {
        numberOfCombats: 0,
        combatsWon: 0,
        totalPf: 0,
        totalPoints: 0,
        totalEnergyPoints: 0,
        totalEnergyValue: 0
    }
    const techniqueValues = {
        coordination: 0,
        hits: 0,
        ranges: 0,
        technique: 0
    }
    await prisma.updateManyTeams({
        where: {
            tournament: {
                id
            },
            // id_not_in: [
            //     "639afaac24aa9a0007f63995",
            //     "643d021024aa9a0007876ab4"
            // ]
        },
        data: {
            rankingPosition: null,
            standingPosition: null,

        }
    })
    await updateFirstAndSecond()
    const list = (await tournamentHandler.teams()).filter(it => it.rankingPosition === null);
    // const teamsChunks = []
    // const chunkSize = 10;
    // for (let i = 0; i < teamsList.length; i += chunkSize) {
    //     const chunk = teamsList.slice(i, i + chunkSize);
    //     teamsChunks.push(chunk)
    // }
    for (const team of list) {
        const teamHandler = new Team();
        if (TEAMS_CACHE[team.id]) {
            teamHandler.set(TEAMS_CACHE[team.id])
        } else {
            await teamHandler.findById(team.id)
            TEAMS_CACHE[team.id] = teamHandler.get()
        }
        const combats = await teamHandler.combats({ status: 'F', tournament: { id } })
        console.log(`Processing team ${team.name}, total combats:`, combats.length)
        for (const c of combats) {
            /** 
             * @typedef {import("../../datasources/mongodb/generated/prisma-client").ComboScore} CScores
             * @typedef {import("../../datasources/mongodb/generated/prisma-client").Team} T
             * @type {import("../../datasources/mongodb/generated/prisma-client").Combat & { scores: Array<CScores>, team1:  T, team2: T}}
             * 
            */
            let combat = null
            if (COMBATS_CACHE[c.id]) {
                combat = COMBATS_CACHE[c.id]
            } else {
                const [combatResult, team1, team2, scores] = await Promise.all([
                    prisma.combat({ id: c.id }),
                    prisma.combat({ id: c.id }).team1(),
                    prisma.combat({ id: c.id }).team2(),
                    prisma.combat({ id: c.id }).scores()
                ])
                combat = { ...combatResult, team1, team2, scores }
                COMBATS_CACHE[c.id] = combat
            }
            if (!teamValues[team.id]) {
                teamValues[team.id] = { ...values }
            }
            const teamSide = combat.team1.id === team.id ? 1 : 2;
            teamValues[team.id].numberOfCombats++
            teamValues[team.id].combatsWon += combat.winner == teamSide ? 1 : 0
            teamValues[team.id].totalPf += combat[`score${teamSide}`]
            if (combat.scores.length) {

                for (const score of combat.scores) {
                    for (const key in score) {
                        if (key in techniqueValues) {
                            if (key === 'technique') {
                                teamValues[team.id].totalPoints += score['technique'] === teamSide ? 2 : 1
                            } else {
                                teamValues[team.id].totalPoints += score[key] === teamSide ? 1 : 0

                            }
                        } else if (key === 'energy') {
                            teamValues[team.id].totalEnergyPoints += score['energy'] === teamSide ? 2 : 1
                        }
                    }

                    // sum avg energy
                    if (score.results) {
                        teamValues[team.id].totalEnergyValue += score.results[`team${teamSide}`].total
                    }
                }
            }
        }
    }


    let teamList = convertTeamToLists(teamValues)
    teamList = sortTournamentRanking(teamList)
    console.log('sorted')
    fs.writeFileSync('./ranking1.json', JSON.stringify(teamList, null, 4))
    for (const t of teamList) {
        await prisma.updateTeam({
            where: {
                id: t.id
            },
            data: {
                rankingPosition: t.ranking,
               standingPosition: t.standing

            }
        })
    }
    console.log('done')
}
async function updateFirstAndSecond() {
    await prisma.updateTeam({
        where: {
            id: "639afaac24aa9a0007f63995"
        },
        data: {
            standingPosition: 1,
            rankingPosition: 2,
        }
    })
    await prisma.updateTeam({
        where: {
            id: "638872fc24aa9a0007d8f60c"
        },
        data: {
            standingPosition: 1,
            rankingPosition: 1,
        }
    })
}
// updateFirstAndSecond()
main("643d021024aa9a0007876ab4")
/**
 
    Ranking Final FWG 2023: 

    -    Puestos 1 y 2: ganador y perdedor del combate Final. 
    -    Puestos 3 a 64: 
    -    Número de combates jugados en el torneo 
    -    Número de combates ganados en el torneo 
    -    Mayor número acumulado de puntos a favor en el Torneo. 
    -    Mayor número acumulado de puntos obtenidos en todos los apartados de técnica  (4). 
    -    Mayor número acumulado de puntos obtenidos en el apartado de Energy. 
    -    Mayor media de Energy de todos los combates disputados.
            numberOfCombats
            combatsWon
            totalPf
            totalPoints
            totalEnergyPoints
            totalEnergyValue
 */
/**
 *
 * 
 */
function sortTournamentRanking(teamList) {
    teamList.sort((a, b) => {
        return (
            b.numberOfCombats - a.numberOfCombats ||
            b.combatsWon - a.combatsWon ||
            b.totalPf - a.totalPf ||
            b.totalPoints - a.totalPoints ||
            b.totalEnergyPoints - a.totalEnergyPoints ||
            b.totalEnergyValue - a.totalEnergyValue
        )
    })
    let ranking = 3
    let standing = 1
    let i = 3;
    for (const team of teamList) {
        team.ranking = ranking
        team.standing = standing;
        if(i % 8 ===0 ){
            standing++
        }
        ranking++
        i++
    }
    return teamList
}