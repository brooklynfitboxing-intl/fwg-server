const { prisma: mongodb } = require('../../datasources/mongodb/generated/prisma-client')
async function main() {
    const tournamentsQfs = await mongodb.tournaments({ where: { finished: true, final_not: true } })
    const trainerWithTeams = {}
    for (const tournament of tournamentsQfs) {
        const teams = await mongodb.tournament({ id: tournament.id }).teams();
        for (const team of teams) {
            const trainer = await mongodb.team({ id: team.id }).createdBy()
            if (!trainerWithTeams[trainer.id]) {
                trainerWithTeams[trainer.id] = { trainer, teams: [], clubAlias: tournament.clubAlias };
            }
            trainerWithTeams[trainer.id].teams.push(team)
        }
    }
    const list = []
    for (const trainer in trainerWithTeams) {
        list.push({


            email: trainerWithTeams[trainer].trainer.email,
            phone: trainerWithTeams[trainer].trainer.phone,
            name: `${trainerWithTeams[trainer].trainer.firstName} ${trainerWithTeams[trainer].trainer.lastName}`,
            teams: trainerWithTeams[trainer].teams.length,
            city: trainerWithTeams[trainer].teams[0].city,
            club: trainerWithTeams[trainer].clubAlias,
        })
    }
    list.sort((a, b) => b.teams - a.teams);
    console.table(list.slice(0, 3))
    // console.log(list[0])

    // console.log(list[1])

    // console.log(list[2])

    // console.log(list[3])

}

main()