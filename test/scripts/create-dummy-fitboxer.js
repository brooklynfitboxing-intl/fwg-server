const { faker } = require('@faker-js/faker');
const { prisma: mongodb } = require('../../datasources/mongodb/generated/prisma-client')
const fitboxerqty = 15;
const crypto = require('crypto');

const hashedPassword = crypto.createHash('sha256').update('colbrand8.').digest('hex');


async function main() {
    // const fitboxers = [];
    for (let i = 1; i <= fitboxerqty; i++) {
        const user = {
            type: "FITBOXER",
            isCaptain: false,
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            password: hashedPassword,
            env: 'DUMMY',
            email: `fiboxer_${i}@fwgpruebas.com`

        }
        // user.email = faker.helpers.unique(faker.internet.email, [
        //     user.firstName,
        //     user.lastName,
        // ]);
        await mongodb.createUser(user)
    }
    process.exit()

}

main()

