const Combat = require("../../Entities/Combat");
const { prisma } = require("../../datasources/mongodb/generated/prisma-client");
const scores = [{
    "number": 1,
    "votes": {
        "63d151d324aa9a000752cf5f": {
            "technique": 1,
            "coordination": 2,
            "hits": 1,
            "ranges": 2
        }
    },
    "coordination": 2,
    "energy": 1,
    "results": {
        "team1": {
            "detail": [
                {
                    "id": "638910b824aa9a0007dad726",
                    "name": "Teresa",
                    "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/638910b824aa9a0007dad726.png",
                    "bag": 12,
                    "power": 100,
                    "energy": 99.31,
                    "status": "OK"
                },
                {
                    "id": "638910cc24aa9a0007dad72b",
                    "name": "Gloria",
                    "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/638910cc24aa9a0007dad72b.png",
                    "bag": 14,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                null,
                {
                    "id": "63891b1324aa9a0007daf0cb",
                    "name": "Maria Jose",
                    "logo": null,
                    "bag": 13,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                {
                    "id": "638923a524aa9a0007daf91f",
                    "name": "Sara",
                    "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/638923a524aa9a0007daf91f.png",
                    "bag": 15,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                }
            ],
            "total": 99.83
        },
        "team2": {
            "detail": [
                {
                    "id": "638b87da24aa9a0007de1a51",
                    "name": "Jazmin",
                    "logo": null,
                    "bag": 3,
                    "power": 100,
                    "energy": 98.61,
                    "status": "OK"
                },
                {
                    "id": "6390b78024aa9a0007e55242",
                    "name": "Agata",
                    "logo": null,
                    "bag": 4,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                {
                    "id": "6390bd1924aa9a0007e5623e",
                    "name": "Ana",
                    "logo": null,
                    "bag": 5,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                {
                    "id": "6391228424aa9a0007e6f37d",
                    "name": "Laura",
                    "logo": null,
                    "bag": 2,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                null
            ],
            "total": 99.65
        }
    },
    "hits": 1,
    "technique": 1,
    "ranges": 2
},
{
    "number": 2,
    "votes": {
        "63d151d324aa9a000752cf5f": {
            "technique": 2,
            "coordination": 2,
            "hits": 2,
            "ranges": 1
        }
    },
    "coordination": 2,
    "energy": 1,
    "results": {
        "team1": {
            "detail": [
                {
                    "id": "638910b824aa9a0007dad726",
                    "name": "Teresa",
                    "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/638910b824aa9a0007dad726.png",
                    "bag": 12,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                {
                    "id": "638910cc24aa9a0007dad72b",
                    "name": "Gloria",
                    "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/638910cc24aa9a0007dad72b.png",
                    "bag": 14,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                null,
                {
                    "id": "63891b1324aa9a0007daf0cb",
                    "name": "Maria Jose",
                    "logo": null,
                    "bag": 13,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                {
                    "id": "638923a524aa9a0007daf91f",
                    "name": "Sara",
                    "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/638923a524aa9a0007daf91f.png",
                    "bag": 15,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                }
            ],
            "total": 100
        },
        "team2": {
            "detail": [
                {
                    "id": "638b87da24aa9a0007de1a51",
                    "name": "Jazmin",
                    "logo": null,
                    "bag": 3,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                {
                    "id": "6390b78024aa9a0007e55242",
                    "name": "Agata",
                    "logo": null,
                    "bag": 4,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                {
                    "id": "6390bd1924aa9a0007e5623e",
                    "name": "Ana",
                    "logo": null,
                    "bag": 5,
                    "power": 100,
                    "energy": 97.16,
                    "status": "OK"
                },
                {
                    "id": "6391228424aa9a0007e6f37d",
                    "name": "Laura",
                    "logo": null,
                    "bag": 2,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                null
            ],
            "total": 99.29
        }
    },
    "hits": 2,
    "technique": 2,
    "ranges": 1
},
{
    "number": 3,
    "votes": {
        "63d151d324aa9a000752cf5f": {
            "technique": 2,
            "coordination": 2,
            "hits": 1,
            "ranges": 1
        }
    },
    "coordination": 2,
    "energy": 1,
    "results": {
        "team1": {
            "detail": [
                {
                    "id": "638910b824aa9a0007dad726",
                    "name": "Teresa",
                    "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/638910b824aa9a0007dad726.png",
                    "bag": 12,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                {
                    "id": "638910cc24aa9a0007dad72b",
                    "name": "Gloria",
                    "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/638910cc24aa9a0007dad72b.png",
                    "bag": 14,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                null,
                {
                    "id": "63891b1324aa9a0007daf0cb",
                    "name": "Maria Jose",
                    "logo": null,
                    "bag": 13,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                {
                    "id": "638923a524aa9a0007daf91f",
                    "name": "Sara",
                    "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/fitboxers/638923a524aa9a0007daf91f.png",
                    "bag": 15,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                }
            ],
            "total": 100
        },
        "team2": {
            "detail": [
                {
                    "id": "638b87da24aa9a0007de1a51",
                    "name": "Jazmin",
                    "logo": null,
                    "bag": 3,
                    "power": 100,
                    "energy": 99.38,
                    "status": "OK"
                },
                {
                    "id": "6390b78024aa9a0007e55242",
                    "name": "Agata",
                    "logo": null,
                    "bag": 4,
                    "power": 100,
                    "energy": 100,
                    "status": "OK"
                },
                {
                    "id": "6390bd1924aa9a0007e5623e",
                    "name": "Ana",
                    "logo": null,
                    "bag": 5,
                    "power": 100,
                    "energy": 98.75,
                    "status": "OK"
                },
                {
                    "id": "6391228424aa9a0007e6f37d",
                    "name": "Laura",
                    "logo": null,
                    "bag": 2,
                    "power": 100,
                    "energy": 98.75,
                    "status": "OK"
                },
                null
            ],
            "total": 99.22
        }
    },
    "hits": 1,
    "technique": 2,
    "ranges": 1
}
]

async function main(combatId) {
    scores.forEach(score => {
        // score.votes = JSON.stringify(score.votes)
        // score.results = JSON.stringify(score.results)
    })
    await prisma.updateCombat({
        where: {
            id: combatId
        },
        data: {
            scores: {
                deleteMany: {
                    number_not: null
                }
            }
        }
    })
    await prisma.updateCombat({
        where: {
            id: combatId
        },

        data: {
            score1: 12,
            winner: 1,
            status: 'F',
            score2: 9,
            scores: {
                create: scores
            }
        }
    })
    const combatHandler  =  new Combat();
    await combatHandler.findById(combatId)
    await combatHandler.finishCombat()
}
main("6464bf0502743900086e9363")