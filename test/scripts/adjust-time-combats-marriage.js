const { prisma } = require("../../datasources/mongodb/generated/prisma-client");

const combats = [
    [{ arena: 1, time: 1100 }, { arena: 3, time: 1300 }],
    [{ arena: 2, time: 1200 }, { arena: 1, time: 1100 }],
    [{ arena: 3, time: 1300 }, { arena: 2, time: 1200 }]
]
async function main() {
    const combatsToUpdate = []


    for (const [combatCurrent, combatNew] of combats) {
        const combat = (await prisma.combats({
            where: {
                time: combatCurrent.time,
                arena: combatCurrent.arena,
                tournament: {
                    id: "646d386d0274390008526f70"
                }
            }
        }))
        combatsToUpdate.push({ ...combat[0], currentTime: combatCurrent.time, currentArena: combatCurrent.arena,  time: combatNew.time, arena: combatNew.arena })
    }
    for (const combat of combatsToUpdate) {
        const team1 = await (prisma.combat({ id: combat.id }).team1())
        const team2 = await (prisma.combat({ id: combat.id }).team2())
        console.log(`Moving combat from: ${combat.currentTime}, arena ${combat.currentArena} to: ${combat.time}, arena ${combat.arena}`)
        console.log(`Team1: ${team1.name} - Team2: ${team2.name} `)
        await prisma.updateCombat({
            where: {
                id: combat.id
            },
            data: {
                arena: combat.arena,
                time: combat.time,
            }
        })
    }

    process.exit()
}

main()