const Combat = require("../../Entities/Combat");
const Team = require("../../Entities/Team");
const Tournament = require("../../Entities/Tournament");

/**
 *     const combats = await combatsList.sort((a,b) => a.time - b.time).sort((a,b) => a.arena - b.arena).slice(0, 4).filter(combat=> arenas.indexOf(combat.arena) !== -1);

 * @param {string} tournamentId
 * @param {Array<number>} arenas
 */
async function main(tournamentId, arenas) {
    const tournament = new Tournament();
    await tournament.findById(tournamentId);
    const combatsList = await tournament.combats(["P"])
    const combats = await combatsList.slice(0, 4).sort((a,b) => a.arena - b.arena).filter(it => it.time === 2200).filter(c => c.id=== "646b81d824aa9a0007ea0b96")
    const combatHandler = new Combat()
    const teamHandler = new Team()
    
    // if(combats.length === 4 ){
    //     console.log(`Asignando sacos a ${combats.length} combates, hora primer combate: ${combats[0].time}, hora ultimo combate: ${combats[3].time}`)
    // } else {
    //     console.log(`Asignando sacos a ${combats.length} combate, hora: ${combats[0].time}`)
    // }
    let bag = 33;
    for (const combat of combats) {
        await combatHandler.findById(combat.id)
        await teamHandler.findById((await combatHandler.team1()).id)
        const fbxsTeam1 = await teamHandler.fitboxers()
        await teamHandler.findById((await combatHandler.team2()).id)
        const fbxsTeam2 = await teamHandler.fitboxers()
        const participants = {}
        fbxsTeam1.forEach(it => {
            participants[it.id] = "0"
        })
        fbxsTeam2.forEach(it => {
            participants[it.id] = "0"
        })
        fbxsTeam1.splice(4,1)
        fbxsTeam2.pop(4,1)
        for (const fitboxer of [...fbxsTeam1, ...fbxsTeam2]) {
            participants[fitboxer.id] = `${bag}`
            bag++
        }

        await combatHandler.setParticipants(participants)
        // const team1 =  prisma
        // await mon
    }
    console.log('Done')
    process.exit()
}

main("643d021024aa9a0007876ab4", [1,2,3])
