const fs = require("fs");
const { format } = require("date-fns");
const {
  prisma: mongodb,
} = require("../../datasources/mongodb/generated/prisma-client");
const District = require("../../../../brooklyn-server/abstractTypes/District");
const FitboxerBrk = require("../../../../brooklyn-server/abstractTypes/Fitboxer");
const { User } = require("../../Entities/User");
const { writeToFile } = require("../../lib/helpers/files");
const { finished } = require("stream");

/**
 * Este script permite generar un CSV con los datos necesarios para importarlos en Marketing Cloud
 */
const avoidDuplicates = {};

const fileStream = fs.createWriteStream(
  `${__dirname}/../../outputs/tournaments-audiences.csv`,
  {
    flags: "w",
  }
);

const HEADERS = [
  "ContactKey",
  "FirstName",
  "LastName",
  "Email",
  "Phone",
  "Rol",
  "TeamName",
  "TeamCode",
  "TeamClubAlias",
  "TeamRankingPosition",
  "TeamStandingPosition",
  "TeamCountry",
  "TeamState",
  "TeamTournament",
];

async function main() {
  writeToFile(fileStream, HEADERS);

  const tournaments = await mongodb.tournaments({ where: { final: true } });
  // const teams = await mongodb.teams({ where: { tournament: { id_not: null } } })
  const clubIdCache = {};
  for (const tournament of tournaments) {
    const teams = await mongodb.tournament({ id: tournament.id }).teams();

    const TeamTournament = `${format(new Date(tournament.date), "yyyyMMdd")}_${
      tournament.clubAlias
    }`;

    for (const team of teams) {
      const fitboxers = await mongodb.team({ id: team.id }).fitboxers();
      const trainer = await mongodb.team({ id: team.id }).createdBy();
      const users = [...fitboxers, trainer];
      for (const user of users) {

        if (avoidDuplicates[user.id]) { // already exist
          continue;
        }
        avoidDuplicates[user.id] = user;
        let club = null;

        let ContactKey = `FWG_${(user.phone || "")
          .replace("+", "")
          .replace(/ +/g, "")}`;

        const districtHandler = new District();
        const userHandler = new User();
        const fiboxerHandlerBrk = new FitboxerBrk();
      
        if (!clubIdCache[team.clubId]) {
          club = team.clubId
            ? await districtHandler.findById(
                team.clubId.substring(team.clubId.indexOf("CL_") + 1)
              )
            : {};
          clubIdCache[team.clubId] = club;
        } else {
          club = clubIdCache[team.clubId];
        }
        try {
          let record = {};
          if (user.type === "FITBOXER") {
            let fbFwg = await userHandler.findById(user.id);
            if (!fbFwg.get("externalId")) {
              const resMigrated = await fiboxerHandlerBrk.findById(
                parseInt(fbFwg.get("v7Id"))
              );
              await userHandler.updateProfile({
                externalId: resMigrated.id,
              });
            }
            await userHandler.findById(user.id);
            let usrBfi = {};
            if (userHandler.get("v7Id")) {
              if (user.type === "FITBOXER") {
                usrBfi = await fiboxerHandlerBrk.findById(
                  parseInt(userHandler.get("v7Id"))
                );
              }
            }
            ContactKey = usrBfi.contactId;
          }

          record = {
            ContactKey: ContactKey || user.phone,
            FirstName: user.firstName,
            LastName: user.lastName,
            Email: user.email,
            Phone: `+${(user.phone || "")
              .replace("+", "")
              .replace(/ +/g, "")
              .trim()}`,
            Rol: user.isCaptain ? "CAPTAIN" : user.type,
            TeamName: team.name,
            TeamCode: team.code,
            TeamClubAlias: club.alias,
            TeamRankingPosition: team.rankingPosition || null,
            TeamStandingPosition: team.standingPosition || null,
          };

          record = {
            ...record,
            TeamCountry: club.ShippingCountryCode,
            TeamState: club.ShippingStateCode,
            TeamTournament,
          };
          // result.push(record);
          await writeToFile(fileStream, Object.values(record));
        } catch (err1) {
          console.log(`Error searching user: `, user);
          console.log(err1);
        }
      }
    }
  }
  // fs.writeFileSync('./teams-tournaments-fwg.json', JSON.stringify(result))
  console.log("Done");
  process.exit();
}
main();
