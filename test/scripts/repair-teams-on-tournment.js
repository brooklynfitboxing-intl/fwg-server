const { prisma } = require("../../datasources/mongodb/generated/prisma-client");
async function main() {
    await prisma.updateTournament({
        where: {
            id: "646d386d0274390008526f70"
        },
        data: {
            status: "FULL"
        }
    })
    process.exit()
    
    const teams = await prisma.teams({
        where: {
            code_starts_with: 'FWG',
        }
    })
    for (const team of teams) {
        await prisma.updateTournament({
            where: {
                id: "646d386d0274390008526f70"
            },
            data: {
                teams: {
                    connect: {
                        code: team.code
                    }
                }
            }
        })
        await prisma.updateTeam({
            where: {
                id: team.id
            },
            data: {
                tournament: {
                    connect: {
                        id: "646d386d0274390008526f70"
                    }
                }
            }

        })

    }
    await prisma.updateTournament({
        where: {
            id: "646d386d0274390008526f70"
        },
        data: {
            status: 'FULL'
        }
    })
    process.exit()

}
main()