module.exports=[
    {
        "name": "Atlanta Hawks",
        "code": "FWG_1"
    },
    {
        "name": "Boston Celtics",
        "code": "FWG_2"
    },
    {
        "name": "Brooklyn Nets",
        "code": "FWG_3"
    },
    {
        "name": "Charlotte Hornets",
        "code": "FWG_4"
    },
    {
        "name": "Chicago Bulls",
        "code": "FWG_5"
    },
    {
        "name": "Cleveland Cavaliers",
        "code": "FWG_6"
    },
    {
        "name": "Dallas Mavericks",
        "code": "FWG_7"
    },
    {
        "name": "Denver Nuggets",
        "code": "FWG_8"
    },
    {
        "name": "Detroit Pistons",
        "code": "FWG_9"
    },
    {
        "name": "Golden State Warriors",
        "code": "FWG_10"
    },
    {
        "name": "Houston Rockets",
        "code": "FWG_11"
    },
    {
        "name": "Indiana Pacers",
        "code": "FWG_12"
    },
    {
        "name": "LA Clippers",
        "code": "FWG_13"
    },
    {
        "name": "Los Angeles Lakers",
        "code": "FWG_14"
    },
    {
        "name": "Memphis Grizzlies",
        "code": "FWG_15"
    },
    {
        "name": "Miami Heat",
        "code": "FWG_16"
    },
    {
        "name": "Milwaukee Bucks",
        "code": "FWG_17"
    },
    {
        "name": "Minnesota Timberwolves",
        "code": "FWG_18"
    },
    {
        "name": "New Orleans Pelicans",
        "code": "FWG_19"
    },
    {
        "name": "New York Knicks",
        "code": "FWG_20"
    },
    {
        "name": "Oklahoma City Thunder",
        "code": "FWG_21"
    },
    {
        "name": "Orlando Magic",
        "code": "FWG_22"
    },
    {
        "name": "Philadelphia 76ers",
        "code": "FWG_23"
    },
    {
        "name": "Phoenix Suns",
        "code": "FWG_24"
    },
    {
        "name": "Portland Trail Blazers",
        "code": "FWG_25"
    },
    {
        "name": "Sacramento Kings",
        "code": "FWG_26"
    },
    {
        "name": "San Antonio Spurs",
        "code": "FWG_27"
    },
    {
        "name": "Toronto Raptors",
        "code": "FWG_28"
    },
    {
        "name": "Utah Jazz",
        "code": "FWG_29"
    },
    {
        "name": "Washington Wizards",
        "code": "FWG_30"
    },
    {
        "name": "Barcelona",
        "code": "FWG_31"
    },
    {
        "name": "Real Madrid",
        "code": "FWG_32"
    },
    {
        "name": "Maccabi Tel Aviv",
        "code": "FWG_33"
    },
    {
        "name": "Fenerbahce Istanbul",
        "code": "FWG_34"
    },
    {
        "name": "Olympiacos Piraeus",
        "code": "FWG_35"
    },
    {
        "name": "Panathinaikos Athens",
        "code": "FWG_36"
    },
    {
        "name": "CSKA Moscow",
        "code": "FWG_37"
    },
    {
        "name": "BC Khimki Moscow Region",
        "code": "FWG_38"
    },
    {
        "name": "Zalgiris Kaunas",
        "code": "FWG_39"
    },
    {
        "name": "Baskonia Vitoria Gasteiz",
        "code": "FWG_40"
    },
    {
        "name": "Efes Istanbul",
        "code": "FWG_41"
    },
    {
        "name": "Bayern Munich",
        "code": "FWG_42"
    },
    {
        "name": "ASVEL Villeurbanne",
        "code": "FWG_43"
    },
    {
        "name": "Zenit St Petersburg",
        "code": "FWG_44"
    },
    {
        "name": "Alba Berlin",
        "code": "FWG_45"
    },
    {
        "name": "Valencia Basket",
        "code": "FWG_46"
    },
    {
        "name": "LDLC ASVEL Villeurbanne",
        "code": "FWG_47"
    },
    {
        "name": "Unicaja Malaga",
        "code": "FWG_48"
    },
    {
        "name": "Maccabi Rishon Lezion",
        "code": "FWG_49"
    },
    {
        "name": "Brose Bamberg",
        "code": "FWG_50"
    },
    {
        "name": "KK Crvena Zvezda",
        "code": "FWG_51"
    },
    {
        "name": "Buducnost VOLI Podgorica",
        "code": "FWG_52"
    },
    {
        "name": "Partizan Belgrade",
        "code": "FWG_53"
    },
    {
        "name": "Lietkabelis Panevezys",
        "code": "FWG_54"
    },
    {
        "name": "Hapoel Jerusalem",
        "code": "FWG_55"
    },
    {
        "name": "Iberostar Tenerife",
        "code": "FWG_56"
    },
    {
        "name": "UNICS Kazan",
        "code": "FWG_57"
    },
    {
        "name": "Tofas Bursa",
        "code": "FWG_58"
    },
    {
        "name": "FMP Belgrade",
        "code": "FWG_59"
    },
    {
        "name": "Nanterre 92",
        "code": "FWG_60"
    },
    {
        "name": "Oostende",
        "code": "FWG_61"
    },
    {
        "name": "Virtus Segafredo Bologna",
        "code": "FWG_62"
    },
    {
        "name": "Limoges CSP",
        "code": "FWG_63"
    },
    {
        "name": "Cholet Basket",
        "code": "FWG_64"
    }
]