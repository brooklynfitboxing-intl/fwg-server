const { prisma } = require("../../datasources/mongodb/generated/prisma-client")

const teamsPro = [
    {
      "name": "The Dragons",
      "code": "77kp1o14p",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6387131924aa9a0007d61848.png",
      "id": "6387131924aa9a0007d61848",
      "order": 12
    },
    {
      "name": "Tiger Squad",
      "code": "iostzhpzl",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6387131b24aa9a0007d6184b.png",
      "id": "6387131b24aa9a0007d6184b",
      "order": 10
    },
    {
      "name": "Oli's Punchers",
      "code": "3l4iczp2c",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6387131c24aa9a0007d61852.png",
      "id": "6387131c24aa9a0007d61852",
      "order": 13
    },
    {
      "name": "Ace of Roses",
      "code": "mkln0k07q",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63871a1624aa9a0007d622d9.png",
      "id": "63871a1624aa9a0007d622d9",
      "order": 3
    },
    {
      "name": "BALBOAS",
      "code": "kb89gsr1s",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63871c6924aa9a0007d62567.png",
      "id": "63871c6924aa9a0007d62567",
      "order": 8
    },
    {
      "name": "Stica Team",
      "code": "otw1focy",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63871e2f24aa9a0007d627d6.png",
      "id": "63871e2f24aa9a0007d627d6",
      "order": 6
    },
    {
      "name": "Butterflies storm",
      "code": "d1j3xwou",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63871f3424aa9a0007d637cc.png",
      "id": "63871f3424aa9a0007d637cc",
      "order": 2
    },
    {
      "name": "LA PANDILLA",
      "code": "0dfkl5pnj",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6387293f24aa9a0007d6559d.png",
      "id": "6387293f24aa9a0007d6559d",
      "order": 1
    },
    {
      "name": "LA GÜESTIA",
      "code": "ljcwj2m3",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63872e0f24aa9a0007d663d9.png",
      "id": "63872e0f24aa9a0007d663d9",
      "order": 9
    },
    {
      "name": "Brooklyn_furies",
      "code": "7vfkna0gl",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63872e1924aa9a0007d663f5.png",
      "id": "63872e1924aa9a0007d663f5",
      "order": 7
    },
    {
      "name": "UpperKannon",
      "code": "n5sk57biv",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6387362024aa9a0007d67166.png",
      "id": "6387362024aa9a0007d67166",
      "order": 11
    },
    {
      "name": "Tekis",
      "code": "vhpw0cg7",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6387374324aa9a0007d67262.png",
      "id": "6387374324aa9a0007d67262",
      "order": 9
    },
    {
      "name": "Aquelarre",
      "code": "k8rkun9ys",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63873df024aa9a0007d67b63.png",
      "id": "63873df024aa9a0007d67b63",
      "order": 2
    },
    {
      "name": "MA Fighters",
      "code": "kec1lwrs",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/638741f824aa9a0007d67ea6.png",
      "id": "638741f824aa9a0007d67ea6",
      "order": 12
    },
    {
      "name": "Random Team",
      "code": "5nbn0vf7",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63874ab424aa9a0007d68d98.png",
      "id": "63874ab424aa9a0007d68d98",
      "order": 2
    },
    {
      "name": "Fenix",
      "code": "zmafj16d",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63874abd24aa9a0007d68da0.png",
      "id": "63874abd24aa9a0007d68da0",
      "order": 12
    },
    {
      "name": "Croft",
      "code": "xdo9cgmen",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6387537e24aa9a0007d69c04.png",
      "id": "6387537e24aa9a0007d69c04",
      "order": 15
    },
    {
      "name": "Hellfish Gijón",
      "code": "e517faxln",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6387564624aa9a0007d69ed5.png",
      "id": "6387564624aa9a0007d69ed5",
      "order": 10
    },
    {
      "name": "TEAM ROCKET",
      "code": "67dcv0vee",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6387564e24aa9a0007d69ee4.png",
      "id": "6387564e24aa9a0007d69ee4",
      "order": 16
    },
    {
      "name": "Astur Panthers",
      "code": "n5gitagc",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/638759aa24aa9a0007d6b803.png",
      "id": "638759aa24aa9a0007d6b803",
      "order": 5
    },
    {
      "name": "PIMPAMFIGTHERS",
      "code": "m0tki7km3",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6387a04c24aa9a0007d7aa56.png",
      "id": "6387a04c24aa9a0007d7aa56",
      "order": 10
    },
    {
      "name": "Thunder Sisters",
      "code": "pyg5xqg5m",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63884f3724aa9a0007d8a8c6.png",
      "id": "63884f3724aa9a0007d8a8c6",
      "order": 8
    },
    {
      "name": "FIVE HOOKS",
      "code": "6bvc1lq7w",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63884f4224aa9a0007d8a8d3.png",
      "id": "63884f4224aa9a0007d8a8d3",
      "order": 1
    },
    {
      "name": "Legavengers",
      "code": "h9p9r3is",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/638854fb24aa9a0007d8acab.png",
      "id": "638854fb24aa9a0007d8acab",
      "order": 3
    },
    {
      "name": "Los Gansos de Brooklyn",
      "code": "n23947voyn",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6388552924aa9a0007d8acc3.png",
      "id": "6388552924aa9a0007d8acc3",
      "order": 4
    },
    {
      "name": "Hit Queens",
      "code": "efc0jma",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/638872fc24aa9a0007d8f60c.png",
      "id": "638872fc24aa9a0007d8f60c",
      "order": 8
    },
    {
      "name": "Crazy Cross",
      "code": "xxzbhmg73",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63888c2124aa9a0007d92435.png",
      "id": "63888c2124aa9a0007d92435",
      "order": 4
    },
    {
      "name": "Mi.Tic.",
      "code": "rftjfvhwk",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6388b6c424aa9a0007d96cb8.png",
      "id": "6388b6c424aa9a0007d96cb8",
      "order": 11
    },
    {
      "name": "Powerful gloves",
      "code": "z0l0biryn",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6388dac924aa9a0007d9cb97.png",
      "id": "6388dac924aa9a0007d9cb97",
      "order": 15
    },
    {
      "name": "LITTLE ANGELS",
      "code": "41ubophwd",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6388f37d24aa9a0007da48f4.png",
      "id": "6388f37d24aa9a0007da48f4",
      "order": 6
    },
    {
      "name": "KARMÁTICOS",
      "code": "ja7zss9p",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/638989d124aa9a0007db0df0.png",
      "id": "638989d124aa9a0007db0df0",
      "order": 5
    },
    {
      "name": "The Breakers",
      "code": "df2w135p5",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6389d1e324aa9a0007db8de2.png",
      "id": "6389d1e324aa9a0007db8de2",
      "order": 5
    },
    {
      "name": "Wildsierra",
      "code": "h44uy47ht",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6389e2ae24aa9a0007dba14d.png",
      "id": "6389e2ae24aa9a0007dba14d",
      "order": 13
    },
    {
      "name": "Sargento's Team",
      "code": "gu3qeybed",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6389eed424aa9a0007dbaefb.png",
      "id": "6389eed424aa9a0007dbaefb",
      "order": 3
    },
    {
      "name": "Mi vida loca",
      "code": "k0ochmlhf",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6389f79324aa9a0007dbb908.png",
      "id": "6389f79324aa9a0007dbb908",
      "order": 11
    },
    {
      "name": "The Wolfpack",
      "code": "rfo5m3p4u",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6389f7e024aa9a0007dbb948.png",
      "id": "6389f7e024aa9a0007dbb948",
      "order": 3
    },
    {
      "name": "TULLIS",
      "code": "yrbzpkats",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/638a09e924aa9a0007dbda32.png",
      "id": "638a09e924aa9a0007dbda32",
      "order": 15
    },
    {
      "name": "Black Panthers",
      "code": "z2m67wyk",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/638b4b1824aa9a0007ddfd3b.png",
      "id": "638b4b1824aa9a0007ddfd3b",
      "order": 7
    },
    {
      "name": "DIMONIS PUNCH",
      "code": "4236lnnra",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/638dea0a24aa9a0007e09336.png",
      "id": "638dea0a24aa9a0007e09336",
      "order": 1
    },
    {
      "name": "Comando G",
      "code": "ex38xvsl",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6392292b24aa9a0007e79b3f.png",
      "id": "6392292b24aa9a0007e79b3f",
      "order": 8
    },
    {
      "name": "Tarzan’s Angels",
      "code": "burr5a4jg",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6393a48e24aa9a0007ea1d4d.png",
      "id": "6393a48e24aa9a0007ea1d4d",
      "order": 6
    },
    {
      "name": "Numantinos",
      "code": "o1gdc9d9",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6394608824aa9a0007eac02e.png",
      "id": "6394608824aa9a0007eac02e",
      "order": 14
    },
    {
      "name": "La Navaja Mecánica",
      "code": "kce8f252f",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6396f89a24aa9a0007ed691d.png",
      "id": "6396f89a24aa9a0007ed691d",
      "order": 9
    },
    {
      "name": "OLIMPO SQUAD",
      "code": "l0y3hhw9",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6397424b24aa9a0007ee1591.png",
      "id": "6397424b24aa9a0007ee1591",
      "order": 4
    },
    {
      "name": "Devil's Queens",
      "code": "xi6trf8p6",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6397829f24aa9a0007ef8936.png",
      "id": "6397829f24aa9a0007ef8936",
      "order": 15
    },
    {
      "name": "The Punishers",
      "code": "nfnlqsan",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/6399e17224aa9a0007f40e0f.png",
      "id": "6399e17224aa9a0007f40e0f",
      "order": 2
    },
    {
      "name": "Golden Phoenix",
      "code": "75c26xisx",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639afaac24aa9a0007f63995.png",
      "id": "639afaac24aa9a0007f63995",
      "order": 14
    },
    {
      "name": "ESCUADRÓN CANARIO",
      "code": "iqsr4ehur",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639b0f7e24aa9a0007f64eff.png",
      "id": "639b0f7e24aa9a0007f64eff",
      "order": 5
    },
    {
      "name": "Atlántidas de Guanarteme",
      "code": "1eikk58pl",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639b0f8b24aa9a0007f64f09.png",
      "id": "639b0f8b24aa9a0007f64f09",
      "order": 10
    },
    {
      "name": "Titanas",
      "code": "wzqbfmzel",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639b2e9424aa9a0007f69ad4.png",
      "id": "639b2e9424aa9a0007f69ad4",
      "order": 6
    },
    {
      "name": "LAS ESTATUAS",
      "code": "18cum1hsl",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639b46a924aa9a0007f6c32c.png",
      "id": "639b46a924aa9a0007f6c32c",
      "order": 4
    },
    {
      "name": "Final Round 3.0",
      "code": "xh19ryde",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639b7c9724aa9a0007f7dcc3.png",
      "id": "639b7c9724aa9a0007f7dcc3",
      "order": 16
    },
    {
      "name": "The Rockies",
      "code": "0m4ib984f",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639b9e3a24aa9a0007f82cf7.png",
      "id": "639b9e3a24aa9a0007f82cf7",
      "order": 16
    },
    {
      "name": "Sangre de Panda 🐼",
      "code": "w76lji3x",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639bb31824aa9a0007f83817.png",
      "id": "639bb31824aa9a0007f83817",
      "order": 9
    },
    {
      "name": "Femmes Fighters",
      "code": "sn3ia2dtd",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639cbc5124aa9a0007f9bad2.png",
      "id": "639cbc5124aa9a0007f9bad2",
      "order": 1
    },
    {
      "name": "Tost-e Warriors",
      "code": "6nqwffb5i",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639df73624aa9a0007fb2a9f.png",
      "id": "639df73624aa9a0007fb2a9f",
      "order": 13
    },
    {
      "name": "Jab Box Team",
      "code": "w95i80iqd",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639f20ea24aa9a0007fc32c2.png",
      "id": "639f20ea24aa9a0007fc32c2",
      "order": 7
    },
    {
      "name": "SPARTAN MASPALOMAS",
      "code": "l1ft50qjd",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639f314124aa9a0007fc5229.png",
      "id": "639f314124aa9a0007fc5229",
      "order": 14
    },
    {
      "name": "Street Fit Fighters",
      "code": "tncbmt3c",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639fa35124aa9a0007fca073.png",
      "id": "639fa35124aa9a0007fca073",
      "order": 13
    },
    {
      "name": "Five Elements",
      "code": "9u6hjkx0m",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/639fa4b524aa9a0007fca10d.png",
      "id": "639fa4b524aa9a0007fca10d",
      "order": 14
    },
    {
      "name": "SOUL OF FIRE",
      "code": "qkajlbqte",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63a2f35d24aa9a0007038e99.png",
      "id": "63a2f35d24aa9a0007038e99",
      "order": 16
    },
    {
      "name": "Million Dollar Babies",
      "code": "4o3k2kgf7",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63a334d924aa9a0007040ae9.png",
      "id": "63a334d924aa9a0007040ae9",
      "order": 11
    },
    {
      "name": "THE FUENLA SHARKS",
      "code": "6ohal19jh",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63a5969524aa9a0007084953.png",
      "id": "63a5969524aa9a0007084953",
      "order": 7
    },
    {
      "name": "Gym & Tonic",
      "code": "whmfmi49",
      "logo": "https://brooklyn-fwg.s3-eu-west-1.amazonaws.com/teams/63b44f2f24aa9a00071ac311.png",
      "id": "63b44f2f24aa9a00071ac311",
      "order": 12
    }
  ]
async function main() {
    const teams = await prisma.teams({
        where: {
            tournament:{
                final: true
            }
        }
    })
    for(const team of teams) {
        const teamUsed = teamsPro.shift()
        await prisma.updateTeam({
            where:{
                code: team.code,

            },
            data:{
                logo: teamUsed.logo,
                name: teamUsed.name,
                order: teamUsed.order
            }
        })

    }
    process.exit()

}
main()