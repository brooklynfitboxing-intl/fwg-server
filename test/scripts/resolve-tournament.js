const Combat = require("../../Entities/Combat");
const Team = require("../../Entities/Team");
const Tournament = require("../../Entities/Tournament");
const {
  prisma: mongodb,
} = require("../../datasources/mongodb/generated/prisma-client");
const { restartCombatsOnPhase } = require("./helper/restartPhase");
const bagsMap = {
  1: [1, 8],
  2: [9, 16],
  3: [17, 24],
  4: [25, 32],
  5: [33, 40],
};

/**
 *
 * @param {Combat} combatHandler
 */
async function getBagsAssigned(combatHandler, arena) {
  console.log(
    `setting participants on Combat:${combatHandler.get(
      "time"
    )}, arena:${combatHandler.get("arena")}`
  );
  const participants = {};
  let bag = bagsMap[arena][0];
  const team1 = await new Team().set(await combatHandler.team1()).fitboxers();
  const team2 = await new Team().set(await combatHandler.team2()).fitboxers();
  team1.forEach((it) => {
    participants[it.id] = "0";
  });
  team2.forEach((it) => {
    participants[it.id] = "0";
  });
  team1.splice(4, 1);
  team2.pop(4, 1);
  for (const fitboxer of [...team1, ...team2]) {
    participants[fitboxer.id] = `${bag}`;
    bag++;
  }
  return participants;
}

// const bagDummy = { "mac": null, "sb": 1, "hits": 123, "fbp": 11, "pwp": 27, "totp": 100, "workout": 0, "power": 0 };
async function main(id, phaseToResolve) {
  await mongodb.tournament({ id });
  const TournamentEntity = new Tournament();
  await TournamentEntity.findById(id);

  const phases = (await TournamentEntity.phases())

  const phase = phases.filter(
    (it) => it.name === phaseToResolve
  )[0];

  if (!phase) {
    console.log(`Phase: ${phaseToResolve}, doesn't exist`)
  }
  const combats = (await TournamentEntity.combats("P", phase.id)).sort(
    (a, b) => a.time - b.time || a.arena - b.arena
  );

  const combatsByTime = combats.reduce((acc, combat) => {
    if (!acc[combat.time]) {
      acc[combat.time] = [];
    }
    acc[combat.time].push(combat);
    return {
      ...acc,
    };
  }, {});

  for (const time in combatsByTime) {

    const combats = combatsByTime[time];
    console.log(`------------------------------------------------`);
    console.log(`--------Processing combats at ${time}--------------------`);
    console.log(`------------------------------------------------`);

    await Promise.all(
      combats.map(async (combat) => {
        const combatHandler = new Combat();
        await combatHandler.findById(combat.id);

        console.log(
          `processing Combat:${combatHandler.get(
            "time"
          )}, arena:${combatHandler.get("arena")}`
        );

        const participants = await getBagsAssigned(
          combatHandler,
          combatHandler.get("arena")
        );

        await combatHandler.setParticipants(participants);

        const refereeIdList = (await combatHandler.referees()).map(
          (it) => it.id
        );

        for (let comboNumber = 1; comboNumber <= 3; comboNumber++) {
          console.log(
            `Setting results and votes on arena:${combatHandler.get(
              "arena"
            )}, combo:${comboNumber}`
          );

          const bags = bagsMap[combat.arena];
          const bagResult = [];
          const votes = {};
          for (const refereeId of refereeIdList) {
            votes[refereeId] = {
              technique: Math.floor(Math.random() * 2) + 1,
              coordination: Math.floor(Math.random() * 2) + 1,
              hits: Math.floor(Math.random() * 2) + 1,
              ranges: Math.floor(Math.random() * 2) + 1,
            };
          }
          await combatHandler.setVotes(comboNumber, votes, "65d374c00274390007e9c24e");

          for (let bagIdx = bags[0]; bagIdx <= bags[1]; bagIdx++) {
            bagResult.push({
              sb: bagIdx,
              workout: Math.floor(Math.random() * 2) + 1,
              status: "OK",
            });
          }

          await combatHandler.setResults(comboNumber, bagResult);
          console.log(`combo:${comboNumber} done`);
        }
      })
    );
  }

}

async function start(tournamentId, phaseName) {
  console.log(`--------------------------------`);
  console.log(`Processing combats for ${phaseName}`);
  console.log(`--------------------------------`);
  await main(tournamentId, phaseName);
  process.exit();
}
// async function processWholeTournament() {

//     console.log(`-------------PHASE ${phase}-------------------`)
//     const mins = ["00", "15", "45"]
//     const hours = ["10", "11", "12", "13", "14", "15"]
//     const times = []
//     for (const hour of hours) {
//         for (const min of mins) {
//             const time = parseInt(`${hour}${min}`)
//             times.push(time)
//             console.log(`Processing combats for ${time}`)
//             console.log(`--------------------------------`)

//             await main("646d386d0274390008526f70", time)
//         }
//     }
//     process.exit()

// }
start("6628f2b90274390008079dd3", "ROUND_OF_16");
// restartCombatsOnPhase("6628f2b90274390008079dd3", "ROUND_OF_16")
