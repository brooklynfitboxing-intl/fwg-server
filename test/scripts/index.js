
const Tournament = require('../../Entities/Tournament');
const { prisma } = require('../../datasources/mongodb/generated/prisma-client')

async function generateEnergyForCombat(combatId) {
  const combat = await prisma.combat({ id: combatId }).$fragment(`
  fragment combatFragment on combat {
    id
    participants
    team1{
      code
      name
      fitboxers {

        id
        firstName
        lastName
        photo
        nickName
      }

    }
    team2{
      code
      name
      fitboxers {

        id
        firstName
        lastName
        photo
        nickName
      }

    }
    scores {
      number
      technique
      ranges
      votes
      energy
      results 
      
    }
  }
 `);
  const teams = { team1: { ...combat.team1 }, team2: { ...combat.team2 } }
  const bags = combat.participants
  console.log(bags);
  for (const score of combat.scores) {
    const results = {
      team1: { total: 0, detail: [] },
      team2: { total: 0, detail: [] },
    }
    for (const team in teams) {
      for (const fitboxer of teams[team].fitboxers) {
        if (parseInt(bags[fitboxer.id])) {
          results[team].detail.push({
            ...fitboxer,
            logo: fitboxer.photo,
            status: 'OK',
            name: fitboxer.firstName,
            bag: bags[fitboxer.id],
            power: Math.floor((Math.random() * (1200 - 900) + 900)),
            energy: Math.floor((Math.random() * (100 - 90) + 90)),
          })
        }
      }
      results[team].total =
        Math.round(
          (results[team].detail.reduce((acc, it) => acc + it.energy, 0) / results[team].detail.length) * 100) / 100
    }
    score.energy = results['team1'].total > results['team2'].total ? 1 : 2
    await prisma.updateCombat({
      where: { id: combat.id }, data: {
        scores: {
          updateMany: {
            where: { number: score.number }, data: {
              ...score,
              results: results
            }
          }
        }
      }
    })
  }
}
async function getPlaceholderImage(users) {
  for (const fb of users) {
    const pictureUrl = await fetch("https://loremflickr.com/100/100")
    fb.photo = pictureUrl.url
  }
  return users;
}
async function assignPictureToFbx() {
 
 
  const users = await prisma.users({ where: { type: "FITBOXER" } })
  const chunks = users.reduce((result, current, idx) => {
    const chunkIndex = Math.floor(idx / 30)
    if (!result[chunkIndex]) {
      result[chunkIndex] = [] // start a new chunk
    }
    result[chunkIndex].push(current)
    return result
  }, [])

  let processed = 0
  for (const userList of chunks) {
    await getPlaceholderImage(userList)
    await Promise.all(userList.map((user) => {
      return prisma.updateUser({
        where: {
          id: user.id

        },
        data: {
          photo: user.photo
        }
      })
    }))
    processed += userList.length
    console.log(`${processed} fitboxers updated`)
  }
}

async function main() {
  const tournament = await prisma.tournament({ id: "" })

  /** Calcular GlobalRanking, cuando ya no hayan torneos pendientes en el dia*/

  const tournamentsToday = await prisma.tournaments({
    where: { date_gte: new Date(tournament.date).getDate() }
  });
  console.log('TODAY 1 ', tournamentsToday.length)

  const startOfDay = new Date(tournament.date);
  startOfDay.setUTCHours(0, 0, 0, 0);
  const endOfDay = new Date(tournament.date);
  endOfDay.setUTCHours(23, 59, 59, 999);

  const existsTournamentsPending = await prisma.tournaments({
    where: {
      date_gte: startOfDay,
      date_lte: endOfDay,
      finished: false
    }
  })
  console.log('TODAY 2', existsTournamentsPending.length)
  /* 
  if (!existsTournamentsPending) {
    Tournament.updateGlobalRanking()
  }*/
}

async function updateGlobalRanking() {
  await Tournament.updateGlobalRanking();
  console.log("done")
  process.exit();

}

updateGlobalRanking();
// generateEnergyForCombat("63e380bc027439000ad9f8c2")