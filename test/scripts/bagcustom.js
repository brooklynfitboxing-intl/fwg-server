const { prisma } = require("../../datasources/mongodb/generated/prisma-client")

async function main() {

    const participants = {
        "6385bdd524aa9a0007d30323": "11",
        "63860efb24aa9a0007d3b8a1": "12",
        "63887b4e24aa9a0007d90291": "10",
        "6398799024aa9a0007f0f683": "0",
        "6436796424aa9a00077a8299": "9",
        "638880aa24aa9a0007d9188b": "16",
        "638881f324aa9a0007d91a3e": "14",
        "6388833824aa9a0007d91b3f": "13",
        "638884ef24aa9a0007d91d25": "15",
        "644b76f924aa9a0007a9895b": "0"
    }
    await prisma.updateCombat({
        where: {
            id: "646b81cd24aa9a0007ea0b59"
        },
        data: {
            participants, 
        }
    })
}
main()