const { prisma: mongodb } = require("../../../datasources/mongodb/generated/prisma-client");


module.exports = {
  restartCombatsOnPhase: async function(tournamentId, phaseName) {
    const {
      restartCombat,
    } = require("../../../services/fwg/resolvers/mutations/Combats");
    const tournament = mongodb.tournament({ id: tournamentId });
    const phases = await tournament.phases();

    const phase = phases.filter((it) => it.name === phaseName)[0];

    const combats = await mongodb.combats({
      where: {
        tournament: { id: tournamentId },
        phase: phase.id,
      },
    });

    await Promise.all(
      combats.map((combat) => {
        if (phase !== "GROUP_STAGE") {
          return mongodb.updateCombat({
            where: {
              id: combat.id,
            },
            data: {
              status: "P",
              lastCombo: 0,
              score1: null,
              score2: null,
              winner: null,
              game: null,
              // team1: {
              //   disconnect: true
              // },
              // team2:{
              //   disconnect: true
              // },
              participants: null,
              scores: {
                deleteMany: [{ number_not: 0 }],
              },
              reviews: {
                deleteMany: [{ number_not: 0 }],
              },
            },
          });
        }
        return restartCombat(null, { id: combat.id });
      })
    );
    console.log("done");
  },
};
