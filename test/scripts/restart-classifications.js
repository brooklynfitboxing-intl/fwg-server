const { prisma } = require("../../datasources/mongodb/generated/prisma-client")

async function main () {
    await prisma.updateManyClassifications({
        where:{
            tournament:{
                id: "643d021024aa9a0007876ab4"
            }
        },
        data:{
            dif:0,
            energy:0,
            lose:0,
            order: 0,
            pa: 0,
            pf: 0,
            maxAvgEnergy: 0,
            technique: 0, 
            won: 0,
            total: 0, 

        }
    })
    console.log('done')
    process.exit();

}
main()