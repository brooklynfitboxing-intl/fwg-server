async function main() {
    const { prisma } = require('../../datasources/mongodb/generated/prisma-client')
   await  prisma.updateCombat({
        where: {
            id: "646b81bf24aa9a0007ea0ae5"
        },
        data: {
            time: 1300,
            arena: 3,
        }
    })
    await prisma.updateCombat({
        where: {
            id: "646b81c024aa9a0007ea0b0f"
        },
        data: {
            time: 1100,
            arena: 1,
        }
    })  
    
   await prisma.updateCombat({
        where: {
            id: "646b81c124aa9a0007ea0b20"
        },
        data: {
            time: 1200,
            arena: 2,
        }
    })
    process.exit()
}
main()