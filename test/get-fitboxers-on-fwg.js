const { prisma: mongodb } = require('../datasources/mongodb/generated/prisma-client')
const { emailService } = require('../../../brooklyn-server/queues');
const Team = require('../Entities/Team');
const teamsMailSent = [];
async function main() {
    const tournament = mongodb.tournament({ id: "626bb75b24aa9a000764d27f" })

    const teams = await mongodb.teams({ where: { code: "ec8l3srvv" } })
    for (const team of teams) {
        const teamEntity = new Team();
        await teamEntity.findByCode(team.code);
        const captain = await teamEntity.captain();
        const fitboxers = await teamEntity.fitboxers();

        emailService.add({
            to: captain.email,
            template: `fwg_team_registration_confirmation_${captain.locale || 'en'}`,
            global_merge_vars: [
                { name: 'name', content: captain.firstName },
                { name: 'link', content: ` ` },
                { name: 'code', content: captain.id }
            ]
        })
        for (const fitboxer of fitboxers) {
            const locale = fitboxer.locale || 'en';
            emailService.add({
                to: fitboxer.email,
                template: `fwg_fitboxer_registration_confirmation_${locale || 'en'}`,
                global_merge_vars: [
                    { name: 'name', content: captain.firstName },
                    { name: 'link', content: ` ` },
                    { name: 'code', content: fitboxer.id }
                ]
            })
        }
        teamsMailSent.push({ name: team.name, code: team.code })
    }
    console.log(new Date());
    console.log(teamsMailSent);
    // const captain = { firstName: "gleyder", locale: "es", id: `test_id` };
    // emailService.add({
    //     to: `gleyder@brooklynfitboxing.com`,
    //     template: `fwg_team_registration_confirmation_${captain.locale || 'en'}`,
    //     global_merge_vars: [
    //         { name: 'name', content: captain.firstName },
    //         { name: 'link', content: ` ` },
    //         { name: 'code', content: captain.id }
    //     ]
    // })
}
main()