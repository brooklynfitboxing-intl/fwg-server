const { prisma: mongodb } = require('../datasources/mongodb/generated/prisma-client')
const fs = require('fs')

async function main() {

    const fwgTrainers = await mongodb.users({ where: { type: 'TRAINER', locale_not: null } })
    const fwgFitboxers = await mongodb.users({ where: { type: 'FITBOXER', locale_not: null } })
    console.log(`Fitboxers:${fwgFitboxers.length}`)
    console.log(`Trainers:${fwgTrainers.length}`)

    const trainersResult = {
        'FWG': {
        },
        'GLOBAL': {
        }
    };
    const fitboxersResult = {
        'FWG': {
        },
        'GLOBAL': {
        }
    };

    for (const fitboxer of fwgFitboxers) {
        let lang = fitboxer.locale
        if (!fitboxersResult.FWG[lang]) {
            fitboxersResult.FWG[lang] = []
        }
        // if (!fitboxersResult.GLOBAL[lang]) {
        //     fitboxersResult.GLOBAL[lang] = []
        // }

        fitboxersResult.FWG[lang].push(fitboxer)
        // fitboxersResult.GLOBAL[lang].push(fitboxer)

        // if (fwgFitboxers.find(f => (f.email || '').toLowerCase().trim() === (fitboxer.email || '').toLowerCase().trim())) {
        //     fitboxersResult.FWG[lang].push(fitboxer)
        // } else {
        //     fitboxersResult.GLOBAL[lang].push(fitboxer)
        // }
    }
    for (const trainer of fwgTrainers) {
        let lang = trainer.locale
        if (!trainersResult.FWG[lang]) {
            trainersResult.FWG[lang] = []
        }
        trainersResult.FWG[lang].push(trainer)
        // if (!trainersResult.GLOBAL[lang]) {
        //     trainersResult.GLOBAL[lang] = []
        // }
        // if (fwgTrainers.find(t => t.email === trainer.email)) {
        // } else {
        //     trainersResult.GLOBAL[lang].push(trainer)
        // }
    }

    //    fs.writeFileSync('./trainers_audience.json', JSON.stringify(trainersResult, null, 4))
    
    let writeStream = fs.createWriteStream('/path/filename.csv')
    for (const type in trainersResult) {
        for (const lang in trainersResult[type]) {
            fs.writeFileSync(`./output/audiences/trainers_audience_${type}_${lang}.json`, JSON.stringify(trainersResult[type][lang], null, 4))
        }
    }

    for (const type in fitboxersResult) {
        for (const lang in fitboxersResult[type]) {
            fs.writeFileSync(`./output/audiences/fitboxers_audience_${type}_${lang}.json`, JSON.stringify(fitboxersResult[type][lang], null, 4))
        }
    }
}

main()
