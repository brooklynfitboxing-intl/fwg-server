const { prisma: mongodb } = require('../datasources/mongodb/generated/prisma-client')
/**
 * Utilidad Para pasar lista, solo es necesario especificar el id del torneo y la hora de los combates
 */
async function main() {
    const combats = await mongodb.combats({ where: { tournament: { id: "625ff6250274390009ecc6cd" }, time: 1100 } });
    let bag = 1;
    for (const combat of combats) {
        const arenasParticipantObjs = {}
        const team1 = await mongodb.combat({ id: combat.id }).team1()
        const team2 = await mongodb.combat({ id: combat.id }).team2()
        const fbxsTeam1 = await mongodb.team({ id: team1.id }).fitboxers()
        const fbxsTeam2 = await mongodb.team({ id: team2.id }).fitboxers()
        for (const fb of [...fbxsTeam1, ...fbxsTeam2]) {
            arenasParticipantObjs[combat.arena] = {
                [fb.id]: `0`,
            }
        }
        for (const fb of [...fbxsTeam1.slice(0, 4), ...fbxsTeam2.slice(0, 4)]) {
            arenasParticipantObjs[combat.arena][fb.id] = bag
            bag++
        }
        const combatDb = await mongodb.updateCombat({
            where: { id: combat.id },
            data: {
                participants: arenasParticipantObjs[combat.arena]
            }
        })
        console.log(`Combate actualizado ${combatDb.id}, con participantes `)
    }
}
main();
