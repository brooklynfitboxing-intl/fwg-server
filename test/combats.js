const { prisma: mongodb } = require('../datasources/mongodb/generated/prisma-client')

const Combat = require('../Entities/Combat')
const Tournament = require('../Entities/Tournament')
const { Trainer } = require('../Entities/User')


async function test_combat(combatId) {
    console.log('**************** COMBATE TEST ****************', combatId)

    const combatEntity = new Combat({ mongodb })
    await combatEntity.findById(combatId)
    await combatEntity.include(['team1', 'team2' ])
    // console.log('**************** COMBATE ****************\n', combatEntity.get())
    
    await combatEntity.finishCombat()
    // await combatEntity.updateCompetition()
    // await combatEntity.updateNextCombats()

}
async function test_trainer(trainerId, combatId) {
    console.log('**************** Find combat for trainer ****************\n')
    const trainer = new Trainer()
    await trainer.findById(trainerId)
    const combats = await trainer.combats(combatId)
    console.log(`Combats for trainer: ${combats.length}`)
}
async function main({ trainerId, combatId }) {
    try {
        // await test_trainer(trainerId, combatId)
        await test_combat(combatId)

    } catch (ex) {
        console.error(ex)
    } finally {
        process.exit()
    }
}

/**
 * Combate semifinal1 : 6217b3970274390008a5e31b 
 * Combate semifinal2 : 6217b3980274390008a5e31c 
 * 
 * 3th - 4th lugar: 6217b3980274390008a5e31d  (lo resuelven los perdedores de Semifinal 2 y Semifinal 1 )
 * Combate Final: 6217b3980274390008a5e31e
 */
const combatId = '622207a40274390008a5e50b'
const trainerId = '61b5e63202743900080d7485'
main({ combatId, trainerId })


