const Tournament = require('../Entities/Tournament')
const Combat = require('../Entities/Combat')

async function updateGlobalRanking() {
    await Tournament.updateGlobalRanking()
}
async function updateCompetition() {
    const tournament = new Tournament()
    const combatHandler = new Combat();
    await tournament.findById('625ff6250274390009ecc6cd')

    const combats = (await tournament.combats())
    for (const combat of combats) {
        await combatHandler.findById(combat.id)
        await combatHandler.updateCompetition()
    }

}
// async function updateCompetition() {
//     const tournament = new Tournament()
//     const combatHandler =  new Combat();
//     await tournament.findById('626bb75b24aa9a000764d27f')

//     const combats = (await tournament.combats())
//     for(const combat of combats){
//         await combatHandler.findById(combat.id)
//         await combatHandler.updateCompetition()
//     }

// }

updateGlobalRanking()