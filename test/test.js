const District = require('../../../brooklyn-server/abstractTypes/District')
const { prisma: mongodb }= require('../datasources/mongodb/generated/prisma-client/')
const Combat = require('../Entities/Combat')
const Tournament = require('../Entities/Tournament')
const { refundPayment } = require('../lib/Adyen')
//D02858EA947A0C9A07A91727'A5F900277426793ED297FBB028B803CF65C09BBC hmac webhook
async function checkCaptain(){
    const teams = await mongodb.teams({ where: { fitboxers_some:{isCaptain:true} } })
    teams.forEach(async (it)=> {
        const fitboxers = await (mongodb.team({id:it.id}).fitboxers({where:{isCaptain:true},orderBy: 'updatedAt_DESC'}))
        if(fitboxers.length > 1){
            console.log(`fitboxer multiple captains team:${it.name}`, fitboxers)
            // console.log(`Captain should be:${fitboxers[0].email}`, fitboxers[0])
            await mongodb.updateUser({where: { id:fitboxers[1].id },data:{ isCaptain: false}})
        } else {
        
        }
    })
    process.exit()

}
async function resetStanding(){
        const tournaments =  await mongodb.tournaments({where:{id_in:["6439284d24aa9a000781a5ba","6439265a24aa9a000781a2aa"]}})
    for(const tournament of tournaments){
        const teamsId  = (await mongodb.tournament({id:tournament.id}).teams()).map(it => it.id)
        await mongodb.updateManyTeams({where:{
            id_in: teamsId
        }, data:{
            standingPosition:null
        }})
    }
    console.log('Done');
    process.exit(0)
    return
}
async function main() {
    const tournamentHandler =  await new Tournament()
    await tournamentHandler.findById('63fcfe5724aa9a000745d4e6')
    const combats  = await tournamentHandler.combats()
    const combatHandler =  new Combat()
    for(const combat of combats){
        await combatHandler.findById(combat.id)
        await combatHandler.updateTournamentRanking()
    }
    console.log("done")
    process.exit(0)

}
main()
// resetStanding()
// checkCaptain()