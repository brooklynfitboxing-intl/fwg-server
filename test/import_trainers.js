const { prisma: mongodb } = require('../datasources/mongodb/generated/prisma-client')
const crypto = require('crypto')
const main = async () => {
  const hashedPassword = crypto.createHash('sha256').update('12345678').digest('hex');
  await mongodb.updateManyUsers({
    data: {
      verified: true,
      status: 'ACTIVE'
    }
  })
}
main()


// const { prisma: mongodb } = require('../datasources/mongodb/generated/prisma-client')

// const trainers = require('./trainers')

// const main = async () => {
//   const trainersToInsert = trainers.map(it => ({
//     firstName: it.firstName,
//     lastName: it.lastName,
//     email: it.email,
//     phone: it.phone.toString(),
//     password: it.password,
//     type: 'TRAINER',
//   }))
//   for (const user of trainersToInsert) {

//     await mongodb.createUser(user)
//   }
// }

// main()