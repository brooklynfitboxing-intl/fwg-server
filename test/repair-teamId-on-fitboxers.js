const {
  prisma: mongodb,
} = require("../datasources/mongodb/generated/prisma-client");
const { createWriteStream } = require("fs");
const Team = require("../Entities/Team");

const fitboxersStream = createWriteStream(`fitboxersUpdated.csv`, {
  flags: "w",
});
async function writeToFile(stream, data) {
  if (!stream.write(`${data.join(";")}\n`, "utf-8")) {
    await once(stream, "drain");
  }
}

async function repairCaptains() {
  const fitboxers = await mongodb.users({
    where: {
      type: "FITBOXER",
      isCaptain: true,
    },
  });
  const fbTeams = fitboxers.reduce((acc, fb) => {
    if (!acc[fb.teamId]) acc[fb.teamId] = [];
    acc[fb.teamId].push(fb);
    return acc;
  }, {});
  const teamsToUpdate= [];
  for (const teamId in fbTeams) {
    if (fbTeams[teamId].length > 1) {
      const team =  await mongodb.team({ id: teamId });
      teamsToUpdate.push(team)
      console.log(`Multiple captains found for team:${team.name}`);
      await mongodb.updateUser({
        where: { id: fbTeams[teamId][1].id },
        data: { isCaptain: false },
      });
    }
  }
  console.log(JSON.stringify(teamsToUpdate))
}
async function main() {
  // const tournament = mongodb.tournament({ id: "626bb75b24aa9a000764d27f" })

  const teams = await mongodb.teams({
    where: {
      fitboxers_some: {
        isCaptain: null,
      },
    },
  });
  let updated = 0;
  console.log(`total teams: ${teams.length}`);
  // const teams = await mongodb.teams({ where: { code: "ec8l3srvv" } })
  for (const team of teams) {
    const teamEntity = new Team();
    await teamEntity.findByCode(team.code);

    const fitboxers = await teamEntity.fitboxers();

    for (const fitboxer of fitboxers) {
      if (fitboxer.teamId === null) {
        await mongodb.updateUser({
          where: { id: fitboxer.id },
          data: { teamId: team.id },
        });
        writeToFile(fitboxersStream, [fitboxer.email]);
        updated++;
      }
    }
  }
  console.log("Total Actualizado", updated);
}
repairCaptains();
