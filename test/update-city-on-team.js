const { Fitboxer, District } = require('../../../brooklyn-server/abstractTypes');
const Team = require('../Entities/Team');
async function main() {
    let updated = 0;
    const teams = await new Team().findAll();
    for (const team of teams) {
        if (team.name && !team.state) {

            const captain = await new Team().set(team).captain()
            if (captain && captain.externalId) {
                const fitboxerBrk = await new Fitboxer().findById(captain.externalId);
                if (fitboxerBrk) {
                    const district = await new District().findByV7Id(fitboxerBrk.district)
                    await new Team().set(team).update({
                        state: district.ShippingState,
                        city: district.ShippingCity,
                        clubId: district.id
                    })
                    // console.log('clubId', district.id, district.alias, district.ShippingCity)
                    updated++
                }
            }
        }
    }
    console.log(`Total teams updated: ${updated}`)
}
main()