const { prisma: mongodb } = require('../datasources/mongodb/generated/prisma-client')

const Tournament = require('../Entities/Tournament')


async function assignTournamentToTeams() {
    const teams = await mongodb.teams({

        where: {
            code_starts_with: "team_",
            tournament: {
                id: "625ff6250274390009ecc6cd",
            }
        }
    });
    for (const team of teams) {
        await mongodb.updateTournament({
            where: {
                id: "625ff6250274390009ecc6cd"
            },
            data: {
                teams: { connect: { id: team.id } }
            }
        });
    }


}
async function generateTeams() {
    // mongodb.createUser(
    for (let i = 1; i <= 64; i++) {
        const team = await mongodb.upsertTeam({
            where: {
                code: `team_${i}`,
            },
            update: {

                // name: `team_${i}`,
                // createdBy: {
                //     connect: {
                //         id: "619faef902743900090d6dfc"
                //     }
                // },
                // tournament: {
                //     connect: {
                //         id: "625ff6250274390009ecc6cd"
                //     }
                // }
            }, create: {
                code: `team_${i}`,

                name: `team_${i}`,
                createdBy: {
                    connect: {
                        id: "619faef902743900090d6dfc"
                    }
                },
                tournament: {
                    connect: {
                        id: "625ff6250274390009ecc6cd"
                    }
                }
            }
        })
        const fitboxers = [`Pedro`, `Maria`, `Chelo`, `Beatriz`, `Laura`].map(fb => ({
            v7Id: "0",
            verified: true,
            email: `${fb}_${i}@email.com`,
            isCaptain: false,
            locale: `es`,
            env: `BROOKLYN`,
            nickName: `${fb}_nickName`,
            firstName: `${fb}_team_${i}`,
            lastName: `apellido_team_${i}`,
            type: `FITBOXER`,
            teamId: team.id,
            password: "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f"
        }))
        for (const fitboxer of fitboxers) {
            const fbCreated = await mongodb.createUser(fitboxer)
            await mongodb.updateTeam({
                where: { id: team.id }, data: {
                    fitboxers: {
                        connect: {
                            id: fbCreated.id
                        }
                    }
                }
            })
        }
    }
}
async function buildCalendar(tournamentId) {
    try {
        const tournamentHander = new Tournament({ mongodb })
        await tournamentHander.findById(tournamentId)
        await tournamentHander.buildCalendar()
    } catch (ex) {
        console.error(ex)
    } finally {
        process.exit()
    }
}
async function buildCustomCalendar(tournamentId) {
    try {
        const tournamentHander = new Tournament({ mongodb })
        await tournamentHander.findById(tournamentId)
        await tournamentHander.buildCustomCalendar()
    } catch (ex) {
        console.error(ex)
    } finally {
        process.exit()
    }
}
async function main(tournamentId) {
    // await buildCalendar(tournamentId);
    await buildCustomCalendar(tournamentId);
    // await assignTournamentToTeams();
}
const tournamentId = '625ff6250274390009ecc6cd'
main(tournamentId)