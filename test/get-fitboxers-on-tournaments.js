const { prisma: mongodb } = require('../datasources/mongodb/generated/prisma-client')
const { emailService } = require('../../../brooklyn-server/queues')

async function main() {
  const captain = { firstName: "gleyder", locale: "es", id: `test_id` };
  emailService.add({
    to: `gleyder@brooklynfitboxing.com`,
    template: `fwg_team_registration_confirmation_${captain.locale || 'en'}`,
    global_merge_vars: [
      { name: 'name', content: captain.firstName },
      { name: 'link', content: ` ` },
      { name: 'code', content: captain.id }
    ]
  })
}
main()