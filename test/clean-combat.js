const { prisma: mongodb } = require('../datasources/mongodb/generated/prisma-client')

async function cleanCombatForPhase(tournamentId, phaseName) {
    const tournament = mongodb.tournament({ id: tournamentId });
    const phases = await tournament.phases()
    // console.log(phases)
    // return
    const phase = phases.filter(it => it.name === phaseName)[0]

    const combats = await mongodb.combats({
        where: {
            tournament: { id: tournamentId },
            phase: phase.id,
            team1: {
                id_not: null
            },
            team2: {
                id_not: null
            }

        }
    });
    // console.log(combats.length)
    // return
    for (const combat of combats) {
        await mongodb.updateCombat({
            where: {
                id: combat.id,
            },
            data: {
                status: 'P',
                lastCombo: 0,
                score1: null,
                score2: null,
                winner: null,
                game: null,
                participants: null,
                scores: {
                    deleteMany: [{ number_not: 0 }]
                },
                reviews: {
                    deleteMany: [{ number_not: 0 }]
                },
                team1: {
                    disconnect: true
                },
                team2: {
                    disconnect: true
                }
            }
        })
    }
    console.log('done')
}


async function cleanCombat(combatId) {
    if (Array.isArray(combatId)) {
        for (const id of combatId) {
            await mongodb.updateCombat({
                where: {
                    id,
                },
                data: {
                    status: 'P',
                    lastCombo: 0,
                    score1: null,
                    score2: null,
                    winner: null,
                    game: null,
                    participants: null,
                    scores: {
                        deleteMany: [{ number_not: 0 }]
                    },
                    reviews: {
                        deleteMany: [{ number_not: 0 }]
                    },
                    team1: {
                        disconnect: true
                    },
                    team2: {
                        disconnect: true
                    }
                }
            })
        }
    } else {
        await mongodb.updateCombat({
            where: {
                id: combatId
            },
            data: {
                status: 'P',
                lastCombo: 0,
                score1: null,
                score2: null,
                winner: null,
                game: null,
                participants: null,
                scores: {
                    deleteMany: [{ number_not: 0 }]
                },
                reviews: {
                    deleteMany: [{ number_not: 0 }]
                },
                team1: {
                    disconnect: true
                },
                team2: {
                    disconnect: true
                }
            }
        })
    }
    console.log('done')
}


const tournamentId = "626bb75b24aa9a000764d27f"
// cleanCombat(["6298e59824aa9a000793318c"])
// cleanCombatForPhase(tournamentId, "PLAY_OFF");
// cleanCombatForPhase(tournamentId, "PLAY_OFF");
// cleanCombatForPhase(tournamentId, "QUARTER_FINALS");
// cleanCombatForPhase(tournamentId, "SEMIFINALS");
// cleanCombatForPhase(tournamentId, "FINAL");  

cleanCombat("6298e59b24aa9a00079331a9")
