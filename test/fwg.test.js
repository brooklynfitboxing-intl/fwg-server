/* eslint-disable no-undef */
const { prisma: mongodb } = require('../datasources/mongodb/generated/prisma-client')
const Team = require('../Entities/Team')
const User = require('../Entities/User')
const Trainer = require('../Entities/User');
const teamService = new Team({ mongodb });
const userService = new User({ mongodb });
const assert = require('assert');
const teamTest = {}
const testTrainer = {
  firstName: 'Jose trainer',
  lastName: 'Perez trainer',
  nickName: 'testTrainer123',
  email: 'test-trainer@trainer.com',
  password: '123123',
  phone: '123123-123123'
}
describe('Users', function () {
  describe('trainer', function () {
    it('should create trainer', async function () {
      let trainer = undefined
      const userService = new User();
      trainer = userService.findByEmail(testTrainer.email)
      if (trainer) {
        console.log(`Trainer:${testTrainer.email} already exists`)
      } else {
        trainer = new Trainer(testTrainer)
        await trainer.save()
      }

    })
  });
});
describe('Team', function () {
  describe('code', function () {
    it('should create team code', async function () {
      const trainer = await userService.findByEmail(testTrainer.email)
      if (!trainer) {
        throw 'Debe existir al menos un trainer'
      }
      console.log(trainer)
      const res = await teamService.createCode(trainer.id)
      teamTest.code = res.code
      console.log(`Codigo creado: ${res.code}`)
    });
  });
  describe('listTeams', function () {
    it(' Debería retornar una lista de equipos (teams)', async function () {
      const res = await teamService.findAll()
      assert.ok(res.length > 0);
    });
  });
  describe('AddFitboxerToTeam', function () {
    it(' Debería anadir un fitbxer a un team', async function () {
      const res = await teamService.findByCode()
      assert.ok(res.length > 0);
    });
  });
  describe('deleteCode', function () {
    it(` Debería eliminar el codigo creado`, async function () {
      await teamService.deleteCode(teamTest.code)
    });
    it('should delete trainer', async function () {
      const trainer = new Trainer(testTrainer)
      await trainer.delete()
    })
  });
});
