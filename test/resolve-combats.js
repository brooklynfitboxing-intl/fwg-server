const { prisma: mongodb } = require('../datasources/mongodb/generated/prisma-client')
const Combat =  require('../Entities/Combat')
/**
* Fill stage groups with winners
*/

const tournamentId = "626bb75b24aa9a000764d27f"


async function endCombat(combatId) {
    const combat = new Combat();
    await combat.findById(combatId)
    await combat.updateNextCombats()
    console.log('done')
}



async function setWinnersOnGroup(tournamentId, phaseName) {
    const { finishCombat } = require('../services/fwg/resolvers/mutations/Combats')
    const tournament = mongodb.tournament({ id: tournamentId });
    if (phaseName === 'GROUP_STAGE') {
        await mongodb.deleteManyClassifications({ tournament: { id: tournamentId } });
    }
    const phases = await tournament.phases()

    const phase = phases.filter(it => it.name === phaseName)[0]
    console.log(`Phase:`, phase)
    const combats = await mongodb.combats({
        where: {
            tournament: { id: tournamentId },
            phase: phase.id,
            // status: "P"
        }
    });
    for (const combat of combats) {
        const winner = Math.floor(Math.random() * (2 - 1 + 1) + 1)
        await finishCombat(null, { id: combat.id, winner })
    }
    // combats.forEach(async combat => {
    //     const winner = Math.floor(Math.random() * (2 - 1 + 1) + 1)
    //     await finishCombat(null, { id: combat.id, winner })
    // })
    console.log(`Total combats updated : ${combats.length}`)

}


// restartCombatsOnPhase(tournamentId, 'GROUP_STAGE');
// restartCombatsOnPhase(tournamentId, 'PLAY_OFF');
// restartCombatsOnPhase(tournamentId, 'PLAY_OFF_2');
// restartCombatsOnPhase(tournamentId, 'QUARTER_FINALS');
// restartCombatsOnPhase(tournamentId, 'SEMIFINALS');
// restartCombatsOnPhase(tournamentId, 'FINAL');
// GROUP_STAGE
// PLAY_OFF
// PLAY_OFF_2
// QUARTER_FINALS
// SEMIFINALS
// FINAL
const resolveAll = async () => {
    const phases = [
        'GROUP_STAGE',
        // 'PLAY_OFF',
        // 'PLAY_OFF_2',
        // 'QUARTER_FINALS',
        // 'SEMIFINALS',
        // 'FINAL'
    ]
    for (const phase of phases) {
        await setWinnersOnGroup(tournamentId, phase);
    }
}
// restartCombatsOnPhase(tournamentId,'QUARTER_FINALS')
async function endCombats() {

    await endCombat("6298e59b24aa9a00079331a7")
    // await endCombat("6298e59b24aa9a00079331a8")
 
}
endCombats()
// resolveAll();