// const combat_schema = [
//     { A: 1, B: 4 },
//     { A: 2, B: 3 },
//     { A: 4, B: 3 },
//     { A: 1, B: 2 },
//     { A: 2, B: 4 },
//     { A: 3, B: 1 }
// ]
// const COMBATS_CONFIG_obj = {
//     GROUP_STAGE: {
//         combat_schema,
//         arenas: 4,
//         group_size: 16,
//         combats: {
//             1100: {},
//             1200: {},
//             1300: {},
//             1400: {},
//             1500: {},
//             1600: {},
//             1115: {},
//             1215: {},
//             1315: {},
//             1415: {},
//             1515: {},
//             1615: {},
//             1130: {},
//             1230: {},
//             1330: {},
//             1430: {},
//             1530: {},
//             1630: {},
//             1145: {},
//             1245: {},
//             1345: {},
//             1445: {},
//             1545: {},
//             1645: {},
//         },
//         PLAY_OFF: {},
//         PLAY_OFF_2: {},
//         QUARTER_FINALS: {},

//     }
// }

/**
 * 
 */
const TOTAL_GROUPS = 16;
const GROUP_SIZE = 4;
const COMBATS_CONFIG = [
    {
        phase: "GROUP_STAGE",
        name: "Group Stage",
        arenaSize: 4,
        combats: [
            /* Groups 1 - 6  */
            { time: 1000, arena: 1, A: 1, B: 4, group: 1 },
            { time: 1000, arena: 2, A: 1, B: 4, group: 2 },
            { time: 1000, arena: 3, A: 1, B: 4, group: 3 },
            { time: 1000, arena: 4, A: 1, B: 4, group: 4 },

            { time: 1100, arena: 2, A: 2, B: 3, group: 1 },
            { time: 1100, arena: 3, A: 2, B: 3, group: 2 },
            { time: 1100, arena: 4, A: 2, B: 3, group: 3 },
            { time: 1100, arena: 1, A: 2, B: 3, group: 4 },

            { time: 1200, arena: 3, A: 4, B: 3, group: 1 },
            { time: 1200, arena: 4, A: 4, B: 3, group: 2 },
            { time: 1200, arena: 1, A: 4, B: 3, group: 3 },
            { time: 1200, arena: 2, A: 4, B: 3, group: 4 },

            { time: 1300, arena: 4, A: 1, B: 2, group: 1 },
            { time: 1300, arena: 1, A: 1, B: 2, group: 2 },
            { time: 1300, arena: 2, A: 1, B: 2, group: 3 },
            { time: 1300, arena: 3, A: 1, B: 2, group: 4 },

            { time: 1400, arena: 1, A: 2, B: 4, group: 1 },
            { time: 1400, arena: 2, A: 2, B: 4, group: 2 },
            { time: 1400, arena: 3, A: 2, B: 4, group: 3 },
            { time: 1400, arena: 4, A: 2, B: 4, group: 4 },

            { time: 1500, arena: 2, A: 3, B: 1, group: 1 },
            { time: 1500, arena: 3, A: 3, B: 1, group: 2 },
            { time: 1500, arena: 4, A: 3, B: 1, group: 3 },
            { time: 1500, arena: 1, A: 3, B: 1, group: 4 },

            /* Groups 5 - 8  */
            { time: 1015, arena: 1,  A: 1, B: 4, group: 5 },
            { time: 1015, arena: 2,  A: 1, B: 4, group: 6 },
            { time: 1015, arena: 3,  A: 1, B: 4, group: 7 },
            { time: 1015, arena: 4,  A: 1, B: 4, group: 8 },

            { time: 1115, arena: 2,  A: 2, B: 3, group: 5 },
            { time: 1115, arena: 3,  A: 2, B: 3, group: 6 },
            { time: 1115, arena: 4,  A: 2, B: 3, group: 7 },
            { time: 1115, arena: 1,  A: 2, B: 3, group: 8 },

            { time: 1215, arena: 3,  A: 4, B: 3, group: 5 },
            { time: 1215, arena: 4,  A: 4, B: 3, group: 6 },
            { time: 1215, arena: 1,  A: 4, B: 3, group: 7 },
            { time: 1215, arena: 2,  A: 4, B: 3, group: 8 },

            { time: 1315, arena: 4,  A: 1, B: 2, group: 5 },
            { time: 1315, arena: 1,  A: 1, B: 2, group: 6 },
            { time: 1315, arena: 2,  A: 1, B: 2, group: 7 },
            { time: 1315, arena: 3,  A: 1, B: 2, group: 8 },

            { time: 1415, arena: 1,  A: 2, B: 4, group: 5 },
            { time: 1415, arena: 2,  A: 2, B: 4, group: 6 },
            { time: 1415, arena: 3,  A: 2, B: 4, group: 7 },
            { time: 1415, arena: 4,  A: 2, B: 4, group: 8 },

            { time: 1515, arena: 2,  A: 3, B: 1, group: 5 },
            { time: 1515, arena: 3,  A: 3, B: 1, group: 6 },
            { time: 1515, arena: 4,  A: 3, B: 1, group: 7 },
            { time: 1515, arena: 1,  A: 3, B: 1, group: 8 },

            /* Groups 9 - 12  */

            { time: 1030,  arena: 1, A: 1, B: 4, group: 9 },
            { time: 1030,  arena: 2, A: 1, B: 4, group: 10 },
            { time: 1030,  arena: 3, A: 1, B: 4, group: 11 },
            { time: 1030,  arena: 4, A: 1, B: 4, group: 12 },

            { time: 1130,  arena: 2, A: 2, B: 3, group: 9 },
            { time: 1130,  arena: 3, A: 2, B: 3, group: 10 },
            { time: 1130,  arena: 4, A: 2, B: 3, group: 11 },
            { time: 1130,  arena: 1, A: 2, B: 3, group: 12 },

            { time: 1230,  arena: 3, A: 4, B: 3, group: 9 },
            { time: 1230,  arena: 4, A: 4, B: 3, group: 10 },
            { time: 1230,  arena: 1, A: 4, B: 3, group: 11 },
            { time: 1230,  arena: 2, A: 4, B: 3, group: 12 },

            { time: 1330,  arena: 4, A: 1, B: 2, group: 9 },
            { time: 1330,  arena: 1, A: 1, B: 2, group: 10 },
            { time: 1330,  arena: 2, A: 1, B: 2, group: 11 },
            { time: 1330,  arena: 3, A: 1, B: 2, group: 12 },

            { time: 1430,  arena: 1, A: 2, B: 4, group: 9 },
            { time: 1430,  arena: 2, A: 2, B: 4, group: 10 },
            { time: 1430,  arena: 3, A: 2, B: 4, group: 11 },
            { time: 1430,  arena: 4, A: 2, B: 4, group: 12 },

            { time: 1530,  arena: 2, A: 3, B: 1, group: 9 },
            { time: 1530,  arena: 3, A: 3, B: 1, group: 10 },
            { time: 1530,  arena: 4, A: 3, B: 1, group: 11 },
            { time: 1530,  arena: 1, A: 3, B: 1, group: 12 },


            /* Groups 13 - 16 */
            { time: 1045,  arena: 1,  A: 1, B: 4, group: 13 },
            { time: 1045,  arena: 2,  A: 1, B: 4, group: 14 },
            { time: 1045,  arena: 3,  A: 1, B: 4, group: 15 },
            { time: 1045,  arena: 4,  A: 1, B: 4, group: 16 },

            { time: 1145,  arena: 2,  A: 2, B: 3, group: 13 },
            { time: 1145,  arena: 3,  A: 2, B: 3, group: 14 },
            { time: 1145,  arena: 4,  A: 2, B: 3, group: 15 },
            { time: 1145,  arena: 1,  A: 2, B: 3, group: 16 },

            { time: 1245,  arena: 3,  A: 4, B: 3, group: 13 },
            { time: 1245,  arena: 4,  A: 4, B: 3, group: 14 },
            { time: 1245,  arena: 1,  A: 4, B: 3, group: 15 },
            { time: 1245,  arena: 2,  A: 4, B: 3, group: 16 },

            { time: 1345,  arena: 4,  A: 1, B: 2, group: 13 },
            { time: 1345,  arena: 1,  A: 1, B: 2, group: 14 },
            { time: 1345,  arena: 2,  A: 1, B: 2, group: 15 },
            { time: 1345,  arena: 3,  A: 1, B: 2, group: 16 },

            { time: 1445,  arena: 1,  A: 2, B: 4, group: 13 },
            { time: 1445,  arena: 2,  A: 2, B: 4, group: 14 },
            { time: 1445,  arena: 3,  A: 2, B: 4, group: 15 },
            { time: 1445,  arena: 4,  A: 2, B: 4, group: 16 },

            { time: 1545,  arena: 2,  A: 3, B: 1, group: 13 },
            { time: 1545,  arena: 3,  A: 3, B: 1, group: 14 },
            { time: 1545,  arena: 4,  A: 3, B: 1, group: 15 },
            { time: 1545,  arena: 1,  A: 3, B: 1, group: 16 },
        ],

    },
    {
        phase: "ROUND_OF_16",
        name: "Round of 16",
        arenaSize: 4,
        combats: [
            // { time: 1600, arena: 1, A: { pos: 1, group: 1 }, B: { pos: 2, group: 16 } },
            // { time: 1600, arena: 2, A: { pos: 1, group: 2 }, B: { pos: 2, group: 15 } },
            // { time: 1600, arena: 3, A: { pos: 1, group: 3 }, B: { pos: 2, group: 14 } },
            // { time: 1600, arena: 4, A: { pos: 1, group: 4 }, B: { pos: 2, group: 13 } },
            // { time: 1615, arena: 1, A: { pos: 1, group: 5 }, B: { pos: 2, group: 12 } },
            // { time: 1615, arena: 2, A: { pos: 1, group: 6 }, B: { pos: 2, group: 11 } },
            // { time: 1615, arena: 3, A: { pos: 1, group: 7 }, B: { pos: 2, group: 10 } },
            // { time: 1615, arena: 4, A: { pos: 1, group: 8 }, B: { pos: 2, group: 9 } },
            // { time: 1630, arena: 1, A: { pos: 1, group: 9 }, B: { pos: 2, group: 8 } },
            // { time: 1630, arena: 2, A: { pos: 1, group: 10 }, B: { pos: 2, group: 7 } },
            // { time: 1630, arena: 3, A: { pos: 1, group: 11 }, B: { pos: 2, group: 6 } },
            // { time: 1630, arena: 4, A: { pos: 1, group: 12 }, B: { pos: 2, group: 5 } },
            // { time: 1645, arena: 1, A: { pos: 1, group: 13 }, B: { pos: 2, group: 4 } },
            // { time: 1645, arena: 2, A: { pos: 1, group: 14 }, B: { pos: 2, group: 3 } },
            // { time: 1645, arena: 3, A: { pos: 1, group: 15 }, B: { pos: 2, group: 2 } },
            // { time: 1645, arena: 4, A: { pos: 1, group: 16 }, B: { pos: 2, group: 1 } },

            { time: 1630, arena: 1, A: { pos: 1, group: 1 }, B: { pos: 1, group: 8 } },
            { time: 1630, arena: 2, A: { pos: 1, group: 2 }, B: { pos: 1, group: 7 } },
            { time: 1630, arena: 3, A: { pos: 1, group: 3 }, B: { pos: 1, group: 6 } },
            { time: 1630, arena: 4, A: { pos: 1, group: 4 }, B: { pos: 1, group: 5 } },
            { time: 1645, arena: 1, A: { pos: 1, group: 9 }, B: { pos: 1, group: 16 } },
            { time: 1645, arena: 2, A: { pos: 1, group: 10 }, B: { pos: 1, group: 15 } },
            { time: 1645, arena: 3, A: { pos: 1, group: 11 }, B: { pos: 1, group: 14 } },
            { time: 1645, arena: 4, A: { pos: 1, group: 12 }, B: { pos: 1, group: 13 } },
            // { time: 1630, arena: 1, A: { pos: 1, group: 9 }, B: { pos: 2, group: 8 } },
            // { time: 1630, arena: 2, A: { pos: 1, group: 10 }, B: { pos: 2, group: 7 } },
            // { time: 1630, arena: 3, A: { pos: 1, group: 11 }, B: { pos: 2, group: 6 } },
            // { time: 1630, arena: 4, A: { pos: 1, group: 12 }, B: { pos: 2, group: 5 } },
            // { time: 1645, arena: 1, A: { pos: 1, group: 13 }, B: { pos: 2, group: 4 } },
            // { time: 1645, arena: 2, A: { pos: 1, group: 14 }, B: { pos: 2, group: 3 } },
            // { time: 1645, arena: 3, A: { pos: 1, group: 15 }, B: { pos: 2, group: 2 } },
            // { time: 1645, arena: 4, A: { pos: 1, group: 16 }, B: { pos: 2, group: 1 } },
        ],
    },
    {
        phase: "QUARTER_FINALS",

        arenaSize: 4,
        combats: [
            { time: 1730, arena: 1, A: { winner: true, combat: { phase: "ROUND_OF_16", arena: 1, time: 1630 } }, B: { winner: true, combat: { phase: "ROUND_OF_16", arena: 2, time: 1630 } } },
            { time: 1730, arena: 2, A: { winner: true, combat: { phase: "ROUND_OF_16", arena: 3, time: 1630 } }, B: { winner: true, combat: { phase: "ROUND_OF_16", arena: 4, time: 1630 } } },
            { time: 1730, arena: 3, A: { winner: true, combat: { phase: "ROUND_OF_16", arena: 1, time: 1645 } }, B: { winner: true, combat: { phase: "ROUND_OF_16", arena: 2, time: 1645 } } },
            { time: 1730, arena: 4, A: { winner: true, combat: { phase: "ROUND_OF_16", arena: 3, time: 1645 } }, B: { winner: true, combat: { phase: "ROUND_OF_16", arena: 4, time: 1645 } } },
            // { time: 1715, arena: 1, A: { winner: true, combat: { phase: "ROUND_OF_16", arena: 1, time: 1730 } }, B: { winner: true, combat: { phase: "ROUND_OF_16", arena: 2, time: 1630 } } },
            // { time: 1715, arena: 2, A: { winner: true, combat: { phase: "ROUND_OF_16", arena: 3, time: 1730 } }, B: { winner: true, combat: { phase: "ROUND_OF_16", arena: 4, time: 1630 } } },
            // { time: 1715, arena: 3, A: { winner: true, combat: { phase: "ROUND_OF_16", arena: 1, time: 1745 } }, B: { winner: true, combat: { phase: "ROUND_OF_16", arena: 2, time: 1645 } } },
            // { time: 1715, arena: 4, A: { winner: true, combat: { phase: "ROUND_OF_16", arena: 3, time: 1745 } }, B: { winner: true, combat: { phase: "ROUND_OF_16", arena: 4, time: 1645 } } },
        ],
    },
    {
        phase: "SEMIFINALS",
        arenaSize: 4,
        combats: [
            { time: 2200, arena: 1, A: { winner: true, combat: { phase: "QUARTER_FINALS", arena: 1, time: 1730 } }, B: { winner: true, combat: { phase: "QUARTER_FINALS", arena: 2, time: 1730 } } },
            { time: 2215, arena: 2, A: { winner: true, combat: { phase: "QUARTER_FINALS", arena: 3, time: 1730 } }, B: { winner: true, combat: { phase: "QUARTER_FINALS", arena: 4, time: 1730 } } },
            // { time: 1745, arena: 3, A: { winner: true, combat: { phase: "QUARTER_FINALS", arena: 1, time: 1715 } }, B: { winner: true, combat: { phase: "QUARTER_FINALS", arena: 2, time: 1715 } } },
            // { time: 1745, arena: 4, A: { winner: true, combat: { phase: "QUARTER_FINALS", arena: 3, time: 1715 } }, B: { winner: true, combat: { phase: "QUARTER_FINALS", arena: 4, time: 1715 } } },

        ],
    },
    {
        phase: "FINAL",
        arenaSize: 1,
        combats: [
            { time: 2245, arena: 5, A: { winner: true, combat: { phase: "SEMIFINALS", arena: 1, time: 2200 } }, B: { winner: true, combat: { phase: "SEMIFINALS", arena: 2, time: 2215 } } },
            // { time: 1930, arena: 5, A: { winner: true, combat: { phase: "QUARTER_FINALS", arena: 3, time: 1845 } }, B: { winner: true, combat: { phase: "QUARTER_FINALS", arena: 4, time: 1845 } } },
        ],
    },
    // {
    //     phase: "FINAL",
    //     arenaSize: 1,
    //     combats: [
    //         { time: 2000, arena: 5, A: { winner: true, combat: { phase: "SEMIFINALS", arena: 5, time: 1915 } }, B: { winner: true, combat: { phase: "SEMIFINALS", arena: 5, time: 1930} } },
    //     ],
    // },

]

module.exports = {
    COMBATS_CONFIG, GROUP_SIZE, TOTAL_GROUPS
}